# PLM Phase Out Project Cockpit

## How do I build the project "plmphaseoutprojectcockpit"?
  Right-click on the folder "plmphaseoutprojectcockpit" and choose "Build". This should be possible when "package.json" and "Gruntfile.js" exist.
  A new dist/ directory is generated. It will contain the compressed + debug version of the project javascript files.

## How do I start this App inside the SAP Web IDE?

    Run > Run Configurations > Add "Web Application" > Select "index3.html" as File Name 

  Press "Save and Run"

## How do I start this App inside the SAP Web IDE?

    Run > Run Configurations > Add "SAP Fiori Launchpad Sandbox" > Select "Component.js" as File Name 

  Press "Save and Run"

  This will start a new tab in your browser pointing to something lengthy like this:

    https://webidetesting5081012-a79086271.dispatcher.hana.ondemand.com/test-resources/sap/ushell/shells/sandbox/fioriSandbox.html?hc_orionpath=%2...

  The mockserver is started automatically here, because inside the "Component.js" there is the following check:

    if(window.location.pathname.indexOf("fioriSandbox.html")>-1){
      mockserver.init();
    }
  

## How do I deploy this App to the SAP Cloud Platform?

    Right-click on the folder "plmphaseoutprojectcockpit" > "Deploy" > "Deploy to SAP Cloud Platform".


## How do I start this App from the SAP Cloud Platform as a Fiori Launchpad App?




## Special Note by Adrian Ngo and Stefan Röhrig from 27.03.2019:
    
    Infos of the category "Accessibility Error" such as e.g. "FIORI: xml_title_accessibility" listed in the "Problems" view are being ignored due to the fact that this application is by far too complex to handle, for a person who must rely on a screen-reader.



GIT Repositories:

Original one based on the SAP Web IDE (NEO):
https://git.hana.ondemand.com/a79086271/plmphaseoutprojectcockpit
user.email: Adrian.Ngo@vaillant-group.com
user.name: Adrian Ngo


Bitbucket Backup:
git clone https://adrian_ngo@bitbucket.org/adrian_ngo/plmphaseoutprojectcockpit.git
user.email: adrian.ngo@ibsolution.de
user.name: Adrian Ngo





