# Ideen/Vorschläge zum ODATA-Service für die Fiori-App **PLM Phase-Out Project-Cockpit**

*Stand 19.04.2018*

### Hinweis zur Notation
*Im Folgenden verwenden wir die in der SAPUI5 Dokumentation von*
[sap.ui.model.odata.v2.ODataModel](https://sapui5.hana.ondemand.com/#/api/sap.ui.model.odata.v2.ODataModel)
*beschriebenen Methoden zur Beschreibung der aus der Sicht der Frontend-Entwicklung erforderlichen ODATA-Schnittstellen. Dabei stellt die Variable*

    oModel

*ein Objekt der Klasse* 

    sap.ui.model.odata.v2.ODataModel 

*dar und mit*

    mParameters

*ist ein Map-Objekt gemeint, in dem für die jeweilige Methode erforderliche weitere Parameter wie z.B. Callback-Funktionen mitgegeben werden können.*

\
&nbsp;
\
&nbsp;

## 1 Anlegen eines neuen Projekts 

Beim Anlegen eines neuen Projekts mit dem "Projekt anlegen"-Dialog wird für die Wertehilfe des Eingabefeldes "PhaseOutManagerKey/PhaseOutManagerDisplayName" (Combobox) die Query 

    oModel.read("/PhaseOutManagerEntitySet", mParameters);

ans Backend geschickt, die eine Liste der verfügbaren PhaseOut-Manager zurückgibt.

Weiterhin wird eine Query benötigt, die eine Liste der frei verfügbaren (also noch keinem anderen PO-Projekt zugewiesenen) ECNs zurückgibt:

    oModel.read("/EcnEntitySet?$filter=ParentProjectKey EQ ''", mParameters);

Ohne den Filter soll 

    oModel.read("/EcnEntitySet", mParameters);

*alle* ECNs zurückgeben.

Zusätzlich wird für ein gewähltes ECN die Liste der Werke benötigt, die zu den Materialien gehören, die in dem gewählten ECN stecken. Ein 

**FunctionImport**

    oModel.callFunction("GetAllPlantsForEcn", {
      urlParameters: {
        "EcnKey": ...
      }
    })

soll so eine Liste liefern. Diese Liste wird dann in der Plant-Multiselektion angezeigt.

Erst danach kann der User die vollständigen Inputs 

* ProjectDescription
* PhaseOutDate
* PhaseOutManagerKey
* Liste der ECNs inklusive Auswahl der zugeörigen Werke

eingeben und diese Daten mit dem Submit-Button ans Backend schicken. Es wird eine Methode der Form 

    var sPath = "/ProjectEntitySet";
    var oData = {
      "ProjectDescription" : ...,
      "PhaseOutDate": ...,
      "PhaseOutManagerKey": ...,
      "ProjectStatus": "Draft",
      "Ecns": { 
          "EcnKey001" : [ "Plant001", "Plant002", "Plant003" ],
          "EcnKey002" : [ "Plant001", "Plant004", "Plant006" ]
      }
    };
    oModel.create(sPath, oData, mParameters);

*(mit Schreibzugriff)* benötigt. Als **Return-Value** wird der neu angelegte **ProjectKey** zurück ans Frontend geschickt. Damit ist ein neues Projekt zur weiteren Bearbeitung angelegt.


\
&nbsp;
\
&nbsp;

## 2 Navigation in das neu angelegte Projekt
Nach dem erfolgreichen Anlegen eines neuen Projekts mit dem Submit-Button wird der "Projekt anlegen"-Dialog wieder geschlossen. Das neue Projekt wird geöffnet für eine sofortige Weiterbearbeitung.

Für die Navigation dorthin mit

    sPath = "/ProjectEntitySet(ProjectKey)";
    oModel.read(sPath, mParameters);

bekommt das Frontend die Infos

    {
      "ProjectKey" : ...,
      "ProjectDescription" : ...,
      "PhaseOutDate": ...,
      "PhaseOutManagerKey": ...,
      "PhaseOutManagerDisplayName": ...,
      "ProjectStatus": "Draft"
    }

zurück. Mit 

    sPath = "/ProjectEntitySet(ProjectKey)/Ecns";
    oModel.read(sPath, mParameters);


erhält das Frontend die Liste der zugehörigen ECNs.

Als Rest-API sollte

    /ProjectEntitySet(ProjectKey)?$expand=Ecns

zur Verfügung stehen.

\
&nbsp;
\
&nbsp;

## 3 Navigation in ein ECN
Beim Klick auf eine ECN wird die Query

    sPath = "/EcnEntitySet(EcnKey)/Projectitems";
    oModel.read(sPath, mParameters);

erforderlich, um die Liste der zugehörigen ProjectItems zu erhalten. Darin sind die Materialhierarchien abgebildet.
Zu jedem ProjectItem gibt es dann eine Zuordnung zu einem Werk. (Hierin sind bislang nur die Werke enthalten, die auch in den ECNs aufgelistet waren.)

Entsprechende Rest-API:

    /EcnEntitySet(EcnKey)?$expand=Projectitems


\
&nbsp;
\
&nbsp;

## 4 Navigation in die Detailansicht

Beim Anklicken eines ProjektItems wird ein MasterData-Request abgeschickt:

    sPath = "/MasterDataEntitySet(MARA_MATNR='...', MARC_WERKS='...')"
    oModel.read(sPath, mParameters);

Damit werden Felder in der Detailsansicht bzw. in den Popovers gefüllt.

Zusätzlich wird noch für jedes Material die gesamte Liste von allen Werken benötigt, die im ERP existieren, beispielsweise realisiert durch einen

**FunctionImport**

    oModel.callFunction("GetPlantsForMaterial", {
      urlParameters: {
        "MARA_MATNR": ...
      }
    })


Damit können dann in der Detailansicht für alle Werke die drei Zustände

* StatusPhaseOut = true
* StatusPhaseOut = false
* nicht im ECN, aber im ERP vorhanden

dargestellt werden.

\
&nbsp;
\
&nbsp;

## 5 Hinzufügen eines neuen ECNs ("Add Available ECNs"-Dialog)
Es wird ein **FunctionImport** der Form 

    oModel.callFunction("AddNewECNsToProject", {
      urlParameters: {
        "ProjectKey": ...,
        "Ecns" : "{ 
          'EcnKey001' : [ 'Plant001', 'Plant002', 'Plant003' ],
          'EcnKey002' : [ 'Plant001', 'Plant004', 'Plant005' ]
        }"
      }
    })

*(mit Schreibzugriff)* benötigt, um einem existierenden Projekt *weitere ECNs* hinzuzufügen.


\
&nbsp;
\
&nbsp;

## 6 Bearbeiten eines existierenden ECNs ("Plants Assignment"-Dialog)

Für den Factory-Button benötigt das Frontend zusätzlich die Information welche Werke für eine ECN zugeordnet sind. Ein

**FunctionImport** der Form

    oModel.callFunction("GetAssignedPlantsForEcn", {
      urlParameters: {
        "EcnKey": ...
      }
    });

soll die Liste dieser Werke zurückgeben, identifiziert durch den **PlantKey**.

**TODO: Zu jedem zugeordneten Werk muss eine *Integritätsprüfung* sicherstellen, ob ein Werk überhaupt zurückgezogen werden darf. Dazu sind z.B. Checks nach dem Phase-Out-Status (Backend?) oder nach dem Editierstatus (Frontend?) nötig.**


Für das Abspeichern der Zuordnungen wird dementsprechend ein

**FunctionImport** der Form

    oModel.callFunction("SetAssignedPlantsForEcn", {
      urlParameters: {
        "EcnKey": ...,
        "Plants": "3001,1002,5555"
      }
    });

*(mit Schreibzugriff)* benötigt.


\
&nbsp;
\
&nbsp;

## 7 Löschen eines existierenden ECNs ("Mülleimer"-Button)

Für die Löschaktion wird eine entsprechende Methode

    oModel.remove("/EcnEntitySet(EcnKey)", mParameters);

*(mit Schreibzugriff)* im Backend benötigt.

**Zu klären von Stefan: Evtl. FunctionImport?**

\
&nbsp;
\
&nbsp;

## 8 Vererbung des Phase-Out-Status 
**TODO von IBsolution:** Aus dem exisitierenden ODATA-Model über entsprechende sPath herausziehen.

\
&nbsp;
\
&nbsp;

## 9 Wechsel zwischen List-View und Subtree-View
**TODO von IBsolution:** Aus dem exisitierenden ODATA-Model über entsprechende sPath herausziehen.

\
&nbsp;
\
&nbsp;

## 10 Phase-Out-Status für die Project-Items abspeichern

Für das Setzen des Phase-Out-Status eines Project-Items ist eine Methode der Form

    sPath = "/ProjectItemEntitySet(ProjectItemKey='...')";
    var oData = {
        "ShouldBePhasedOut" : {true|false}
    };

*(mit Schreibzugriff)* erforderlich.

**TODO von IBsolution:** **Save-Button** / *Pending Save*

\
&nbsp;
\
&nbsp;

## 11 Weitere Queries für die verschiedenen Filter

### Hinweis zur Notation

* [sap.ui.model.Filter](https://sapui5.netweaver.ondemand.com/#/api/sap.ui.model.Filter)
* [sap.ui.model.FilterOperator](https://sapui5.netweaver.ondemand.com/#/api/sap.ui.model.FilterOperator)

#### 11.1 Filter in der Projektübersicht (Worklist-View)
Es wird jeweils ein Filter für die Suche nach 

* Project ID

      "/ProjectEntitySet?$filter=ProjectKey Contains ..."


* Project Description

      "/ProjectEntitySet?$filter=ProjectDescription Contains ..."

* Phase Out Manager

      "/ProjectEntitySet?$filter=PhaseOutManagerDisplayName Contains ..."

* Phase Out Date **(Referenzzeitpunkt: 23:59:59)**

      "/ProjectEntitySet?$filter=PhaseOutDate LE ..."

* Project Status

      "/ProjectEntitySet?$filter=ProjectStatus EQ ..."

benötigt.

Erweiterte Filter nach Eigenschaften, die **keine** direkten Attribute von **ProjectEntitySet** sind:

* Change Number (ECN)

      "/ProjectEntitySet?$filter=EcnKey Contains ..."

* WBS Element

      "/ProjectEntitySet?$filter=EcnWbsElement Contains ..."

* Material

      "/ProjectEntitySet?$filter=MaterialKey Contains ..."

* Plant

      "/ProjectEntitySet?$filter=PlantKey Contains ..."

##### Logische Verknüpfungen

Alle aufgelisteten Filter müssen mit **AND** verknüpfbar gemacht werden.

**Bemerkung von Stefan:**
*Die erweiterten Filter sollen eventuell über eine separate Such-/Filter-View realisiert werden.*


#### 11.2 Filter in der Projekt-Item-Ansicht (Tree-View)
Für die Freitextsuche nach Materialnummer *oder* Materialbeschreibung in einem *einzigen* Eingabefeld werden die beiden Filter nach

* Materialnummer

      "/ProjectItemEntitySet?$filter=MatrialKey Contains ..."

* Materialbeschreibung

      "/ProjectItemEntitySet?$filter=MatrialDescription Contains ..."

benötigt. 

##### Logische Verknüpfungen

Die beiden Suchergebnisse werden mit **OR** verknüpft.

Weiterhin gibt es die Spaltenfilter 

* Plant

      "/ProjectItemEntitySet?$filter=PlantKey Contains ..."

* Phase-Out-Status

      "/ProjectItemEntitySet?$filter=ShouldBePhaseOut EQ {true|false}"

##### Logische Verknüpfungen

Die zusätzlichen Spaltenfilter sollten mit **AND** verknüpfbar gemacht werden, sowohl gegenseitig als auch mit dem Ergebnis der obigen Freitextsuche.


\
&nbsp;
\
&nbsp;

## 12 Projektstatus in der Projektübersicht

#### 12.1 Projektstatus anzeigen

Es wird eine neue Entity für die Wertehilfe für den ProjectStatus benötigt, z.B.: **ProjectStatusEntity**.
Dessen Attribute könnten z.B. folgenden Werte haben:

| ProjectStatusKey | | ProjectStatusDescription  |
| ---------------- | -- | --------------------------|
| POS001           | | Draft                     |
| POS002 | | Published |
| POS003 | | In Process |
| POS004 | | Closed |

**Zu klären von Stefan: Enum über Entity?**

#### 12.2 Projektstatus setzen

Um ein Projekt-Status zu setzen, ist eine Methode der Form

    sPath = "/ProjectEntitySet(ProjectKey='...')";
    oData = {
        "ProjectStatusKey": ...
    };

*(mit Schreibzugriff)* erforderlich.

**TODO von IBsolution:** **Neuer Save-Button** / *Pending Save*

\
&nbsp;
\
&nbsp;

## 13 "Edit Project Header Data"-Dialog

Für die Änderung der Projekt-Kopfdaten ist eine Methode der Form

    sPath = "/ProjectEntitySet(ProjectKey='...')";
    oData = {
      "ProjectDescription" : ...,
      "PhaseOutDate": ...,
      "PhaseOutManagerKey": ...
    };

*(mit Schreibzugriff)* erforderlich.

**TODO von IBsolution:** **Save-Button** / *Pending Save*

\
&nbsp;
\
&nbsp;

## Bemerkung:
Der Save-Button ist am Anfang inaktiv bzw. unsichtbar. Er wird aktiviert, sobald der Benutzer eine Modifikation 

* an den Project Header Data,
* am Phase-Out-Status pro Project-Item oder
* das Löschen eines ECNs mit dem TrashCan-Button

durchführen will.

Erst mit dem Drücken des "Save"-Buttons werden alle Modifikationen zusammen ans Backend geschickt:

    oModel.submitChanges(mParameters);

**Unabhängig** davon werden die Aktionen, die mit
* dem "Add available ECNs"-Dialog und
* dem "Modify Plants Assignment"-Dialog
initiiert werden, sofort ans Backend geschickt.

Eine Änderung der ECN Liste bzw. einer ECN-To-Plants-Zuordnung bewirkt einen Refresh der Tree-View.

Daher werden die drei Buttons 

* "Add ECN"-Button
* Factory-Button
* Mülleimer-Button

deaktiviert, sobald der Save-Button aktiv ist.




