sap.ui.define([
		"sap/ui/core/UIComponent",
		"sap/ui/Device",
		"sap/ui/model/json/JSONModel",
		"com/vaillant/plm/fiori/projectcockpit/model/models",
		"com/vaillant/plm/fiori/projectcockpit/controller/ErrorHandler"
	], function (UIComponent, Device, JSONModel, models, ErrorHandler) {
		"use strict";

		return UIComponent.extend("com.vaillant.plm.fiori.projectcockpit.Component", {

			metadata : {
				manifest: "json"
			},

			/**
			 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
			 * In this function, the FLP and device models are set and the router is initialized.
			 * @public
			 * @override
			 */
			init : function () {
				
				var bBatchMode = true;
				// The "BatchMode" flag should be true in productive code.
				// For debugging purposes it can be set to false to enable a better network logging for each GET reqeust.
				// However, do not forget to set it back to true before
				// committing to git or before deploying to the ABAP repository!
				
				// Alternatively use the URL parameter batchMode="off" to disable batchMode
				var sValue = jQuery.sap.getUriParameters().get("batchMode");
				if(sValue==="off"){
					bBatchMode = false;
				}
				

				if(!bBatchMode){
					jQuery.sap.log.setLevel(jQuery.sap.log.Level.INFO);
					jQuery.sap.log.warning("**************");
					jQuery.sap.log.warning("Batch mode off");
					jQuery.sap.log.warning("**************");
					jQuery.sap.log.setLevel(jQuery.sap.log.Level.ERROR);
				}

				var oSettingsModel = sap.ui.getCore().getModel("settingsModel");
            	// The "TestMode" flag is used to execute special code required for the App
            	// to run properly in mockdata-mode! Such code should be very rare and only an exception.
            	// They occur in those cases when e.g. the proper backend code is not yet available or when
            	// OData features are not available with Mockdata.

				if(!oSettingsModel){
    				sap.ui.getCore().setModel(new JSONModel({
    					"BatchMode" : bBatchMode,
    					"TestMode" : false
    				}),"settingsModel");
				} else {
				    // Startup.js from the integration test may have already set the "TestMode"
				    sap.ui.getCore().getModel("settingsModel").setProperty("/BatchMode", bBatchMode);
				}

				var oModel = new sap.ui.model.odata.v2.ODataModel({
					serviceUrl: "/sap/opu/odata/sap/Z_PLM_PHASE_OUT_02_V2_SRV/"
				});

				oModel.setChangeGroups({
					"StatusManagementEntity": {
						groupId: "StatusManagementEntityGroupId",
						single: true
					},
					"ProjectEntity": {
						groupId: "ProjectEntityGroupId",
						single: true
					}
				});

				oModel.setDeferredGroups(["StatusManagementEntityGroupId","ProjectEntityGroupId"]);

				// for debug off batch mode
				oModel.setUseBatch( bBatchMode );

				this.setModel(oModel,"serviceModel");

				// call the base component's init function
				// This calls App.controller.js onInit function which load the metadata.xml of our "serviceModel"!
				UIComponent.prototype.init.apply(this, arguments);

				// initialize the error handler with the component
				this._oErrorHandler = new ErrorHandler(this);

				// set the device model
				this.setModel(models.createDeviceModel(), "device");
				// set the FLP model
				this.setModel(models.createFLPModel(), "FLP");

				// create the views based on the url/hash
				this.getRouter().initialize();
			},

			/**
			 * The component is destroyed by UI5 automatically.
			 * In this method, the ErrorHandler is destroyed.
			 * @public
			 * @override
			 */
			destroy : function () {
				this._oErrorHandler.destroy();
				// call the base component's destroy function
				UIComponent.prototype.destroy.apply(this, arguments);
			},

			/**
			 * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
			 * design mode class should be set, which influences the size appearance of some controls.
			 * @public
			 * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
			 */
			getContentDensityClass : function() {
				if (this._sContentDensityClass === undefined) {
					// check whether FLP has already set the content density class; do nothing in this case
					if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
						this._sContentDensityClass = "";
					} else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
						this._sContentDensityClass = "sapUiSizeCompact";
					} else {
						// "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
						this._sContentDensityClass = "sapUiSizeCozy";
					}
				}
				return this._sContentDensityClass;
			}

		});

	}
);