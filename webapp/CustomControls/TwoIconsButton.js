sap.ui.define([
	"sap/m/Button",
	"sap/ui/core/Icon"
], function(Button, Icon) {

	return Button.extend("com.vaillant.plm.fiori.projectcockpit.CustomControls.TwoIconsButton", {

		metadata: {
			properties : {
				leftIcon: {type : "sap.ui.core.URI"},
				rightIcon: {type : "sap.ui.core.URI"}
			},
			aggregations: {
				_leftIcon: {
					type: "sap.ui.core.Icon",
					multiple: false
				},
				_rightIcon: {
					type: "sap.ui.core.Icon",
					multiple: false
				}
			}
		},
		
		init: function(){
			//set properties
			this.setProperty("leftIcon");
			this.setProperty("rightIcon");
			
			//set aggrigations
			this.setAggregation("_leftIcon", new Icon({
				size: "8px"
			}).addStyleClass("myTwoIconsMargin"));
			this.setAggregation("_rightIcon", new Icon({
			}).addStyleClass("myTwoIconsMargin"));
		},
		
		renderer: function(oRm, oControl) {
			oRm.write("<button");
			oRm.writeControlData(oControl);
			oRm.addClass("sapMBtn sapMBtnBase");
			oRm.writeClasses();
			oRm.write(">");
			oRm.write("<div");
			oRm.addClass("sapMBtnDefault sapMBtnHoverable sapMBtnInner sapMBtnText sapMFocusable");
			oRm.writeClasses();
			oRm.write(">");
			oRm.write("<span");
			oRm.addClass("sapMBtnContent");
			oRm.writeClasses();
			oRm.write(">");
			
			oRm.renderControl(oControl.getAggregation("_leftIcon").setSrc(oControl.getProperty("leftIcon")));
			oRm.renderControl(oControl.getAggregation("_rightIcon").setSrc(oControl.getProperty("rightIcon")));
			oRm.write(oControl.getText());
			
			oRm.write("</span>");
			oRm.write("</div>");
			oRm.write("</button>");
		}

	});
});