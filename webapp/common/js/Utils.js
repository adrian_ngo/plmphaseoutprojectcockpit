sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/Button"
], function(Controller, JSONModel, MessageToast, Dialog, Text, Button) {
	"use strict";
	return Controller.extend("com.vaillant.plm.fiori.projectcockpit.common.js.Utils", {

		confirmDiscardOLD: function(sMsgText, aActions) {
			var dialog = new Dialog({
			    id: "idConfirmationDialogOLD",
				title: "Confirm",
				type: "Message",
				content: new Text({
					text: sMsgText
				}),
				beginButton: new Button({
				    id: "idConfirmYesOLD",
					text: "Yes",
					press: function() {
						for (var index in aActions) {
							aActions[index]();
						}
						dialog.close();
					}
				}),
				endButton: new Button({
				    id: "idConfirmNoOLD",
					text: "No",
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});
			dialog.open();
		},
		
		confirmDiscardB: function(sMsgText, aActions, oParameters) {
			var that = this;
			var dialog = new Dialog({
			    id: "idConfirmationDialog",
				title: "Confirm",
				type: "Message",
				content: new Text({
					text: sMsgText
				}),
				beginButton: new Button({
				    id: "idConfirmYes",
					text: "Yes",
					press: function() {
						for (var index in aActions) {
							//aActions[index]();
							aActions[index].apply(that, [oParameters]);
							
						}
						dialog.close();
					}
				}),
				endButton: new Button({
				    id: "idConfirmNo",
					text: "No",
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});
			dialog.open();
		},
		
		fnOneActionDiscard: function(sFirstMessageText, fnAction, oParameters) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			var that = this;
			sap.m.MessageBox.warning(
				sFirstMessageText, {
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.CANCEL],
					styleClass: bCompact ? "sapUiSizeCompact" : "",
					onClose: function(sAction1) {
						if (sAction1 === "YES") {
							//MessageToast.show("Action selected: " + sAction2);
							fnAction.apply(that, [oParameters]);
						}

					}
				}
			);
		},

		fnDiscardAction: function(sFirstMessageText, sSecondMessageText, oContext, fnAction, oDialog) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			var that = this;
			sap.m.MessageBox.warning(
				sFirstMessageText, {
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.CANCEL],
					styleClass: bCompact ? "sapUiSizeCompact" : "",
					onClose: function(sAction1) {
						if (sAction1 === "YES") {
							sap.m.MessageBox.warning(
								sSecondMessageText, {
									actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.CANCEL],
									styleClass: bCompact ? "sapUiSizeCompact" : "",
									onClose: function(sAction2) {
										if (sAction2 === "YES") {
											//MessageToast.show("Action selected: " + sAction2);
											fnAction.apply(that, [oContext]);

											//close dialog
											oDialog.close();
											oDialog.unbindElement();

										}
									}
								}
							);
						}

					}
				}
			);
		}

	});
});