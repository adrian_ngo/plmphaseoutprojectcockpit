jQuery.sap.declare("com.vaillant.plm.fiori.projectcockpit.common.js.Constants");

com.vaillant.plm.fiori.projectcockpit.common.js.Constants = {
	
	"ProjectStatus": {
		"ACTIVE": "Active",
		"DRAFT": "Draft",
		"DELETED": "Deleted",
		"TO_BE_PUBLISHED": "ToBePublished",
		"PUBLISHED": "Published",
		"IN_PROCESS": "InProcess",
		"STOCK_CLEANSING": "StockCleansing",
		"CLOSED": "Closed"
	},

	"ProjectStates": {
		"ACTIVE": {
			"Key": "Active",
			"DisplayName" : "projectStatusActive"
		},
		"DRAFT": {
			"Key": "Draft",
			"DisplayName" : "projectStatusDraft"
		},
		"DELETED": {
			"Key": "Deleted",
			"DisplayName" : "projectStatusDeleted"
		},
		"TO_BE_PUBLISHED": {
			"Key": "ToBePublished",
			"DisplayName" : "projectStatusToBePublished"
		},
		"PUBLISHED": {
			"Key": "Published",
			"DisplayName" : "projectStatusPublished"
		},
		"IN_PROCESS": {
			"Key": "InProcess",
			"DisplayName" : "projectStatusInProcess"
		},
		"STOCK_CLEANSING": {
			"Key": "StockCleansing",
			"DisplayName" : "projectStatusStockCleansing"
		},
		"CLOSED": {
			"Key": "Closed",
			"DisplayName" : "projectStatusClosed"
		}
	}

}