jQuery.sap.declare("com.vaillant.plm.fiori.projectcockpit.localServiceV2.V2.mockdata.functionImport.getPotentialPlants");

com.vaillant.plm.fiori.projectcockpit.localServiceV2.V2.mockdata.functionImport.getPotentialPlants =
{
    "d": {
        "results": [
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0100')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0100')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0100",
                "PlantDescription": "Vertriebswerk Region Nord"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0201",
                "PlantDescription": "Werk 201"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0301",
                "PlantDescription": "Werk 301"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0100')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0100')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "1100",
                "PlantDescription": "Werk 1100"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0400')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0400')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0400",
                "PlantDescription": "Vertriebswerk Region Ost"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0401",
                "PlantDescription": "Werk 401"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0501",
                "PlantDescription": "Werk 501"
            },
            {
                "__metadata": {
                    "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                    "type": "phaseout.PlantEntity"
                },
                "PlantKey": "0601",
                "PlantDescription": "Werk 601"
            }
        ]
    }
}
;