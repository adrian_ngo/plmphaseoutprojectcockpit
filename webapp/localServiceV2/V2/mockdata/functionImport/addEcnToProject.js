jQuery.sap.declare("com.vaillant.plm.fiori.projectcockpit.localServiceV2.V2.mockdata.functionImport.addEcnToProject");

com.vaillant.plm.fiori.projectcockpit.localServiceV2.V2.mockdata.functionImport.addEcnToProject = {

    "d": {
        "__metadata": {
            "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/EcnEntitySet('ECN000000126')",
            "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/EcnEntitySet('ECN000000126')",
            "type": "phaseout.EcnEntity"
        },
        "EcnKey": "ECN000000126",
        "ParentProjectKey": "1111111553",
        "EcnDescription": "My first material release",
        "PsProject": "00000000",
        "WBSElement": "00063254",
        "ValidFrom": "/Date(1560117600000)/",
        "AvailablePlants": {
            "results": [
                {
                    "__metadata": {
                        "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                        "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0001')",
                        "type": "phaseout.PlantEntity"
                    },
                    "PlantKey": "0001",
                    "PlantDescription": "Werk 1 Remscheid"
                },
                {
                    "__metadata": {
                        "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0002')",
                        "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0002')",
                        "type": "phaseout.PlantEntity"
                    },
                    "PlantKey": "0100",
                    "PlantDescription": "Vertriebswerk Region Nord"
                },
                {
                    "__metadata": {
                        "id": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0003')",
                        "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/PlantEntitySet('0003')",
                        "type": "phaseout.PlantEntity"
                    },
                    "PlantKey": "0400",
                    "PlantDescription": "Vertriebswerk Region Ost"
                }
            ]
        },
        "ProjectItems": {
            "__deferred": {
                "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/EcnEntitySet('ECN000000126')/ProjectItems"
            }
        },
        "SelectedPlants": {
            "__deferred": {
                "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/EcnEntitySet('ECN000000126')/SelectedPlants"
            }
        },
        "Project": {
            "__deferred": {
                "uri": "http://sge04.vaillant.vhgroup.lan:8082/sap/opu/odata/sap/Z_PLM_PHASE_OUT_SRV_01/EcnEntitySet('ECN000000126')/Project"
            }
        }
    }

};