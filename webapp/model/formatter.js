sap.ui.define([
	"sap/ui/core/format/DateFormat",
	"sap/ui/core/ValueState",
	"com/vaillant/plm/fiori/projectcockpit/common/js/Constants"
], function (DateFormat, ValueState, Constants) {
	"use strict";

	return {

		translate: function(sText){
			var oBundle = this.getResourceBundle();
			return oBundle.getText(sText);
		},

		/**
		 * set enabled boolean value for submit button in new project dialog
		 * @public
		 * @param {boolean} bFullDialog short of full version of new project dialog
		 * @param {boolean} bHeader all required fields in header are filled
		 * @param {boolean} bItem all fields in ecn list are filled
		 * @returns {boolean}
		 */

		getSubmitBtnEnabled: function(bFullDialog, bHeader, bItem){
			if(bFullDialog){
				return bHeader && bItem;
			}else{
				return bHeader;
			}
		},

		/**
		 * set name text of submit button in new project dialog
		 * @public
		 * @param {boolean} bFullDialog short of full version of new project dialog
		 * @param {string} sSubmitTitle name of button for full version of dialog
		 * @param {string} sAddEcnTitle name of button for short version of dialog
		 * @returns {string} sButtonTitle
		 */

		getSubmitBtnText: function(bFullDialog, sSubmitTitle, sAddEcnTitle){
			var sButtonTitle = sAddEcnTitle;
			if(bFullDialog){
				sButtonTitle = sSubmitTitle;
			}

			return sButtonTitle;
		},


		/**
		 * set visibility boolean value for RootElement icon
		 * @public
		 * @param {integer} iHierarchyLevel level of project item
		 * @param {string} sTableModeText of tree table mode
		 * @returns {boolean}
		 */

		getVisibleRootIcon:function(iHierarchyLevel, sTableModeText){
			var bVisible = true;
			if(sTableModeText !== "flatTableMode"){
				if(iHierarchyLevel === 0){
					bVisible = false;
				}
			}
			return bVisible;
		},

		/**
		 * set icon depending on plant pahse out status
		 * @public
		 * @param {string} sValue the plant status in order Phase out projects
		 * @returns {string}
		 */

		getStatusIconForPlant: function(sValue){
			var sIconPositiveStatus = "sap-icon://status-positive",
				sIconNegativeStatus = "sap-icon://status-negative",
				sIconInactiveStatus = "sap-icon://status-inactive",
				sResultIcon;

			switch(sValue){
				case "true":
					sResultIcon = sIconPositiveStatus;
					break;
				case "false":
					sResultIcon = sIconNegativeStatus;
					break;
				default:
					sResultIcon = sIconInactiveStatus;
			}
			return sResultIcon;
		},

		/* *
		 * set visibility of action depending on project status
		 * @public
		 * @param {string} sValue the staus of project
		 * @returns {boolean}
		 */
		getPublishDeleteVisibility: function(sValue){
			// Draft => User can press the "Play"-button or the "Delete"-button
			return (sValue === Constants.ProjectStatus.DRAFT);
		},

		getEditVisibility: function(sValue){
			// Project is in status "To be published", "Published", "In Process", "Stock Cleansing" or "Closed"
			// => User can only press the "Stop"-button.
			var aProjectPublished = [
				Constants.ProjectStatus.TO_BE_PUBLISHED,
				Constants.ProjectStatus.PUBLISHED,
				Constants.ProjectStatus.IN_PROCESS,
				Constants.ProjectStatus.STOCK_CLEANSING,
				Constants.ProjectStatus.CLOSED
			];
			return (aProjectPublished.indexOf(sValue) > -1);
		},

		getUndeleteVisibility: function(sValue){
			return (sValue === Constants.ProjectStatus.DELETED);
		},

		/**
		 * set the project status text
		 * @public
		 * @param {string} sText the status of the project
		 * @returns {string} draft if sText is empty
		 */
		getProjectStatusText: function(sText){
			if(sText===""){
				return this.getView().getModel("i18n").getResourceBundle().getText("projectStatusDraft");
			}
			return this.getView().getModel("i18n").getResourceBundle().getText("projectStatus"+sText);
		},

		/**
		 * set the state of project
		 * @public
		 * @param {string} sValue the staus of project
		 * @returns {string} standard state of object status
		 */

		 getProjectStatusState: function(sValue){
		 	var sValueState = "None";
			var oBundle = this.getView().getModel("i18n").getResourceBundle();

		 	switch(sValue){
		 		case Constants.ProjectStatus.TO_BE_PUBLISHED:
		 			sValueState = "Warning";
		 			break;
		 		case Constants.ProjectStatus.PUBLISHED:
		 			sValueState = "Success";
		 			break;
		 		case Constants.ProjectStatus.IN_PROCESS:
		 			sValueState = "Success";
		 			break;
		 		case Constants.ProjectStatus.STOCK_CLEANSING:
		 			sValueState = "Success";
		 			break;
		 		case Constants.ProjectStatus.CLOSED:
		 			sValueState = "Success";
		 			break;
		 	}

		 	return sValueState;
		 },


		valueState: function(sValue) {
			if (sValue === "None") {
				return ValueState.None;
			} else if (sValue === "Success") {
				return ValueState.Success;
			} else if (sValue === "Warning") {
				return ValueState.Warning;
			} else if (sValue === "Error") {
				return ValueState.Error;
			} else {
				return (sValue);
			}
		},

		date: function(value) {
			if (value) {
				var oDateFormat = DateFormat.getDateTimeInstance({
					pattern: "dd.MM.yyyy" //"yyyy-MM-dd"
				});
				// Check format of date
				if (typeof value === "object") {
					// value is Date
					return oDateFormat.format(value);
				} else if (typeof value === "string" && value.search(/\/Date\(/) !== -1) {
					var a = value.match(/\/Date\((\d*)\)\//);
					return oDateFormat.format(new Date(parseInt(a[1], 10)));
				} else if (typeof value === "string") {
					// value is string and
					// format is: "2013-05-22T22:00:00.000Z"
					return oDateFormat.format(new Date(value));
				} else {
					return value;
				}
			} else {
				return value;
			}
		},

		/**
		 * Rounds the number unit value to 2 digits
		 * @public
		 * @param {string} sValue the number string to be rounded
		 * @returns {string} sValue with 2 digits rounded
		 */
		numberUnit: function(sValue) {
			if (!sValue) {
				return "";
			}
			return parseFloat(sValue).toFixed(2);
		},

		decimalNumber: function(sValue) {
            return sValue;
		    // requires new version of SAPUI5. "1.52.9" would not work.
		    /*
		    var oNumberFormat = sap.ui.core.format.NumberFormat;
			return oNumberFormat.getFloatInstance({
			    decimals: 2,
			    groupingEnabled: true
			}).format(sValue);
			*/
		},


		/**
		 * Data Model 2.0:
		 * Used to show PO project number of another PO project in which this item is already being marked for phased-out!
		 *
		 */
		showOtherProject: function(sProjectKey, sCurrentProjectKey){
			if(sProjectKey === sCurrentProjectKey){
					return "";
			} else {
					return sProjectKey;
			}
		},

		showOtherProjectHighlightIcon: function(sProjectKey, sCurrentProjectKey){
			if(!sProjectKey){
				return "sap-icon://unlocked";
			} else {
				if(sProjectKey === sCurrentProjectKey){
						return "sap-icon://locked";
				} else {
						return "sap-icon://locked";
				}
			}
		},

		showOtherProjectHighlight: function(sProjectKey, sCurrentProjectKey){
			if(!sProjectKey){
				return "Warning";
			} else {
				if(sProjectKey === sCurrentProjectKey){
						return "Information";
				} else {
					return "Error";
				}
			}
		},

		showOtherProjectHighlightText: function(sProjectKey, sCurrentProjectKey){
			if(sProjectKey === sCurrentProjectKey){
					return "";
			} else {
					return "in other Project";
			}
		},

		/**
		 * Data Model 2.0:
		 * The old condition 
		 * 'StatusManagement/ShouldBePhasedOut' === true 
		 * is replaced by the new condition
		 * 'AssignedProject/ProjectKey' === Current ProjectKey
		 *
		 */
		showAcceptedPhaseOut: function(sProjectKey, sCurrentProjectKey, sMARC_MMSTA, sMaterialPlantItemStatus, sMaterialPlantItemDQ){
			var bReturn = false;
			if(sProjectKey && sProjectKey === sCurrentProjectKey){
				// Indicating that the item is assigned to this project.
				if( sMaterialPlantItemStatus === 'clean_plantstatus' ||
					sMaterialPlantItemStatus === 'error_plantstatus' ||
					sMaterialPlantItemStatus === 'clean_workflow' ||
					sMaterialPlantItemStatus === 'error_workflow' ||
					sMaterialPlantItemStatus === 'warning' ||
					sMaterialPlantItemStatus === 'pending_workflow' ||
					sMaterialPlantItemStatus === 'phaseout_finished'
				){
					bReturn = true;
				}
			}

			if( sMaterialPlantItemStatus === "transfer_phaseout_true" ){
				bReturn = true;
			}

			if( sMaterialPlantItemStatus === "transfer_phaseout_false" ){
				bReturn = false;
			}

			return bReturn;
		},

		enableAcceptedPhaseOut: function(sProjectKey, sCurrentProjectKey, sMARC_MMSTA, sMaterialPlantItemStatus, sMaterialPlantItemDQ){
			var bReturn = true;
			// Rule: 
			// Items with PlantStatus > 50 are already in a PO-process 
			// and MUST NOT be taken out from phase out!
			if( parseInt(sMARC_MMSTA,10) > 50 ){
				bReturn = false;
			}
			return bReturn;
		},


		/**
		 * Data Model 2.0:
		 * The old condition 
		 * 'StatusManagement/ShouldBePhasedOut' === false
		 * is replaced by the new condition
		 * 'AssignedProject/ProjectKey' === undefined, null or empty.
		 *
		 */
		showDeclinedPhaseOut: function(sProjectKey, sCurrentProjectKey, sMARC_MMSTA, sMaterialPlantItemStatus, sMaterialPlantItemDQ){
			var bReturn = false;

			if(sProjectKey === null || sProjectKey === undefined || sProjectKey === ""){
				
				// Indicating that the item is not yet assigned to the project.
				bReturn = true;

			}

			if( sMaterialPlantItemStatus === "transfer_phaseout_true" ){
				bReturn = false;
			}

			if( sMaterialPlantItemStatus === "transfer_phaseout_false" ){
				bReturn = true;
			}

			return bReturn;
		},
		
		enableDeclinedPhaseOut: function(sProjectKey, sCurrentProjectKey, sMARC_MMSTA, sMaterialPlantItemStatus, sMaterialPlantItemDQ){
			var bReturn = true;

			// Rule:
			// The user MUST NOT enable phase-out for this item if DQ is bad!
			if( sMaterialPlantItemDQ === "E" ){
				bReturn = false;
			}

			return bReturn;
		},



		formatWildCards: function( sQueryString ){
			var sNewString = sQueryString;
			if(sQueryString && sQueryString !== ""){
				if(sQueryString[0] !== "*"){
					sNewString = "*" + sQueryString;
				}
				if(sNewString[sNewString.length - 1] !== "*"){
					sNewString = sNewString + "*";
				}
			}
			return sNewString;
		}

	};

});