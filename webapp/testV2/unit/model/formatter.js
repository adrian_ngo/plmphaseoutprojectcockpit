sap.ui.define([
		"com/vaillant/plm/fiori/projectcockpit/model/formatter"
	], function (formatter) {
		"use strict";

		QUnit.module("Formatter unit");

		function numberUnitValueTestCase(assert, sValue, fExpectedNumber) {
			// Act
			var fNumber = formatter.numberUnit(sValue);

			// Assert
			assert.strictEqual(fNumber, fExpectedNumber, "The rounding was correct");
		}

		function dateValueTestCase(assert, sDateValue, sExpectedDateString) {
			// Act
			var sDateString = formatter.date(sDateValue);

			// Assert
			assert.equal(sDateString, sExpectedDateString, "The date string was correct");
		}
		
		QUnit.test("Should round down a 3 digit number", function (assert) {
			numberUnitValueTestCase.call(this, assert, "3.123", "3.12");
		});

		QUnit.test("Should round up a 3 digit number", function (assert) {
			numberUnitValueTestCase.call(this, assert, "3.128", "3.13");
		});

		QUnit.test("Should round a negative number", function (assert) {
			numberUnitValueTestCase.call(this, assert, "-3", "-3.00");
		});

		QUnit.test("Should round an empty string", function (assert) {
			numberUnitValueTestCase.call(this, assert, "", "");
		});

		QUnit.test("Should round a zero", function (assert) {
			numberUnitValueTestCase.call(this, assert, "0", "0.00");
		});
 
		QUnit.test("Should convert date string '2012-09-18T18:15:20.500Z' to date string '18.09.2012' ", function (assert) {
			dateValueTestCase.call(this, assert, "2012-09-18T18:15:20.500Z", "18.09.2012");
		});

		QUnit.test("Should convert date string 'Sun Dec 22 2019 17:36:37 GMT+0100 (Mitteleuropäische Zeit)' to date string '22.12.2019' ", function (assert) {
			dateValueTestCase.call(this, assert, "Sun Dec 22 2019 17:36:37 GMT+0100 (Mitteleuropäische Zeit)", "22.12.2019");
		});

		QUnit.test("Should convert date string '/Date(1577032597000)/' to date string '22.12.2019' ", function (assert) {
			dateValueTestCase.call(this, assert, "/Date(1577032597000)/", "22.12.2019");
		});

		QUnit.test("Should convert date object to date string '22.12.2019' ", function (assert) {
			dateValueTestCase.call(this, assert, new Date(2019, 11, 22, 8, 10, 10), "22.12.2019");
		});
	}
);