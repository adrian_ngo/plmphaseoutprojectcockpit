sap.ui.define([
	"sap/ui/test/opaQunit",
	"sap/ui/test/Opa5"
], function (opaTest, Opa5) {
	"use strict";

	QUnit.module("Project Creation");
	var aIndexesToBeDeleted = [2, 1];
	var oProjectHeadData = {
		ProjectDescription: "Test Description",
		PhaseOutManagerKey: "UserKey 100",
		PhaseOutDate: "03.07.2018"
	};
	var aEcnsToBeAdded = [{
		index: 0,
		ECN: "ECN000000141",
		plants: [
			"1350",
			"1351"
		]
	}, {
		index: 1,
		ECN: "ECN000000201",
		plants: [
			"3110",
			"3116",
			"3001"
		]
	}, {
		index: 2,
		ECN: "ECN000000244",
		plants: [
			"3100",
			"3150",
			"9001"
		]
	}];

	opaTest("Fill project head data and Add ECNs", function (Given, When, Then) {
		//Action
		When.onTheProjectCreationDialog.iFillTheProjectHeadData(oProjectHeadData);

		//Assertion
		Then.onTheProjectCreationDialog.theProjectDescriptionIsFilled();
		Then.onTheProjectCreationDialog.theProjectManagerIsFilled();
		Then.onTheProjectCreationDialog.theProjectDateIsFilled();

		//Action
		When.onTheProjectCreationDialog.iPressApplyButton();

		//Assertion
		Then.onTheProjectCreationDialog.anEcnCanBeAdded();

		//Action
		for (var i = 0; i < aEcnsToBeAdded.length; i++) {
			var oEcnToBeAdded = aEcnsToBeAdded[i];
			When.onTheProjectCreationDialog.iPressAddEcnButton();
			When.onTheProjectCreationDialog.iAddAnEcnKey(oEcnToBeAdded);
			When.onTheProjectCreationDialog.iOpenThePlantPicker(oEcnToBeAdded.index);
			When.onTheProjectCreationDialog.iAddAPlantKey(oEcnToBeAdded);

			// Assertions
			Then.onTheProjectCreationDialog.theTableWasFilledCorrectly(oEcnToBeAdded.index);
		}

	});
	
	opaTest("Save and Close Dialog", function (Given, When, Then) {
		//Action
		When.onTheProjectCreationDialog.iPressCancelButton();

		//Assertion
		Then.onTheProjectCreationDialog.theWarningDialogIsShown();
		
		//Action
		When.onTheProjectCreationDialog.iPressCancelWarningButton();
		
		//Assertion
		Then.onTheProjectCreationDialog.theWarningDialogIsClosed();
	});


	// opaTest("Remove ECNs", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheProjectCreationDialog.iPressDeleteEcnButtonOnIndexes(aIndexesToBeDeleted);

	// 	//Assertions
	// 	Then.onTheProjectCreationDialog.onlyThisManyEcnsAreAvaiable(iNumberOfEcnsToBeAdded - aIndexesToBeDeleted.length);
	// });

	// opaTest("Add and reset ECNs", function (Given, When, Then) {
	// 	//Actions
	// 	for (var i = 0; i < iNumberOfEcnsToBeAdded; i++) {
	// 		When.onTheProjectCreationDialog.iPressAddEcnButton();
	// 	}

	// 	When.onTheProjectCreationDialog.iPressResetTableButton();

	// 	//Assertions
	// 	Then.onTheProjectCreationDialog.theEcnTableIsEmpty();
	// });

	// opaTest("Open cancelation dialog", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheProjectCreationDialog.iPressCancelButton();

	// 	//Assertions
	// 	Then.onTheProjectCreationDialog.theCancelationPopUpIsDisplayed();
	// });

	// opaTest("Close cancelation dialog by pressing no", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheProjectCreationDialog.iPressNoButton();

	// 	//Assertions
	// 	Then.onTheProjectCreationDialog.theCancelationPopUpIsDestroyed();
	// });

	// opaTest("Open cancelation dialog a second time", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheProjectCreationDialog.iPressCancelButton();

	// 	//Assertions
	// 	Then.onTheProjectCreationDialog.theCancelationPopUpIsDisplayed();
	// });

	// opaTest("Close cancelation dialog by pressing yes", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheProjectCreationDialog.iPressYesButton();

	// 	//Assertions
	// 	Then.onTheProjectCreationDialog.theProjectCreationDialogIsDestroyed();
	// });
});