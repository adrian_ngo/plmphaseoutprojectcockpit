sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/AggregationLengthEquals",
	"sap/ui/test/matchers/AggregationFilled",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/Common",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/shareOptions"
], function (Opa5, Press, EnterText, AggregationLengthEquals, AggregationFilled, PropertyStrictEquals, Common, shareOptions) {
	"use strict";

	var sViewName = "ProjectWorklist";
	var sAddEcnButton = "addEcnToProject";
	var sResetProjectButton = "resetProject";
	var sEcnTableId = "idEcnList";
	var sCancelProjectCreationButtonId = "cancelProjectCreation";

	Opa5.createPageObjects({

		onTheProjectCreationDialog: {
			baseClass: Common,
			actions: jQuery.extend({
					iPressCancelWarningButton: function () {
						return this.waitFor({
							autoWait: true,
							searchOpenDialogs: true,
							controlType: "sap.m.Button",
							matchers: function (oButton) {
								return oButton.getText() === "Abbrechen";
							},
							actions: new Press(),
							errorMessage: "The cancel button of the warning dialog wasn't pressed"
						});
					},

					iOpenThePlantPicker: function (iIndex) {
						return this.waitFor({
							autoWait: true,
							searchOpenDialogs: true,
							controlType: "sap.m.Table",
							matchers: function (oTable) {
								var oItem = oTable.getItems()[iIndex];
								var oPlantInput = oItem.getCells()[1];
								if (oPlantInput) {
									return oPlantInput;
								}
								return false;
							},
							actions: function (oInput) {
								oInput.getPicker().open();
							},
							errorMessage: "Could not open the Plant Picker"
						});
					},

					iAddAnEcnKey: function (oEcnToBeAdded) {
						return this.waitFor({
							autoWait: true,
							searchOpenDialogs: true,
							controlType: "sap.m.Table",
							matchers: function (oTable) {
								var oSessionContext = sap.ui.test.Opa5.getContext();
								oSessionContext.oEcnToBeAdded = oEcnToBeAdded;
								var oItem = oTable.getItems()[oEcnToBeAdded.index];
								var oEcnKeyInput = oItem.getCells()[0].getItems()[0];
								return oEcnKeyInput;
							},
							actions: function (oInput) {
								var oSessionContext = sap.ui.test.Opa5.getContext();
								oInput.getPicker().open();
								var oItem = oInput.getItems().find(function (oItemEval) {
									if (oItemEval.getKey() === oSessionContext.oEcnToBeAdded.ECN) {
										return true;
									}
									return false;
								});
								oItem.$().trigger("tap");
							},
							errorMessage: "Could not add an ECN to the project"
						});
					},

					iAddAPlantKey: function (oEcnToBeAdded) {
						return this.waitFor({
							autoWait: true,
							searchOpenDialogs: true,
							controlType: "sap.m.Table",
							matchers: function (oTable) {
								var oItem = oTable.getItems()[oEcnToBeAdded.index];
								var oPlantInput = oItem.getCells()[1];
								if (oPlantInput) {
									return oPlantInput;
								}
								return false;
							},
							actions: function (oInput) {
								var oSessionContext = sap.ui.test.Opa5.getContext();
								var aItems = [];
								var aItemsEval = oInput.getItems();
								// cant use oInput.getItems().filter(), as it is async! So a Custom Filter was created
								for (var i = 0; i < aItemsEval.length; i++) {
									var oItem = aItemsEval[i];
									if (oSessionContext.oEcnToBeAdded.plants.includes(oItem.getKey())) {
										aItems.push(oItem);
									} else if (aItems.length === oSessionContext.oEcnToBeAdded.plants.length) {
										break;
									}
								}
								oInput.setSelectedItems(aItems);
								oInput.fireSelectionChange();
							},
							errorMessage: "Could not add a plant to the project"
						});
					},

					iPressAddEcnButton: function () {
						return this.waitFor({
							controlType: "sap.m.Button",
							searchOpenDialogs: true,
							matchers: function (oButton) {
								var sText = oButton.getText();
								if (sText === "add ECN") {
									return oButton;
								}
								return false;
							},
							actions: new Press(),
							errorMessage: "Could not Press Add ECN Button"
						});
					},

					iPressApplyButton: function () {
						return this.waitFor({
							controlType: "sap.m.Button",
							searchOpenDialogs: true,
							matchers: function (oButton) {
								var sText = oButton.getText();
								if (sText === "add ECNs") {
									return oButton;
								}
								return false;
							},
							actions: new Press(),
							errorMessage: "The button \"Add ECNs\" could not be found"
						});
					},

					iFillTheProjectHeadData: function (oProjectHeadData) {
						return this.waitFor({
							controlType: "sap.m.InputBase",
							searchOpenDialogs: true,
							matchers: function (oInput) {
								var sInputName = oInput.getName();
								switch (sInputName) {
								case "ProjectDescription":
								case "PhaseOutManagerKey":
								case "PhaseOutDate":
									return oInput;
								default:
									return false;
								}
							},
							actions: function (oInput) {
								if (oInput.getMetadata().getName() === "sap.m.ComboBox") {
									var oItem = oInput.getItems().find(function (oItemEval) {
										if (oItemEval.getKey() === oProjectHeadData[oInput.getName()]) {
											return true;
										}
										return false;
									});
									oInput.getPicker().open();
									oItem.$().trigger("tap");
								} else {
									new EnterText({
										text: oProjectHeadData[oInput.getName()]
									}).executeOn(oInput);
								}
							},
							success: function () {
								var SessionContext = sap.ui.test.Opa5.getContext();
								SessionContext.projectHeaderData = oProjectHeadData;
							},
							errorMessage: "Project Header could not be filled"
						});
					},

					iPressDeleteEcnButtonOnIndexes: function (aIndexesToBeDeleted) {
						return this.waitFor({
							id: sEcnTableId,
							viewName: sViewName,
							matchers: function (oTable) {
								var aItems = oTable.getItems();
								var aButtons = [];
								for (var i = 0; i < aItems.length; i++) {
									if (aIndexesToBeDeleted.includes(i)) {
										var oItem = aItems[i];
										var oButton = oItem.getCells()[2];
										aButtons.push(oButton);
									}
								}
								return aButtons;
							},
							actions: new Press(),
							errorMessage: "Could not Press Delete ECN Button on the requested indexes"
						});
					},

					iPressResetTableButton: function () {
						return this.waitFor({
							id: sResetProjectButton,
							viewName: sViewName,
							actions: new Press(),
							errorMessage: "Could not press reset ECN button"
						});
					},

					iPressCancelButton: function () {
						return this.waitFor({
							autoWait: true,
							searchOpenDialogs: true,
							controlType: "sap.m.Button",
							// visible: false,
							matchers: function (oButton) {
								return oButton.getText() === "Cancel";
							},
							actions: new Press()
						});
					},

					iPressNoButton: function () {
						return this.waitFor({
							controlType: "sap.m.Dialog",
							matchers: function (oDialog) {
								if (oDialog.getTitle() === "Confirm") {
									return oDialog.getEndButton();
								}
								return false;
							},
							actions: new Press(),
							errorMessage: 'Cancelation dialog couldn\'t be closed by pressing "no".'
						});
					},

					iPressYesButton: function () {
						return this.waitFor({
							controlType: "sap.m.Dialog",
							matchers: function (oDialog) {
								if (oDialog.getTitle() === "Confirm") {
									return oDialog.getBeginButton();
								}
								return false;
							},
							actions: new Press(),
							errorMessage: 'Cancelation dialog couldn\'t be closed by pressing "yes".'
						});
					}
				},
				shareOptions.createActions(sViewName)),
			assertions: jQuery.extend({
				theWarningDialogIsClosed: function() {
					//todo
				},
				
				theWarningDialogIsShown: function () {
					return this.waitFor({
						autoWait: true,
						searchOpenDialogs: true,
						controlType: "sap.m.Dialog",
						matchers: function (oDialog) {
							return oDialog.getTitle() === "Warnung";
						},
						success: function (oDialog) {
							Opa5.assert.ok(true, "The warning dialog is shown");
						},
						errorMessage: "The warning dialog is not shown"
					});
				},

				theTableWasFilledCorrectly: function (iIndex) {
					return this.waitFor({
						autoWait: true,
						searchOpenDialogs: true,
						controlType: "sap.m.Table",
						matchers: function (oTable) {
							var aInputs = [];
							var oItem = oTable.getItems()[iIndex];
							var oEcnKeyInput = oItem.getCells()[0].getItems()[0];
							var oPlantKeyInput = oItem.getCells()[1];
							aInputs.push(oEcnKeyInput, oPlantKeyInput);
							return aInputs;
						},
						success: function (aInputs) {
							var oSessionContext = sap.ui.test.Opa5.getContext();
							var oEcnInput = aInputs[0][0];
							var oPlantInput = aInputs[0][1];
							Opa5.assert.strictEqual(oEcnInput.getSelectedKey(), oSessionContext.oEcnToBeAdded.ECN,
								"The ECN of index " + iIndex + " is correct");

							Opa5.assert.strictEqual(oPlantInput.getSelectedKeys().sort(function (a, b) {
								return a - b;
							}).toString(), oSessionContext.oEcnToBeAdded.plants.sort(function (a, b) {
								return a - b;
							}).toString(), "The plants of index " + iIndex + " are correct");
						}
					});
				},

				anEcnCanBeAdded: function () {
					return this.waitFor({
						controlType: "sap.m.InputBase",
						searchOpenDialogs: true,
						matchers: function (oInput) {
							var sInputName = oInput.getName();
							if (sInputName === "ProjectDescription") {
								return oInput;
							}
							return false;
						},
						success: function (aInput) {
							var oInput = aInput[0];
							var oSessionContext = sap.ui.test.Opa5.getContext();
							Opa5.assert.strictEqual(oInput.getValue(), oSessionContext.projectHeaderData.ProjectDescription,
								"The Project Description is correct");
						},
						errorMessage: "The Project Phase Out Date does not match the correct value"
					});
				},

				theProjectDescriptionIsFilled: function () {
					return this.waitFor({
						controlType: "sap.m.InputBase",
						searchOpenDialogs: true,
						matchers: function (oInput) {
							var sInputName = oInput.getName();
							if (sInputName === "ProjectDescription") {
								return oInput;
							}
							return false;
						},
						success: function (aInput) {
							var oInput = aInput[0];
							var oSessionContext = sap.ui.test.Opa5.getContext();
							Opa5.assert.strictEqual(oInput.getValue(), oSessionContext.projectHeaderData.ProjectDescription,
								"The Project Description is correct");
						},
						errorMessage: "The Project Phase Out Date does not match the correct value"
					});
				},

				theProjectManagerIsFilled: function () {
					return this.waitFor({
						controlType: "sap.m.InputBase",
						searchOpenDialogs: true,
						matchers: function (oInput) {
							var sInputName = oInput.getName();
							if (sInputName === "PhaseOutManagerKey") {
								return oInput;
							}
							return false;
						},
						success: function (aInput) {
							var oInput = aInput[0];
							var oSessionContext = sap.ui.test.Opa5.getContext();
							Opa5.assert.strictEqual(oInput.getSelectedKey(), oSessionContext.projectHeaderData.PhaseOutManagerKey,
								"The Project Manager is correct");
						},
						errorMessage: "The Project Phase Out Date does not match the correct value"
					});
				},

				theProjectDateIsFilled: function () {
					return this.waitFor({
						controlType: "sap.m.InputBase",
						searchOpenDialogs: true,
						matchers: function (oInput) {
							var sInputName = oInput.getName();
							if (sInputName === "PhaseOutDate") {
								return oInput;
							}
							return false;
						},
						success: function (aInput) {
							var oInput = aInput[0];
							var oSessionContext = sap.ui.test.Opa5.getContext();
							Opa5.assert.strictEqual(oInput.getValue(), oSessionContext.projectHeaderData.PhaseOutDate,
								"The Project Phase Out Date is correct");
						},
						errorMessage: "The Project Phase Out Date does not match the correct value"
					});
				},

				anEcnIsAddedToTheTable: function () {
					return this.waitFor({
						controlType: "sap.m.Table",
						searchOpenDialogs: true,
						matchers: function (oTable) {
							var aItems = oTable.getItems();
							if (aItems.length === 1) {
								return true;
							}
							return false;
						},
						success: function () {
							Opa5.assert.ok(true, "The Table got a new ECN");
						},
						errorMessage: "The Table has no new Entries"
					});
				},
				onlyThisManyEcnsAreAvaiable: function (iCounter) {
					return this.waitFor({
						id: sEcnTableId,
						viewName: sViewName,
						check: function (oTable) {
							var aItems = oTable.getItems();
							if (aItems.length === iCounter) {
								return true;
							}
							return false;
						},
						success: function () {
							Opa5.assert.ok(true, "The Table got only the requested amount of ECNs");
						},
						errorMessage: "The Table has not the requested amount of Entries"
					});
				},

				theEcnTableIsEmpty: function () {
					return this.waitFor({
						id: sEcnTableId,
						viewName: sViewName,
						check: function (oTable) {
							var aItems = oTable.getItems();
							if (aItems.length === 0) {
								return true;
							}
							return false;
						},
						success: function () {
							Opa5.assert.ok(true, "The Table got reseted");
						},
						errorMessage: "The Table has not been reseted"
					});
				},

				theCancelationPopUpIsDisplayed: function () {
					return this.waitFor({
						controlType: "sap.m.Dialog",
						matchers: function (oDialog) {
							if (oDialog.getTitle() === "Confirm") {
								return true;
							}
							return false;
						},
						check: function (oDialog) {
							return !!oDialog;
						},
						success: function () {
							Opa5.assert.ok(true, "The cancelation dialog was opened");
						},
						errorMessage: "The cancelation dialog couldn't be opened"
					});
				},

				theCancelationPopUpIsDestroyed: function () {
					return this.waitFor({
						controlType: "sap.m.Dialog",
						check: function (aDialogs) {
							var bResponse = false;
							for (var i = 0; i < aDialogs.length; i++) {
								var oDialog = aDialogs[i];
								if (oDialog.getTitle() === "Confirm") {
									return false;
								} else if (oDialog.getTitle() === "PLM Phase Out Project Initialization") {
									bResponse = true;
								}
							}
							return bResponse;
						},
						success: function () {
							Opa5.assert.ok(true, "The cancelation dialog was destroyed");
						},
						errorMessage: "The cancelation dialog couldn't be destroyed"
					});
				},

				theProjectCreationDialogIsDestroyed: function () {
					return this.waitFor({
						viewName: sViewName,
						check: function (aControls) {
							for (var i = 0; i < aControls.length; i++) {
								var oControl = aControls[i];
								if (oControl.getMetadata().getName() === "sap.m.Dialog") {
									if (oControl.getTitle() === "Confirm" || oControl.getTitle() === "PLM Phase Out Project Initialization") {
										return false;
									}
								}
							}
							return true;
						},
						success: function () {
							Opa5.assert.ok(true, "The cancelation dialog and the project creation dialog were destroyed");
						},
						errorMessage: "The cancelation dialog and the project creation dialog couldn't be destroyed"
					});
				}
			}, shareOptions.createAssertions(sViewName))
		}
	});
});