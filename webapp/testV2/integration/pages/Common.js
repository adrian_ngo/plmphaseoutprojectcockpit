sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText"
], function(Opa5, Press, EnterText) {
	"use strict";
	

	return Opa5.extend("com.vaillant.plm.fiori.projectcockpit.test.integration.pages.Common", {
    	

		createAWaitForAnEntitySet : function  (oOptions) {
			return {
				success: function () {
					var aEntitySet;

					var oMockServerInitialized = this.getMockServer().then(function (oMockServer) {
						aEntitySet = oMockServer.getEntitySetData(oOptions.entitySet);
					});

					this.iWaitForPromise(oMockServerInitialized);
					return this.waitFor({
						success : function () {
							oOptions.success.call(this, aEntitySet);
						}
					});
				}
			};
		},

		getMockServer : function () {
			return new Promise(function (success) {
				Opa5.getWindow().sap.ui.require(["com/vaillant/plm/fiori/projectcockpit/localServiceV2/V2/mockserver"], function (mockserver) {
					success(mockserver.getMockServer());
				});
			});
		},

		iShouldSeeTheElementById: function(sElementId,sViewName) {
			return this.waitFor({
			    id: sElementId,
                viewName : sViewName,
                success : function (oTitle) {
                    Opa5.assert.ok(oTitle.getVisible(), "Element visible");
                },
		        errorMessage: "Element not found: id=" + sElementId
			});
		},

		iShouldSeeTheElementContainingId: function(sElementId,sViewName,sControlType) {
			return this.waitFor({
                viewName : sViewName,
				controlType: sControlType,
				matchers: function(oControl) {
					return ( oControl.getId().indexOf( sElementId ) > -1 );
				},
                success : function (oControl) {
                    Opa5.assert.ok(oControl[0].getVisible(), "Element visible: id = " + oControl[0].getId());
                },
		        errorMessage: "Element not found: id = " + sElementId
			});
		},

		iPressButtonById: function(sButtonId,sViewName) {
			return this.waitFor({
				viewName: sViewName,
				controlType: "sap.m.Button",
				matchers: function(oControl) {
					return ( oControl.getId().indexOf( sButtonId ) > -1 );
				},
				actions: new Press(),
		        errorMessage: "Button not found: id=" + sButtonId
			});
		},
		
        iPressDialogButtonById : function (sButtonId) {
            var oOrderNowButton = null;
            this.waitFor({
                searchOpenDialogs : true,
                controlType : "sap.m.Button",
                check : function (aButtons) {
                    return aButtons.filter(function (oButton) {
                        if(oButton.getId().indexOf(sButtonId) > -1 ) {
                            return false;
                        }
        
                        oOrderNowButton = oButton;
                        return true;
                    });
                },
                actions: new Press(),
                errorMessage : "Did not find the dialog button with id containing the string " + sButtonId
            });
            return this;
        },

        iPressDialogButtonByText : function (sButtonText) {
            var oOrderNowButton = null;
            this.waitFor({
                searchOpenDialogs : true,
                controlType : "sap.m.Button",
                check : function (aButtons) {
                    return aButtons.filter(function (oButton) {
                        if(oButton.getText() !== sButtonText) {
                            return false;
                        }
        
                        oOrderNowButton = oButton;
                        return true;
                    });
                },
                actions: new Press(),
                errorMessage : "Did not find the dialog button with name = " + sButtonText
            });
            return this;
        },

    	iShouldSeeTheDialog: function(sDialogId,sViewName) {
    		return this.waitFor({
    			searchOpenDialogs: true,
    			controlType: "sap.m.Dialog",
    			matchers: function(oDialog) {
    			    return (oDialog.getId().indexOf(sDialogId) > -1);
    			},
    			success: function (oDialog) {
    				Opa5.assert.ok(oDialog, "The Dialog shown has the title " + oDialog[0].getTitle());
    			}
    		});
    	},
    	
    	iEnterTextToInputField: function(sNewText,sInputFieldId){
            return this.waitFor({
                id: sInputFieldId,
                actions: [
                    new EnterText({
                        text: sNewText
                    })
                ]
            });
    	},
    	
    	iStartSearching: function(sViewName,sSearchFieldId,sSearchString){
    	    var aActions = [
    		    new EnterText({
    				text: sSearchString
    		    }), 
    		    new Press()
    		];
    		return this.waitFor({
    			id: sSearchFieldId,
    			viewName: sViewName,
    			actions: aActions,
    			errorMessage: "Failed searching for the string " + sSearchString
    		});
    	},
    	
		theControlShouldHaveTheCorrectText: function(sControlType,sViewName,sControlId,sValue){
			return this.waitFor({
				controlType: sControlType,
				viewName: sViewName,
				matchers: function(oControl){
				    return (oControl.getId().indexOf(sControlId) > -1);
				},
				success: function(oControl) {
				    var bCorrectText = (oControl[0].getText().indexOf(sValue) > -1);
					Opa5.assert.ok(bCorrectText, " Element has the correct text: " + sValue);
				},
				errorMessage: "Element not found"
			});
		}

	});

});