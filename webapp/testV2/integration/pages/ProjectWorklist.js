sap.ui.define([
	"sap/ui/test/Opa5",
	"./Common",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/AggregationLengthEquals",
	"sap/ui/test/matchers/AggregationFilled",
	"sap/ui/test/matchers/PropertyStrictEquals"
], function(Opa5, Common, Press, EnterText, AggregationLengthEquals, AggregationFilled, PropertyStrictEquals) {
	"use strict";

	var sViewName = "ProjectWorklist",
		sTableId = "idProjectTable",
		sSearchFieldId = "idSearchField",
		sSomethingThatCannotBeFound = "*#-Q@@||";


	Opa5.createPageObjects({
		onTheProjectWorklistPage : {

			baseClass : Common,

			actions : {

				iRememberTheListItem : function (oListItem) {
					var oBindingContext = oListItem.getBindingContext("ProjectsModel")
					this.getContext().currentItem = {
						bindingPath: oBindingContext.getPath(),
						id: oBindingContext.getProperty("ProjectKey"),
						title: oBindingContext.getProperty("ProjectDescription")
					};
				},
			    
				iRememberTheIdOfListItemAtPosition : function (iPosition) {
					return this.waitFor({
						id : "idProjectTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPosition];
						},
						success : function (oListItem) {
							this.iRememberTheListItem(oListItem);
						},
						errorMessage : "The list does not have an item at the index " + iPosition
					});
				},
				
				iPressOnTheObjectAtPosition : function (iPositon) {
					return this.waitFor({
						id : "idProjectTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						actions : new Press(),
						errorMessage : "List 'list' in view '" + sViewName + "' does not contain an ObjectListItem at position '" + iPositon + "'"
					});
				},

				iDoQuickSearch: function (sSearchString) {
					return this.iStartSearching(sViewName,sSearchFieldId,sSearchString)
				},
				
				iPressTheStatusStopButtonInTheFirstRow: function () {
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
        				matchers: function(oTable) {
        					return ( oTable.getItems()[0].getCells()[5].getItems()[1] );
        				},
        				success: function(oButton){
        				    this.iPressButtonById(oButton.getId(),sViewName);
        				},
						errorMessage: "Can't see the Project Status Stop Button."
					});
				},

				iPressTheStatusDeleteButtonInTheFirstRow: function () {
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
        				matchers: function(oTable) {
        					return ( oTable.getItems()[0].getCells()[5].getItems()[2] );
        				},
        				success: function(oButton){
        				    this.iPressButtonById(oButton.getId(),sViewName);
        				},
						errorMessage: "Can't see the Project Status Delete Button."
					});
				},

				iPressTheStatusUndeleteButtonInTheFirstRow: function () {
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
        				matchers: function(oTable) {
        					return ( oTable.getItems()[0].getCells()[5].getItems()[3] );
        				},
        				success: function(oButton){
        				    this.iPressButtonById(oButton.getId(),sViewName);
        				},
						errorMessage: "Can't see the Project Status Undelete Button."
					});
				},

			},

			assertions : {
			    
				iShouldSeeTheTable: function () {
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
						success: function (oTable) {
							Opa5.assert.ok(oTable, "Found the object Table");
						},
						errorMessage: "Can't see the Projects Table."
					});
				},
			    
				theFirstTableRowShouldShowTheProject: function (sProjectKey) {
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
						success: function (oTable) {
						    var nRows = oTable.getItems().length;
						    var bMatchNumberOfRows = (nRows === 1);
							Opa5.assert.ok(bMatchNumberOfRows, "Number of visible rows is = " + nRows);
						    var bMatchKey = (oTable.getItems()[0].getCells()[0].getItems()[0].getTitle() === sProjectKey);
							Opa5.assert.ok(bMatchKey, "Found the project with ProjectKey = " + sProjectKey);
						},
						errorMessage: "Can't see the Projects Table."
					});
				},
				
				theProjectStatusInRowOneShouldBeDraft: function(){
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
						success: function (oTable) {
						    var bPlayButtonVisible = oTable.getItems()[0].getCells()[5].getItems()[0].getVisible();
							Opa5.assert.ok( bPlayButtonVisible, "Play button visible.");
						    var bDeleteButtonVisible = oTable.getItems()[0].getCells()[5].getItems()[2].getVisible();
							Opa5.assert.ok( bDeleteButtonVisible, "Delete button visible.");
						    var bDraft = (oTable.getItems()[0].getCells()[4].getText() === "Draft");
							Opa5.assert.ok( bDraft, "Project is in draft mode.");
						},
						errorMessage: "Can't see the Projects Table."
					});
				},
			    
			    
				theProjectStatusInRowOneShouldBeDeleted: function(){
					return this.waitFor({
						id: sTableId,
						viewName: sViewName,
						success: function (oTable) {
						    var bPlayButtonVisible = oTable.getItems()[0].getCells()[5].getItems()[3].getVisible();
							Opa5.assert.ok( bPlayButtonVisible, "Revert button visible.");
						    var bDraft = (oTable.getItems()[0].getCells()[4].getText() === "Deleted");
							Opa5.assert.ok( bDraft, "Project is in deleted mode.");
						},
						errorMessage: "Can't see the Projects Table."
					});
				},
			    
			    
				theListShouldBeSortedAscendingOnField : function (sField) {
					function fnCheckSort (oList){
						var oLastValue = null,
							fnSortByField = function (oElement) {
								if (!oElement.getBindingContext()) {
									return false;
								}

								var oCurrentValue = oElement.getBindingContext().getProperty(sField);

								if (oCurrentValue === undefined) {
									return false;
								}

								if (!oLastValue || oCurrentValue >= oLastValue){
									oLastValue = oCurrentValue;
								} else {
									return false;
								}
								return true;
							};

						return oList.getItems().every(fnSortByField);
					}

					return this.waitFor({
						viewName : sViewName,
						id : "list",
						matchers : fnCheckSort,
						success : function() {
							Opa5.assert.ok(true, "Master list has been sorted correctly for field '" + sField + "'.");
						},
						errorMessage : "Master list has not been sorted correctly for field '" + sField + "'."
					});
				}
			}
		}
	});
});