sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/Common",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/shareOptions"
], function(Opa5, Press, EnterText, PropertyStrictEquals, Common, shareOptions) {
	"use strict";

	var sObjectViewNameId = "ProjectObject";
	var sMasterObjectViewNameId = "FCLmaster";
	var sMasterTableId = "masterTable";
	var sSemanticPageId = "page";
	var sEditProjectHeaderDataButtonId = "editProjectHeaderDataButton";
	var sEditProjectHeaderDataDialogId = "editProjectHeaderDataDialog";
	var sEditHeaderDataFormId = "SimpleFormNewProject";
	var sProjectDescriptionInputId = "idProjectDescription";
	var sPhaseOutManagerInputId = "idPhaseOutManagerDisplayName";
	var sPhaseOutDateInputId = "idPhaseOutDate";
	var sHeaderDataApplyButtonId = "idDialogProjectHeaderSaveBtn";
	var sCancelChangesButtonId = "idFooterCancelButton";

	Opa5.createPageObjects({
		onTheObjectPage: {
			baseClass: Common,
			actions: jQuery.extend({

				iPressTheEditProjectHeaderDataButton: function() {
					return this.waitFor({
						id: sEditProjectHeaderDataButtonId,
						viewName: sObjectViewNameId,
						actions: new Press(),
						errorMessage: "The \"Edit Project Header Data\" Button couldn't be pressed"
					});
				},

				iEditTheHeaderData: function(oNewHeaderData) {
					return this.waitFor({
						id: sEditHeaderDataFormId,
						success: function(oForm) {
							var aContent = oForm.getContent();
							for (var i = 0; i < aContent.length; i++) {
								var oInput = aContent[i];
								var sText;

								switch (oInput.getId()) {
									case sProjectDescriptionInputId:
										sText = oNewHeaderData.description;
										break;
									case sPhaseOutManagerInputId:
										sText = oNewHeaderData.manager;
										break;
									case sPhaseOutDateInputId:
										sText = oNewHeaderData.phaseOutDate;
										break;
									default:
										continue;
								}

								this.waitFor({
									id: oInput.getId(),
									actions: new EnterText({
										text: sText
									}),
									errorMessage: "Could not enter text"
								});
							}
						},
						errorMessage: "The project data could not be changed"
					});
				},
				
				iPressHeaderDataApplyButton: function() {
					return this.waitFor({
						id: sHeaderDataApplyButtonId,
						actions: new Press(),
						errorMessage: "Could not apply changes"
					});
				},
				
				iPressCancelChangesButton: function(){
					return this.waitFor({
						id: sCancelChangesButtonId,
						viewName: sObjectViewNameId,
						actions: new Press(),
						errorMessage: "Could not cancel changes"
					});
				},
				
				iPressThePlantButtonOnIndex: function(iIndex) {
					return this.waitFor({
						id: sMasterTableId,
						viewName: sMasterObjectViewNameId,
						matchers: function(oTable){
							var oRow = oTable.getItems()[iIndex];
							var oCell = oRow.getCells()[0];
							var oChangePlantButton = oCell.getItems()[1].getItems()[1];
							return oChangePlantButton;
						},
						actions: new Press(),
						errorMessage: "Could not press the Change Plant button on index: " + iIndex
					});
				},
				
				iSelectTheFollowingPlantsToBeAdded: function(aIndices){
					return this.waitFor({
						controlType: "sap.m.Table",
						searchOpenDialogs: true,
						matchers: function(oDialog){
							//debugger;
						}
					});
				}

			}, shareOptions.createActions(sObjectViewNameId)),

			assertions: jQuery.extend({

					iShouldSeeTheObjectPage: function() {
						return this.waitFor({
							id: sSemanticPageId,
							viewName: sObjectViewNameId,
							success: function(oPage) {
								Opa5.assert.ok(oPage, "View transition to the object page");
							},
							errorMessage: "The view did not change to the object page"
						});
					},

					theTitleShouldBeCorrect: function(sNewTitle) {
						return this.waitFor({
							id: sSemanticPageId,
							viewName: sObjectViewNameId,
							matchers: function(oPage) {
								return oPage.getTitleHeading();
							},
							success: function(oTitleHeading) {
								var oItemContext = this.getContext().currentItem;
								var sExpectation = oItemContext.name + " (" + oItemContext.id + ")";
								if(sNewTitle){
									sExpectation = sNewTitle + " (" + oItemContext.id + ")";
								}
								var sTitle = oTitleHeading.getText();
								Opa5.assert.strictEqual(sTitle, sExpectation, "Title display");
							},
							errorMessage: "The pages title wasn't set correctly"
						});
					},

					theSubTitleShouldBeCorrect: function(sManager, sDate) {
						return this.waitFor({
							id: sSemanticPageId,
							viewName: sObjectViewNameId,
							matchers: function(oPage) {
								return oPage.getTitleExpandedContent();
							},
							success: function(aExpandedContent) {
								var oItemContext = this.getContext().currentItem;
								var sManagerExpectation = sManager || oItemContext.manager;
								var sPhaseOutDateExpectation = sDate || oItemContext.phaseOutDate.toLocaleDateString();
								for (var i = 0; i < aExpandedContent.length; i++) {
									var oContent = aExpandedContent[i];
									switch (oContent.getTitle()) {
										case "Phase Out Manager":
											Opa5.assert.strictEqual(oContent.getText(), sManagerExpectation, "Manager display");
											break;
										case "Phase Out Date":
											Opa5.assert.strictEqual(oContent.getText(), sPhaseOutDateExpectation, "Date display");
											break;
										case "Project Status":
											Opa5.assert.strictEqual(oContent.getText(), oItemContext.projectStatus, "Project status");
											break;
									}
								}
							},
							errorMessage: "The pages subtitle wasn't changed correctly"
						});
					},

					iShouldSeeTheEditProjectHeaderDialog: function() {
						return this.waitFor({
							id: sEditProjectHeaderDataDialogId,
							success: function(oDialog) {
								var oItemContext = this.getContext().currentItem;
								var sExpectation = "Project ID: " + oItemContext.id;
								Opa5.assert.strictEqual(oDialog.getTitle(), sExpectation, "Correct dialog opening test");
							},
							errorMessage: "The correct dialog couldn't be opened"
						});
					},
					
					theProjectHeaderShouldHaveChanged: function(oNewHeaderData){
						this.theTitleShouldBeCorrect(oNewHeaderData.description);
						this.theSubTitleShouldBeCorrect(oNewHeaderData.manager, oNewHeaderData.phaseOutDate);
					},
					
					thePlantDialogForTheIndexShouldBeOpen: function(iIndex){
						return this.waitFor({
							controlType: "sap.m.Dialog",
							matchers: function(oDialog){
								var sId = oDialog.getId();
								if (sId.includes("idSelectPlantsDialog")){
									return oDialog;
								} else {
									return false;
								}
							},
							success: function(oDialog){
								Opa5.assert.ok(oDialog, "The Dialog was opened");
							}
						});
					}
					

					// iShouldSeeTheRememberedObject : function () {
					// 	return this.waitFor({
					// 		success : function () {
					// 			var sBindingPath = this.getContext().currentItem.bindingPath;
					// 			this.waitFor({
					// 				id : "page",
					// 				viewName : sViewNameId,
					// 				matchers : function (oPage) {
					// 					return oPage.getBindingContext("serviceModel") && oPage.getBindingContext("serviceModel").getPath() === sBindingPath;
					// 				},
					// 				success : function (oPage) {
					// 					Opa5.assert.strictEqual(oPage.getBindingContext("serviceModel").getPath(), sBindingPath, "was on the remembered detail page");
					// 				},
					// 				errorMessage : "Remembered object " + sBindingPath + " is not shown"
					// 			});
					// 		}
					// 	});
					// },

					// iShouldSeeTheObjectViewsBusyIndicator : function () {
					// 	return this.waitFor({
					// 		id : "page",
					// 		viewName : sViewNameId,
					// 		matchers : function (oPage) {
					// 			return oPage.getBusy();
					// 		},
					// 		success : function (oPage) {
					// 			Opa5.assert.ok(oPage.getBusy(), "The object view is busy");
					// 		},
					// 		errorMessage : "The object view is not busy"
					// 	});
					// },

					// theViewIsNotBusyAnymore : function () {
					// 	return this.waitFor({
					// 		id : "page",
					// 		viewName : sViewNameId,
					// 		matchers : function (oPage) {
					// 			return !oPage.getBusy();
					// 		},
					// 		success : function (oPage) {
					// 			Opa5.assert.ok(!oPage.getBusy(), "The object view is not busy");
					// 		},
					// 		errorMessage : "The object view is busy"
					// 	});
					// },

					// theObjectViewsBusyIndicatorDelayIsZero : function () {
					// 	return this.waitFor({
					// 		id : "page",
					// 		viewName : sViewNameId,
					// 		success : function (oPage) {
					// 			Opa5.assert.strictEqual(oPage.getBusyIndicatorDelay(), 0, "The object view's busy indicator delay is zero.");
					// 		},
					// 		errorMessage : "The object view's busy indicator delay is not zero."
					// 	});
					// },

					// theObjectViewsBusyIndicatorDelayIsRestored : function () {
					// 	return this.waitFor({
					// 		id : "page",
					// 		viewName : sViewNameId,
					// 		matchers: new PropertyStrictEquals({
					// 			name : "busyIndicatorDelay",
					// 			value: 1000
					// 		}),
					// 		success : function () {
					// 			Opa5.assert.ok(true, "The object view's busy indicator delay default is restored.");
					// 		},
					// 		errorMessage : "The object view's busy indicator delay is still zero."
					// 	});
					// },

					// theShareTileButtonShouldContainTheRememberedObjectName : function () {
					// 	return this.waitFor({
					// 		id : "shareTile",
					// 		viewName : sViewNameId,
					// 		matchers : function (oButton) {
					// 			var sObjectName = this.getContext().currentItem.name;
					// 			var sTitle = oButton.getTitle();
					// 			return sTitle && sTitle.indexOf(sObjectName) > -1;
					// 		}.bind(this),
					// 		success : function () {
					// 			Opa5.assert.ok(true, "The Save as Tile button contains the object name");
					// 		},
					// 		errorMessage : "The Save as Tile did not contain the object name"
					// 	});
					// }

				},
				shareOptions.createAssertions(sObjectViewNameId))

		}

	});

});