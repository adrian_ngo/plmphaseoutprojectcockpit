sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"./Common",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/AggregationLengthEquals",
	"sap/ui/test/matchers/AggregationFilled",
    "sap/ui/test/matchers/Ancestor",
    "sap/ui/test/matchers/Properties",
	"sap/ui/test/matchers/PropertyStrictEquals"
], function(
    Opa5, 
    Press, 
    Common, 
    EnterText, 
    AggregationLengthEquals, 
    AggregationFilled, 
    Ancestor,
    Properties,
    PropertyStrictEquals) {
	"use strict";

	var sViewName = "FCLmaster";
	var sSomethingThatCannotBeFound = "*#-Q@@||";
	
	Opa5.createPageObjects({
		onTheMasterPage : {

			baseClass : Common,

			actions : {

				iRememberTheListItem : function (oListItem) {
					var oBindingContext = oListItem.getBindingContext("serviceModel")
					this.getContext().currentItem = {
						bindingPath: oBindingContext.getPath(),
						id: oBindingContext.getProperty("EcnKey"),
						title: oBindingContext.getProperty("EcnDescription")
					};
				},
			    
				iRememberTheIdOfListItemAtPosition : function (iPosition) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPosition];
						},
						success : function (oListItem) {
							this.iRememberTheListItem(oListItem);
						},
						errorMessage : "The list does not have an item at the index " + iPosition
					});
				},
				
				iPressOnTheObjectAtPosition : function (iPositon) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						actions : new Press(),
						errorMessage : "List 'list' in view '" + sViewName + "' does not contain an ObjectListItem at position '" + iPositon + "'"
					});
				},
				
				iPressOnTheFactoryButtonAtPosition : function (iPositon) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						success : function (oListItem) {
						    var index = 1; // factory button is the first one
						    var sButtonId = oListItem.getCells()[0].getItems()[1].getItems()[index].sId;
							this.iPressButtonById(sButtonId,sViewName);
						},
						errorMessage : "Master table: Problem with iPressOnTheFactoryButtonAtPosition on first ECN item"
					});
				},

				iPressOnTheDeleteButtonAtPosition : function (iPositon) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						success : function (oListItem) {
						    var index = 2; // delete button is the next one
						    var sButtonId = oListItem.getCells()[0].getItems()[1].getItems()[index].sId;
							this.iPressButtonById(sButtonId,sViewName);
						},
						errorMessage : "Master table: Problem with iPressOnTheDeleteButtonAtPosition on first ECN item"
					});
				},

				iPressOnTheUndoDeleteButtonAtPosition : function (iPositon) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						success : function (oListItem) {
						    var index = 2; // still the same delete button!
						    var sButtonId = oListItem.getCells()[0].getItems()[1].getItems()[index].sId;
							this.iPressButtonById(sButtonId,sViewName);
						},
						errorMessage : "Master table: Problem with iPressOnTheUndoDeleteButtonAtPosition on first ECN item"
					});
				},

				iPressOnTheCancelButtonOnTheSelectPlantsDialogA : function () {
					return this.waitFor({
            			searchOpenDialogs: true,
            			controlType: "sap.m.Dialog",
            			matchers: function(oDialog) {
            			    var sDialogId = "idSelectPlantsDialog";
            			    return (oDialog.getId().indexOf(sDialogId) > -1);
            			},
						success : function () {
						    var sButtonId = "idSelectPlantsDialog";
							this.iPressButtonById(sButtonId,sViewName);
						},
						errorMessage : "Problem with iPressOnTheCancelButtonOnTheSelectPlantsDialog()"
					});
				},

				iPressOnTheCancelButtonOfTheSelectPlantsDialog : function () {
				    var sButtonId = "idCancelDialogSelectPlants";
				    //var sViewName = "idSelectPlantsDialog";
					return this.iPressButtonById(sButtonId,sViewName);
				},
				
				iPressTheAddECNsButton: function(){
				    var sButtonId = "idAddECNs";
					return this.iPressButtonById(sButtonId,sViewName);
				},

				iPressTheAddECNButtonOfTheECNSelectionDialog: function(){
				    var sButtonId = "idAddOneECN";
					return this.iPressButtonById(sButtonId,sViewName);
				},

				iPressTheCancelEcnSelectionButton: function(){
				    var sButtonId = "idCancelEcnSelection";
					//return this.iPressButtonById(sButtonId,sViewName);
					return this.waitFor({
						viewName : sViewName,
						success : function (oListItem) {
        					return this.iPressDialogButtonById(sButtonId,sViewName);
						},
						error: function (oError) {
							Opa5.assert.ok("iPressTheCancelEcnSelectionButton");
						},
						errorMessage : "Problems with iPressTheCancelEcnSelectionButton"
					});
				},
				
				iConfirmClosingTheEcnSelectionDialog: function(){
				    var sButtonId = "idConfirmYes";
					//return this.iPressDialogButtonById(sButtonId,sViewName);
					
					return this.waitFor({
						viewName : sViewName,
						success : function (oListItem) {
        					return this.iPressDialogButtonById(sButtonId,sViewName);
						},
						errorMessage : "Problems with iConfirmClosingTheEcnSelectionDialog"
					});
					
				},
				
				iEnterTextInEcnSearchField: function(sNewText){

                    var sControlType = "sap.m.Input";
                    var sElementId = "idInputEcn";
                    //var sViewName = "SelectECNsForProject";
                    
        			return this.waitFor({
                        viewName : sViewName,
        				controlType: sControlType,
        				matchers: function(oControl) {
        					return ( oControl.getId().indexOf( sElementId ) > -1 );
        				},
                        actions: new EnterText({
                            text: sNewText
                        }),
                        /*
                        success: function (oInput) {
                            // Select a suggestion by pressing an item with text "John".
                            // After the press action, the value of the input should be changed to "John".
                            // Note that the focus will remain in the input field.
                            this.waitFor({
                                controlType: "sap.ui.core.Item",
                                matchers: function(oItem){
                                    return (oItem.getId());
                                },
                                actions: new Press(),
		        		        errorMessage: "Problem finding ECN suggestion list - maybe too early!"
                            });
                        },
                        */
        		        errorMessage: "Element containing id not found: id = " + sElementId
        			});

				},


			},

			assertions : {
			    
			    theFactoryButtonShouldBeDisabledAtPosition: function (iPositon) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						success : function (oListItem) {
							var bEnabled = oListItem.getCells()[0].getItems()[1].getItems()[1].getEnabled();
							Opa5.assert.ok(!bEnabled, "The Factory Button is disabled.");
						},
						errorMessage : "Master table in view '" + sViewName + "' does not contain an ECN Item at position '" + iPositon + "'"
					});
				},

			    theFactoryButtonShouldBeEnabledAtPosition: function (iPositon) {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getItems()[iPositon];
						},
						success : function (oListItem) {
							var bEnabled = oListItem.getCells()[0].getItems()[1].getItems()[1].getEnabled();
							Opa5.assert.ok(bEnabled, "The Factory Button is enabled.");
						},
						errorMessage : "Master table in view '" + sViewName + "' does not contain an ECN Item at position '" + iPositon + "'"
					});
				},


				theListShouldHaveNoSelection : function () {
					return this.waitFor({
						id : "masterTable",
						viewName : sViewName,
						matchers : function(oList) {
							return !oList.getSelectedItem();
						},
						success : function (oList) {
							Opa5.assert.strictEqual(oList.getSelectedItems().length, 0, "The list is not selected.");
						},
						errorMessage : "List selection was not removed"
					});
				},

				iShouldSeeTheSelectPlantsDialog : function () {
					return this.iShouldSeeTheDialog("idSelectPlantsDialog",sViewName);
				},

				iShouldSeeTheECNSelectionDialog: function () {
					return this.iShouldSeeTheDialog("idSelectECNsDialog",sViewName);
				},
				
				iShouldSeeTheECNSearchField: function () {
					return this.iShouldSeeTheElementContainingId("idInputEcn",sViewName,"sap.m.Input");
				},
				
				iShouldSeeTheWarningDialog: function(){
				    var sDialogId = "idConfirmationDialog";
				    return this.iShouldSeeTheDialog(sDialogId,sViewName);
				},
				
				iShouldSeeTheECNValueHelp: function () {
                    var sControlType = "sap.ui.core.Item";
        			return this.waitFor({
                        viewName : sViewName,
        				controlType: sControlType,
        				matchers: function(oItem){
                            return (oItem.getKey()==="ECN000000126");
        				},
                        actions: new Press(),
        		        errorMessage: "Problem with iShouldSeeTheECNValueHelp()"
        			});
				},
				
				theEcnSearchFieldShouldHaveTheSuggestion: function(sEcnKey){

                    var sControlType = "sap.m.Input";
                    var sElementId = "idInputEcn";
                    //var sViewName = "SelectECNsForProject";
                    
        			return this.waitFor({
                        viewName : sViewName,
        				controlType: sControlType,
        				matchers: function(oControl) {
        					return ( oControl.getId().indexOf( sElementId ) > -1 );
        				},
                        success: function (oInput) {
                            var aSuggestionItems = oInput[0].getSuggestionItems();
                            var bFoundECN = (aSuggestionItems[0].getKey() === sEcnKey);
							Opa5.assert.ok(bFoundECN, "Suggestion Items found: ECN Key = " + sEcnKey);
                            /*
                            this.waitFor({
                                controlType: "sap.ui.core.Item",
                                matchers: function(oItem){
                                    return (oItem.getId());
                                },
                                actions: new Press(),
		        		        errorMessage: "Problem finding ECN suggestion list - maybe too early!"
                            });
                            */
                        },
        		        errorMessage: "Element containing id not found: id = " + sElementId
        			});

				},
				

			}
		}
	});
});