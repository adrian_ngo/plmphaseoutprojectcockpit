sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"./Common",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/AggregationLengthEquals",
	"sap/ui/test/matchers/AggregationFilled",
	"sap/ui/test/matchers/PropertyStrictEquals"
], function(Opa5, Press, Common, EnterText, AggregationLengthEquals, AggregationFilled, PropertyStrictEquals) {
	"use strict";

	var sViewName = "FCLinfo";
	var sSomethingThatCannotBeFound = "*#-Q@@||";
	
	Opa5.createPageObjects({
		onTheDetailDetailPage : {

			baseClass : Common,

			actions : {

                iNavigateToSection: function (idSection) {
					return this.waitFor({
						id: "detailDetailPage",
						viewName: sViewName,
						success: function (oObjectPageLayout) {
						    //oObjectPageLayout.getSelectedSection(idSection);
						    //var iid = oObjectPageLayout.getSelectedSection();
						    var aSections = oObjectPageLayout.getSections();
						    var oSection = aSections.filter(function(item){
						        return item.sId.indexOf(idSection) > -1;
						    })[0];
						    oObjectPageLayout.scrollToSection( oSection.sId, 500, 0 );
						},
						//actions: new Press(),
						errorMessage: "Did not find the nav button on object page"
					});
				},
				
				iPressCloseButton: function(){
					return this.waitFor({
						id: "idCloseBtnMasterDetails",
						viewName: sViewName,
						actions: new Press(),
						errorMessage: "Did not find the Close button on the object page"
					});
				    
				}

			},

			assertions : {
			    
			    theSubsectionShouldOccur: function(idSection){
					return this.waitFor({
						id: "detailDetailPage",
						viewName: sViewName,
						success: function (oObjectPageLayout) {
						    var sFoundSectionId = oObjectPageLayout.getScrollingSectionId();
						    var bFound = (sFoundSectionId.indexOf(idSection) > -1) ;
							Opa5.assert.ok(bFound, "Found the section with id=" + sFoundSectionId);
						},
						error: function(oError){
						    console.log(oError);
						}
					});
			    },

				iShouldSeeTheInfoPage: function () {
					return this.waitFor({
						viewName: sViewName,
						id: "detailDetailPage",
						success: function (oPage) {
							Opa5.assert.ok(oPage, "Found the info page");
						},
						error: function(oError){
						    console.log(oError);
						}
					});
				}


			}
		}
	});
});