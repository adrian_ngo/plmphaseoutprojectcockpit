sap.ui.define([
	"sap/ui/test/Opa5",
	"./Common",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/PropertyStrictEquals"
], function(Opa5, Common, Press, EnterText, PropertyStrictEquals) {
	"use strict";

	var sViewName = "ProjectObject";

	Opa5.createPageObjects({
		onTheProjectObjectPage : {

			baseClass : Common,

			actions : {

				iPressTheSaveButton: function() {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.f.semantic.FooterMainAction",
						matchers: function(oControl) {
							return (oControl.getProperty("visible"));
						},
						actions: new Press(),
						errorMessage: "The button \"Save\" was not found in the footer"
					});
				},
				
				iPressTheFooterSaveButton: function(){
				    var sButtonId = "idFooterSaveButton";
					return this.iPressButtonById(sButtonId,sViewName);
				},
				

				iPressTheEditProjectHeaderButton: function(){
				    var sButtonId = "editProjectHeaderDataButton";
					return this.iPressButtonById(sButtonId,sViewName);
				},
				
				iPressOnTheCloseButtonOfTheEditProjectHeaderDialog: function () {
				    var sButtonText = "Close";
					return this.iPressDialogButtonByText(sButtonText);
				},
				
				iPressOnTheSaveButtonOfTheEditProjectHeaderDialog: function () {
				    var sButtonText = "Save";
					return this.iPressDialogButtonByText(sButtonText);
				},
				
				iModifyTheProjectDescription: function(sNewText){
				    return this.iEnterTextToInputField(sNewText,"idObjectHeaderData--idProjectDescription");
				},

				iCloseTheMessageBox : function () {
					return this.waitFor({
						id: "serviceErrorMessageBox",
						autoWait: false,
						success: function (oMessageBox) {
							oMessageBox.destroy();
							Opa5.assert.ok(true, "The MessageBox was closed");
						}
					});
				}
			},

			assertions : {

				iShouldSeeTheBusyIndicator : function () {
					return this.waitFor({
						id : sAppControl,
						viewName : sViewName,
						matchers: new PropertyStrictEquals({
							name: "busy",
							value: true
						}),
						autoWait: false,
						success : function () {
							Opa5.assert.ok(true, "The app is busy");
						},
						errorMessage : "The app is not busy"
					});
				},

				theAppShowsFCLDesign: function (sLayout) {
					return this.waitFor({
						id : "fcl",
						viewName : sViewName,
						matchers : new PropertyStrictEquals({
						    name: "layout", 
						    value: sLayout
						}),
						success : function () {
							Opa5.assert.ok(true, "the app shows " + sLayout + " layout");
						},
						errorMessage : "The app does not show " + sLayout + " layout"
					});
				},
				
				iShouldSeeTheEndView: function () {
					return this.waitFor({
						viewName: sViewName,
						id: "endView",
						success: function (oPage) {
							Opa5.assert.ok(oPage, "Found the info page");
						},
						error: function(oError){
						    console.log(oError);
						}
					});
				},
				
				iShouldSeeTheFooterBar: function(){
				    var sFooterId = "__component0---projectObject--page-pageFooter";
				    return this.iShouldSeeTheElementById(sFooterId,sViewName);
				},
				
				theFooterIsVisible: function(bVisible){
					return this.waitFor({
						//controlType: "sap.f.semantic.SemanticPage",
						controlType: "sap.uxap.ObjectPageLayout",
						viewName: sViewName,
						matchers: new sap.ui.test.matchers.PropertyStrictEquals({
							name: "showFooter",
							value: bVisible
						}),
						success: function() {
							Opa5.assert.ok(true, bVisible ? "The footer is shown" : "The footer is hidden");
						},
						errorMessage: bVisible ? "Faild to find the footer" : "The footer is shown"
					});
				},

				iShouldSeeTheProjectHeaderDialog : function () {
					return this.iShouldSeeTheDialog("idEditProjectHeaderDialog",sViewName);
				},
				
				iShouldSeeTheNewProjectDescription: function(sNewText){
				    return this.waitFor({
                        success : function () {
        				    return this.theControlShouldHaveTheCorrectText(
        				        "sap.m.Title",
        				        sViewName,
        				        "idProjectTitle",
        				        sNewText
        				    );
                        }
                    });
				},

				iShouldSeeTheECNSearchField: function(){
				    var sControlId = "idInputEcn";
        			return this.waitFor({
        				controlType: "sap.m.Input",
        				viewName: sViewName,
        				matchers: function(oControl){
        				    return (oControl.getId().indexOf(sControlId) > -1);
        				},
        				success: function(oControl) {
        				    var bCorrectText = (oControl[0].getText().indexOf(sValue) > -1);
        					Opa5.assert.ok(bCorrectText, " Element has the correct text: " + sValue);
        				},
        				errorMessage: "Element not found"
        			});
				},

				iShouldSeeTheMessageBox : function () {
					return this.waitFor({
						searchOpenDialogs: true,
						controlType: "sap.m.Dialog",
						matchers : new PropertyStrictEquals({ name: "type", value: "Message"}),
						success: function () {
							Opa5.assert.ok(true, "The correct MessageBox was shown");
						}
					});
				}

			}

		}

	});

});

