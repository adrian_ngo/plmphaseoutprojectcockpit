sap.ui.define([
	"sap/ui/test/Opa5",
	"./Common",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText",
	"sap/ui/test/matchers/AggregationLengthEquals",
	"sap/ui/test/matchers/AggregationFilled",
	"sap/ui/test/matchers/PropertyStrictEquals"
], function(Opa5, Common, Press, EnterText, AggregationLengthEquals, AggregationFilled, PropertyStrictEquals) {
	"use strict";

	var sViewName = "FCLdetail";
	var sSomethingThatCannotBeFound = "*#-Q@@||";
	
	Opa5.createPageObjects({
		onTheDetailPage : {

			baseClass : Common,

			actions : {

				iRememberTheListItem : function (oListItem) {
					var oBindingContext = oListItem.getBindingContext("serviceModel")
					this.getContext().currentItem = {
						bindingPath: oBindingContext.getPath(),
						id: oBindingContext.getProperty("ProjectItemKey"),
						title: oBindingContext.getProperty("MaterialDescription")
					};
				},
			    
				iRememberTheIdOfListItemAtPosition : function (iPosition) {
					return this.waitFor({
						id : "TableBinding",
						viewName : sViewName,
						matchers : function (oList) {
							return oList.getRows()[iPosition];
						},
						success : function (oListItem) {
							this.iRememberTheListItem(oListItem);
						},
						errorMessage : "The table does not have a project-item at the index " + iPosition
					});
				},
				
				iPressOnTheObjectAtPosition : function (iPositon) {
					return this.waitFor({
						id : "TableBinding",
						viewName : sViewName,
						/*
						matchers : function (oTable) {
							//return oList.getRows()[iPositon];
							return oTable;
						},
						*/
						success: function (oTable) {
						    oTable.setSelectedIndex(iPositon);
							//oListItem.fireEvent("cellClick");
							//istItem.$().trigger("press");
						},
						//actions : new Press(),
						errorMessage : "Table with id='TableBinding' in view '" + sViewName + "' does exist!"
					});
				},
				
				
				iPressTheDQwarningButton: function(sButtonId) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.Button",
						matchers: function(oControl) {
							return (oControl.getId().indexOf("idDQwarningButton") > -1);
						},
						actions: new Press(),
						errorMessage: "The button was not found"
					});
				},

				iPressTheMaterialLink: function(sMaterialId,sPlantId) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.Link",
						matchers: function(oControl) {
							return (oControl.getText().indexOf(sMaterialId) > -1);
						},
						actions: new Press(),
						errorMessage: "The Link was not found"
					});
				},

				iPressThePlantLink: function(sMaterialId,sPlantId) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.Link",
						matchers: function(oControl) {
							return (oControl.getText().indexOf(sPlantId) > -1);
						},
						actions: new Press(),
						errorMessage: "The Link was not found"
					});
				},

				iPressThePhaseOutButton: function(sButtonId) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.Button",
						matchers: function(oControl) {
							return (oControl.getProperty("visible") && 
							    oControl.getProperty("icon") === "sap-icon://decline");
						},
						actions: new Press(),
						errorMessage: "The PhaseOut button was not found"
					});
				}

			},

			assertions : {

				theListShouldHaveNoSelection : function () {
					return this.waitFor({
						id : "TableBinding",
						viewName : sViewName,
						matchers : function(oList) {
							return oList.getSelectedIndex()===-1;
						},
						success : function (oList) {
							Opa5.assert.strictEqual(oList.getSelectedIndices().length, 0, "The list is not selected.");
						},
						errorMessage : "List selection was not removed"
					});
				},
				
				theDQMessagePopoverShouldOccur: function (sIdPopover) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.ResponsivePopover",
						matchers: function(oControl) {
							return (oControl.getId().indexOf("idDQpopover") > -1);
						},
						success: function (oPage) {
							Opa5.assert.ok(oPage, "The DQ error message shows up");
						},
						error: function(oError){
						    console.log(oError);
						},
						errorMessage: "The DQ message popover was not found"
					});
				},
				
				theMaterialPopoverShouldOccur: function (sIdPopover) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.ResponsivePopover",
						matchers: function(oControl) {
							return (oControl.getId().indexOf("popover") > -1);
						},
						success: function (oPage) {
							Opa5.assert.ok(oPage, "The material popover shows up");
						},
						error: function(oError){
						    console.log(oError);
						},
						errorMessage: "The material popover was not found"
					});
				},
				
				thePlantPopoverShouldOccur: function (sIdPopover) {
					return this.waitFor({
						viewName: sViewName,
						controlType: "sap.m.ResponsivePopover",
						matchers: function(oControl) {
							return (oControl.getId().indexOf("popover") > -1);
						},
						success: function (oPage) {
							Opa5.assert.ok(oPage, "The plant popover shows up");
						},
						error: function(oError){
						    console.log(oError);
						},
						errorMessage: "The plant popover was not found"
					});
				}

			}
		}
	});
});