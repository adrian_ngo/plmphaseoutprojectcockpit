/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
	"use strict";

	sap.ui.require([
		"com/vaillant/plm/fiori/projectcockpit/testV2/integration/AllJourneys"
	], function() {
		QUnit.start();
	});
});