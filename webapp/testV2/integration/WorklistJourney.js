sap.ui.define([
	"sap/ui/test/opaQunit",
	"./pages/Worklist"
], function(opaTest) {
	"use strict";

	QUnit.module("Project Cockpit");

	var sStatusPublished = "Published";
	var sStatusDraft = "Draft";
	var sStatusInProcess = "In Process";
	var oProjectSearchCriteria = {
		filterId: "",
		filterDescr: "",
		filterManager: "",
		filterDate: "", // Date in the following Format as a string: "DD.MM.YYYY".
		filterDocStatus: "" // Use only sStatusPublished, sStatusDraft, sStatusInProcess.
	};
	
	// Used to reset the project search criteria for further testings
	var fnResetProjectSearchCriteria = function(){
		oProjectSearchCriteria = {
			filterId: "",
			filterDescr: "",
			filterManager: "",
			filterDate: "",
			filterDocStatus: ""
		};
	};

	opaTest("Should see the table with all entries", function(Given, When, Then) {
		// Arrangements
		Given.iStartMyApp();

		//Actions
		When.onTheWorklistPage.iLookAtTheScreen();

		// Assertions
		Then.onTheWorklistPage.theTableShouldHaveAllEntries().
		and.theTitleShouldDisplayTheTotalAmountOfItems();
	});
/*
	opaTest("Testing all filters", function(Given, When, Then) {
		//Actions
		// Creation of search criteria
		oProjectSearchCriteria.filterDate = "22.10.2008";
		oProjectSearchCriteria.filterDocStatus = sStatusInProcess;
		
		When.onTheWorklistPage.iSearchForProjects(oProjectSearchCriteria);

		// Assertions
		Then.onTheWorklistPage.theTableDisplaysOnlyProjectsBasedOnSearchCriteria();
		
		fnResetProjectSearchCriteria();
		oProjectSearchCriteria.filterDate = "22.10.2008";
		oProjectSearchCriteria.filterDocStatus = sStatusPublished;

		When.onTheWorklistPage.iSearchForProjects(oProjectSearchCriteria);

		Then.onTheWorklistPage.theTableDisplaysOnlyProjectsBasedOnSearchCriteria();

		//Reset of all filters for further testings
		When.onTheWorklistPage.iResetSearchTermValues()
			.and.iPressForSearch()
			.and.iWaitUntilTheTableIsUpdated();
	});

	opaTest("Testing table status buttons", function(Given, When, Then) {
		//Actions
		var aPositionsToBeTested = [0, 1, 2, 3, 4];
		When.onTheWorklistPage.iPressStatusButtonAtPosition(aPositionsToBeTested);

		// Assertions
		Then.onTheWorklistPage.theStatusChanged(aPositionsToBeTested);
	});
	
	opaTest("Testing project creation", function(Given, When, Then) {
		//Actions
		When.onTheWorklistPage.iPressCreateProject();

		// Assertions
		Then.onTheWorklistPage.theProjectCreationScreenOpened();
	});
*/

	// opaTest("Searching for projects with the description 'ProjectDescription 3'. " +
	// 	"The results should deliver all Projects that contains 'ProjectDescription 3' in the description.",
	// 	function(Given, When, Then) {
	// 		//Actions
	// 		When.onTheWorklistPage.iSearchForProjectsByProjectDescription("ProjectDescription 3");

	// 		// Assertions
	// 		Then.onTheWorklistPage.theTableShowsOnlyObjectsWithTheSearchTermInProjectDescription();
	// 	});

	// opaTest("Searching for projects with the manager 'PhaseOutManagerDisplayName 5'. " +
	// 	"The results should deliver all Projects that contains 'PhaseOutManagerDisplayName 5' in the description.",
	// 	function(Given, When, Then) {
	// 		//Actions
	// 		When.onTheWorklistPage.iSearchForProjectsByProjectManager("PhaseOutManagerDisplayName 5");

	// 		// Assertions
	// 		Then.onTheWorklistPage.theTableShowsOnlyObjectsWithTheSearchTermInProjectDescription();
	// 	});

	// opaTest("Entering something that cannot be found into search field and pressing search field's refresh should leave the list as it was", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheWorklistPage.iTypeSomethingInTheSearchThatCannotBeFoundAndTriggerRefresh();

	// 	// Assertions
	// 	Then.onTheWorklistPage.theTableHasEntries();
	// });

	// opaTest("Should open the share menu and display the share buttons", function (Given, When, Then) {
	// 	// Actions
	// 	When.onTheWorklistPage.iPressOnTheShareButton();

	// 	// Assertions
	// 	Then.onTheWorklistPage.iShouldSeeTheShareEmailButton().
	// 		and.iTeardownMyAppFrame();
	// });

	// opaTest("Should see the busy indicator on app view while worklist view metadata is loaded", function (Given, When, Then) {
	// 	// Arrangements
	// 	Given.iStartMyApp({
	// 		delay: 5000
	// 	});

	// 	//Actions
	// 	When.onTheWorklistPage.iLookAtTheScreen();

	// 	// Assertions
	// 	Then.onTheAppPage.iShouldSeeTheBusyIndicatorForTheWholeApp();
	// });

	// opaTest("Should see the busy indicator on worklist table after metadata is loaded", function (Given, When, Then) {
	// 	//Actions
	// 	When.onTheAppPage.iWaitUntilTheAppBusyIndicatorIsGone();

	// 	// Assertions
	// 	Then.onTheWorklistPage.iShouldSeeTheWorklistTableBusyIndicator().
	// 		and.iTeardownMyAppFrame();
	// });

});