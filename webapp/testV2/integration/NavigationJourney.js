sap.ui.define([
	"sap/ui/test/opaQunit",
	"sap/ui/test/actions/Press",
	"sap/ui/test/actions/EnterText",
	"./pages/Common",
	"./pages/Browser",
	"./pages/App",
	"./pages/ProjectWorklist",
	"./pages/ProjectObject",
	"./pages/FCLmaster",
	"./pages/FCLdetail",
	"./pages/FCLinfo"
], function (opaTest,Press,EnterText) {
	"use strict";

	QUnit.module("Navigation");
	
	
    /**************************************************************************
     * 
     * Test Quick Search
     * 
     * 
     **************************************************************************/

	opaTest("Enter 'vwl' in SearchField - Should see only one project item", function (Given, When, Then) {
		// Arrangements
		Given.iStartMyApp({delay : 1000});
		// Actions
        var sSearchString = "vwl";
		When.onTheProjectWorklistPage.iDoQuickSearch(sSearchString);
		// Assertions
		Then.onTheProjectWorklistPage.iShouldSeeTheTable().
		and.theFirstTableRowShouldShowTheProject("1111111546");
	});

	opaTest("Press Stop Button in first Row - Should switch to Draft Mode", function (Given, When, Then) {
		// Actions
		When.onTheProjectWorklistPage.iPressTheStatusStopButtonInTheFirstRow();
		// Assertions
		Then.onTheProjectWorklistPage.theProjectStatusInRowOneShouldBeDraft();
	});

	opaTest("Press Delete Button in first Row - Should switch to Deleted Mode", function (Given, When, Then) {
		// Actions
		When.onTheProjectWorklistPage.iPressTheStatusDeleteButtonInTheFirstRow();
		// Assertions
		Then.onTheProjectWorklistPage.theProjectStatusInRowOneShouldBeDeleted();
	});

	opaTest("Press Undelete Button in first Row - Should switch back to Draft Mode", function (Given, When, Then) {
		// Actions
		When.onTheProjectWorklistPage.iPressTheStatusUndeleteButtonInTheFirstRow();
		// Assertions
		Then.onTheProjectWorklistPage.theProjectStatusInRowOneShouldBeDraft();
	});

	opaTest("Empty the SearchField - Should see all four projects", function (Given, When, Then) {
		// Actions
        var sSearchString = "";
		When.onTheProjectWorklistPage.iDoQuickSearch(sSearchString);
		// Assertions
		Then.onTheProjectWorklistPage.iShouldSeeTheTable();
	});


	
    /**************************************************************************
     * 
     * Test ECN Selection Dialog
     * 
     * 
     **************************************************************************/
	
	opaTest("Click on the second project - Should navigate into the list of ECNs", function (Given, When, Then) {
		// Actions
		When.onTheProjectWorklistPage.iRememberTheIdOfListItemAtPosition(1).
			and.iPressOnTheObjectAtPosition(1);
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("OneColumn");
		Then.onTheMasterPage.theListShouldHaveNoSelection();
	});

	opaTest("Press Add ECNs Button - Should see the ECN SelectionDialog with ECN Search Field", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressTheAddECNsButton().and.iPressTheAddECNButtonOfTheECNSelectionDialog();
		// Assertions
		//Then.onTheMasterPage.iShouldSeeTheECNSelectionDialog().and.iShouldSeeTheECNSearchField();
		Then.onTheMasterPage.iShouldSeeTheECNSelectionDialog();
	});
	
	/*
	// Problem with this test: The Suggestion List does not show up in the OPA Test!
	
	opaTest("Enter 12 in the ECN search field - Should see the ECN Value Help", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iEnterTextInEcnSearchField("12");
		// Assertions
		Then.onTheMasterPage.theEcnSearchFieldShouldHaveTheSuggestion("ECN000000126");
    	// Cleanup
		//Then.iTeardownMyApp();
	});
	*/
	

	opaTest("ECN Selection Dialog: Press Cancel - Should show Warning Dialog", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressTheCancelEcnSelectionButton();
		// Assertions
		Then.onTheMasterPage.iShouldSeeTheWarningDialog();
	});
	
	opaTest("Confirm: Press Yes - Should close the ECN Selection Dialog and show the list of ECNs", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iConfirmClosingTheEcnSelectionDialog()
		//.and.iConfirmClosingTheEcnSelectionDialog()
		;
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("OneColumn");
	});

/*
	opaTest("Select the ECN - The MultiComboBox should have three plants listed ", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iSelectTheECNValueHelp();
		// Assertions
		Then.onTheProjectObjectPage.iShouldSeeTheMultiComboBoxWithPlants();
	});
	
	opaTest("Press Select the ECN - The MultiComboBox should have three plants listed ", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressTheSelectAllButton().and.iPressTheSaveButton();
		// Assertions
		Then.onTheProjectObjectPage.iShouldSeeTheEcnSelectionMessageToast();
	});
*/

	opaTest("Press on the Browser Back Button - Should see the list of projects again", function (Given, When, Then) {
		// Actions
		When.onTheBrowser.iPressOnTheBackwardsButton();
		// Assertions
		Then.onTheProjectWorklistPage.iShouldSeeTheTable();
	});



    /**************************************************************************
     * 
     * - Go to another project
     * - Test the Edit Header Dialog
     * - Test Modify ECN Button
     * - Test Delete ECN / Revert Delete Button
     * - Test the Phase Out Buttons
     * 
     **************************************************************************/

	opaTest("Click on the first project - Should navigate into the list of ECNs", function (Given, When, Then) {
		// Actions
		When.onTheProjectWorklistPage.iRememberTheIdOfListItemAtPosition(0).
			and.iPressOnTheObjectAtPosition(0);

		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("OneColumn");
		Then.onTheMasterPage.theListShouldHaveNoSelection();

	});
	
	opaTest("Press Edit Project Header Button - Should show the Edit Project Header  Dialog", function (Given, When, Then) {
		// Actions
		When.onTheProjectObjectPage.iPressTheEditProjectHeaderButton();
		// Assertions
		Then.onTheProjectObjectPage.iShouldSeeTheProjectHeaderDialog();
	});

	opaTest("Press Close Button - Should close the Edit Project Header Dialog", function (Given, When, Then) {
		// Actions
		When.onTheProjectObjectPage.iPressOnTheCloseButtonOfTheEditProjectHeaderDialog();
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("OneColumn");
		//Then.onTheMasterPage.iShouldSeeTheSelectPlantsDialog();
	});

	opaTest("Modify Project Header - Should show the new Project Header Data", function (Given, When, Then) {
		// Actions
		var sNewText = "OPA-TEST Testproject";
		When.onTheProjectObjectPage
		.iPressTheEditProjectHeaderButton()
		.and.iModifyTheProjectDescription(sNewText)
		.and.iPressOnTheSaveButtonOfTheEditProjectHeaderDialog();
		// Assertions
		Then.onTheProjectObjectPage.iShouldSeeTheNewProjectDescription(sNewText);
	});

	
	opaTest("Press Factory Button on first ECN - Should show the Select Plants Dialog for this ECN", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressOnTheFactoryButtonAtPosition(0);
		// Assertions
		Then.onTheMasterPage.iShouldSeeTheSelectPlantsDialog();
	});

	opaTest("Press Cancel Button - Should close the Select Plants Dialog", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressOnTheCancelButtonOfTheSelectPlantsDialog();
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("OneColumn");
		//Then.onTheMasterPage.iShouldSeeTheSelectPlantsDialog();
	});



	opaTest("Press Delete Button on the first ECN item - Should show the Revert-Remove-ECN button", function (Given, When, Then) {

		// Actions
		When.onTheMasterPage.iPressOnTheDeleteButtonAtPosition(0);

		// Assertions
		Then.onTheMasterPage.theFactoryButtonShouldBeDisabledAtPosition(0);

	});

	opaTest("Press Undo Delete - Should show the Remove-ECN button again", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressOnTheUndoDeleteButtonAtPosition(0);
		// Assertions
		Then.onTheMasterPage.theFactoryButtonShouldBeEnabledAtPosition(0);
	});

    /**************************************************************************
     * 
     * - Go into an ECN
     * - Test Info Buttons in the Data Quality Column
     * - Test Action Buttons in the Phase Out Column
     * 
     **************************************************************************/

	opaTest("Press ECN - Should open detail view", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iRememberTheIdOfListItemAtPosition(0).
			and.iPressOnTheObjectAtPosition(0);
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("TwoColumnsMidExpanded");
		Then.onTheDetailPage.theListShouldHaveNoSelection();
		// Cleanup
		//Then.iTeardownMyApp();
	});
	
	opaTest("Press Warning Button - Should show Popover with DQ message", function (Given, When, Then) {
		// Actions
		When.onTheDetailPage.iPressTheDQwarningButton();
		// Assertions
		Then.onTheDetailPage.theDQMessagePopoverShouldOccur();
	});

	opaTest("Press Plant Link - Should show Popover with Plant Details", function (Given, When, Then) {
		// Actions
		When.onTheDetailPage.iPressThePlantLink("0029401901","0001");
		// Assertions
		Then.onTheDetailPage.thePlantPopoverShouldOccur();
	});
	
	opaTest("Press Material Link - Should show Popover with Material Details", function (Given, When, Then) {
		// Actions
		When.onTheDetailPage.iPressTheMaterialLink("0029401901","0001");
		// Assertions
		Then.onTheDetailPage.theMaterialPopoverShouldOccur();
	});

	opaTest("Press Project Item - Should show the info view in the 3rd column", function (Given, When, Then) {
		// Actions
		When.onTheDetailPage.iPressOnTheObjectAtPosition(0);
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("ThreeColumnsMidExpanded");
	});

/*
	opaTest("Scroll down to a section - Should scroll down to Additional Info Section", function (Given, When, Then) {
		// Actions
		When.onTheDetailDetailPage.iNavigateToSection("__section5");
		// Assertions
		Then.onTheDetailDetailPage.theSubsectionShouldOccur("__section");
	});
*/

	opaTest("Press Close Button - Should close Info View", function (Given, When, Then) {
		// Actions
		When.onTheDetailDetailPage.iPressCloseButton();
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("TwoColumnsMidExpanded");
	});


	opaTest("Press another Project Item - Should open another Info view", function (Given, When, Then) {
		// Actions
		When.onTheDetailPage.iPressOnTheObjectAtPosition(2);
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("ThreeColumnsMidExpanded");
	});


	opaTest("Scroll down to section 5 - Should scroll down to Additional Info Section", function (Given, When, Then) {
		// Actions
		When.onTheDetailDetailPage.iNavigateToSection("__section5");
		// Assertions
		Then.onTheDetailDetailPage.theSubsectionShouldOccur("__section");
	});


	opaTest("Press Close Button - Should close Info View", function (Given, When, Then) {
		// Actions
		When.onTheDetailDetailPage.iPressCloseButton();
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("TwoColumnsMidExpanded");
	});

	opaTest("Press Phase Out Button - Should show the footer with the Save button", function (Given, When, Then) {
		// Actions
		When.onTheDetailPage.iPressThePhaseOutButton();
		// Assertions
		Then.onTheProjectObjectPage.theFooterIsVisible(true);
		
		//Then.onTheProjectObjectPage.iShouldSeeTheFooterBar();
		// NOTE for Stefan, by Adrian on 6.6.2019:
		// =======================================
		// There seems to be a UI5 bug with the attribute "showFooter" af the Semantic-Page and the ObjectPageLayout. 
		// Inside the OPA-test, it does not toggle between the CSS classes 
		// "sapContrast sapContrastPlus sapFDynamicPageFooter sapFFooter-CTX sapUiHidden"
		// and 
		// "sapContrast sapContrastPlus sapFDynamicPageFooter sapFFooter-CTX"
		// of the <footer>-element 
		// when switching between showFooter="false" and "true"!
	});

	opaTest("Press Save Button - Should close the detail", function (Given, When, Then) {
		// Actions
		When.onTheProjectObjectPage.iPressTheFooterSaveButton();
		// Assertions
		Then.onTheProjectObjectPage.theFooterIsVisible(false);
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("OneColumn");
	});

	opaTest("Re-open the ECN - Should show the Project item with new PO status", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iRememberTheIdOfListItemAtPosition(0).
			and.iPressOnTheObjectAtPosition(0);
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("TwoColumnsMidExpanded");
	    // Then.onTheDetailPage.theListShouldHaveNoSelection();
	});

	opaTest("Press Factory Button on first ECN - Should show the Select Plants Dialog for this ECN", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressOnTheFactoryButtonAtPosition(0);
		// Assertions
		Then.onTheMasterPage.iShouldSeeTheSelectPlantsDialog();
	});

	opaTest("Press Cancel Button - Should close the Select Plants Dialog", function (Given, When, Then) {
		// Actions
		When.onTheMasterPage.iPressOnTheCancelButtonOfTheSelectPlantsDialog();
		// Assertions
		Then.onTheProjectObjectPage.theAppShowsFCLDesign("TwoColumnsMidExpanded");
		//Then.onTheMasterPage.iShouldSeeTheSelectPlantsDialog();
	});


    /***
     * 
     * The END of this Journey: Go back to the projects list and tear down this journey.
     * 
     ***/
	opaTest("Press on the Browser Back Button - Should see the list of projects again", function (Given, When, Then) {
		// Actions
		When.onTheBrowser.iPressOnTheBackwardsButton();
		// Assertions
		Then.onTheProjectWorklistPage.iShouldSeeTheTable();

		// Cleanup
		Then.iTeardownMyApp();
		
	});
    
	return;

    /***
     * 
     * End of "NavigationJouney.js"
     * 
     * 
     ***/ 
	




/*




		opaTest("Should see the objects list", function (Given, When, Then) {
			// Arrangements
			Given.iStartMyApp();

			//Actions
			When.onTheWorklistPage.iLookAtTheScreen();

			// Assertions
			Then.onTheWorklistPage.iShouldSeeTheTable();
		});

		opaTest("Should react on hashchange", function (Given, When, Then) {
			// Actions
			When.onTheWorklistPage.iRememberTheItemAtPosition(2);
			When.onTheBrowser.iChangeTheHashToTheRememberedItem();

			// Assertions
			Then.onTheObjectPage.iShouldSeeTheRememberedObject().
				and.theViewIsNotBusyAnymore();
		});

		opaTest("Should go back to the TablePage", function (Given, When, Then) {
			// Actions
			When.onTheBrowser.iPressOnTheBackwardsButton();

			// Assertions
			Then.onTheWorklistPage.iShouldSeeTheTable();
		});

		opaTest("Object Page shows the correct object Details", function (Given, When, Then) {
			// Actions
			When.onTheWorklistPage.iRememberTheItemAtPosition(1).
				and.iPressATableItemAtPosition(1);

			// Assertions
			Then.onTheObjectPage.iShouldSeeTheRememberedObject();
		});

		opaTest("Should be on the table page again when browser back is pressed", function (Given, When, Then) {
			// Actions
			When.onTheBrowser.iPressOnTheBackwardsButton();

			// Assertions
			Then.onTheWorklistPage.iShouldSeeTheTable();
		});

		opaTest("Should be on the object page again when browser forwards is pressed", function (Given, When, Then) {
			// Actions
			When.onTheBrowser.iPressOnTheForwardsButton();

			// Assertions
			Then.onTheObjectPage.iShouldSeeTheRememberedObject().
				and.iTeardownMyAppFrame();
		});

		opaTest("Should see a busy indication while loading the metadata", function (Given, When, Then) {
			// Arrangements
			Given.iStartMyApp({
				delay: 10000
			});

			//Actions
			When.onTheWorklistPage.iLookAtTheScreen();

			// Assertions
			Then.onTheAppPage.iShouldSeeTheBusyIndicatorForTheWholeApp().
				and.iTeardownMyAppFrame();
		});


		opaTest("Start the App and simulate metadata error: MessageBox should be shown", function (Given, When, Then) {
			//Arrangement
			Given.iStartMyAppOnADesktopToTestErrorHandler("metadataError=true");

			//Assertions
			Then.onTheAppPage.iShouldSeeTheMessageBox().
				and.iTeardownMyAppFrame();

		});
		
*/
		

/*SR: Test was deactivated, because simulation of bad request fails, so OPA recognizes a false negative. Modify test in order to get a real bad request.
		opaTest("Start the App and simulate bad request error: MessageBox should be shown", function (Given, When, Then) {
			//Arrangement
			Given.iStartMyAppOnADesktopToTestErrorHandler("errorType=serverError");

			//Assertions
			Then.onTheAppPage.iShouldSeeTheMessageBox().
				and.iTeardownMyAppFrame();

		});
*/
	}
);