sap.ui.define([
	"sap/ui/test/Opa5",
	"./arrangements/Startup",
//	"./BusyJourney",
	"./NavigationJourney"
], function (Opa5, Startup) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Startup(),
		viewNamespace: "com.vaillant.plm.fiori.projectcockpit.view.",
		autoWait: true
	});
});

/*
sap.ui.require([
	"sap/ui/test/Opa5",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/Worklist",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/Object",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/NotFound",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/Browser",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/App",
	"com/vaillant/plm/fiori/projectcockpit/testV2/integration/pages/ProjectCreation"
], function(Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		autoWait: {
			timeoutWaiter: {
				maxDelay: 500 // timeouts with delays >= 500 will not block test execution
			}
		},
		viewNamespace: "com.vaillant.plm.fiori.projectcockpit.view."
	});

	sap.ui.require([
		"com/vaillant/plm/fiori/projectcockpit/testV2/integration/WorklistJourney",
		"com/vaillant/plm/fiori/projectcockpit/testV2/integration/ProjectCreationJourney",
		"com/vaillant/plm/fiori/projectcockpit/testV2/integration/ObjectJourney"
		// "com/vaillant/plm/fiori/projectcockpit/testV2/integration/NavigationJourney",
		// "com/vaillant/plm/fiori/projectcockpit/testV2/integration/NotFoundJourney",
		// "com/vaillant/plm/fiori/projectcockpit/testV2/integration/FLPIntegrationJourney"
	], function() {
		QUnit.start();
	});
});
*/