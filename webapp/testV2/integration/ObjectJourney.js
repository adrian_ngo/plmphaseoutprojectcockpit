sap.ui.define([
	"sap/ui/test/opaQunit",
	"sap/ui/test/Opa5"
], function(opaTest, Opa5) {
	"use strict";

	QUnit.module("Object");

	opaTest("Changing to the object screen", function(Given, When, Then) {
		var iProjectIndex = 0;
		//Actions
		When.onTheWorklistPage.iRememberAnObjectAtIndex(iProjectIndex)
			.and.iSelectAnObjectAtIndex(iProjectIndex);
		//Assertions
		Then.onTheObjectPage.iShouldSeeTheObjectPage()
			.and.theTitleShouldBeCorrect()
			.and.theSubTitleShouldBeCorrect();
	});

	opaTest("Editing the project header data", function(Given, When, Then) {
		var oNewHeaderData = {
			description: "New Project Test",
			manager: "New Manager Test",
			phaseOutDate: "10.01.2010"
		};
		//Actions
		When.onTheObjectPage.iPressTheEditProjectHeaderDataButton();
		//Assertions
		Then.onTheObjectPage.iShouldSeeTheEditProjectHeaderDialog();
		//Actions
		When.onTheObjectPage.iEditTheHeaderData(oNewHeaderData)
			.and.iPressHeaderDataApplyButton();
		//Assertions
		Then.onTheObjectPage.theProjectHeaderShouldHaveChanged(oNewHeaderData);
		//Actions
		When.onTheObjectPage.iPressCancelChangesButton();
		//Assertions
		Then.onTheObjectPage.theTitleShouldBeCorrect()
			.and.theSubTitleShouldBeCorrect();
	});
	
	opaTest("Open Plant dialog screen", function(Given, When, Then) {
		var iIndexToBePressed = 0;
		var aIndicesOfPlantsToBeAdded = [0,3,4,6,8];
		//Actions
		When.onTheObjectPage.iPressThePlantButtonOnIndex(iIndexToBePressed);
		//Assertions
		Then.onTheObjectPage.thePlantDialogForTheIndexShouldBeOpen();
		//Actions
		When.onTheObjectPage.iSelectTheFollowingPlantsToBeAdded(aIndicesOfPlantsToBeAdded);
	});
});





















