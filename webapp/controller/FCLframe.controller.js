/********************************************************************************************************************************************
 * Filename :  FCLframe.controller.js:
 * Author   :  d475 / IBsolution GmbH
 *
 * Event Bus:
 * ==========
 * "setDetailPage": Show the project items for the ECN that was selected by the user in the Master View
 * "setDetailDetailPage": Show the detail information for the selected Project Item
 *
 * Event Handlers:
 * ===============
 *
 * ******************************************************************************************************************************************/
sap.ui.define(
	[
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"com/vaillant/plm/fiori/projectcockpit/model/formatter",
		"com/vaillant/plm/fiori/projectcockpit/common/js/Utils"
	],
	function (
		BaseController,
		JSONModel,
		History,
		Filter,
		FilterOperator,
		formatter,
		Utils
	) {
		"use strict";

		return BaseController.extend(
			"com.vaillant.plm.fiori.projectcockpit.controller.FCLframe", {

				formatter: formatter,

				/* =========================================================== */
				/* lifecycle methods                                           */
				/* =========================================================== */

				/**
				 * Called when the worklist controller is instantiated.
				 * @public
				 */
				onInit: function () {
					try {

						// set Flexible3Layouts controller
						this.bus = sap.ui.getCore().getEventBus();
						this.bus.subscribe(
							"flexible",
							"setDetailPage",
							this.setDetailPage,
							this
						);
						this.bus.subscribe(
							"flexible",
							"closeDetailPage",
							this.closeDetailPage,
							this
						);
						this.bus.subscribe(
							"flexible",
							"setDetailDetailPage",
							this.setDetailDetailPage,
							this
						);
						this.bus.subscribe(
							"flexible",
							"editProjectHeaderData",
							this.fnOpenProjectHeaderDataDialog,
							this
						);

						this.oFlexibleColumnLayout = this.getView().byId("fcl");

						// Model used to manipulate control states. The chosen values make sure,
						// detail page is busy indication immediately so there is no break in
						// between the busy indication for loading the view's meta data
						var iOriginalBusyDelay,
							oViewModel = new JSONModel({
								busy: true,
								delay: 0,
								oFlexibleColumnLayout: this.oFlexibleColumnLayout,
								bSwitchAction: false,
								bProjectIsModified: false,
								bProjectHeaderIsModified: false,
								aDeletedEcnList: [],
								iCountEcnsMarkedAsDeleted: 0
							});

						this.getRouter()
							.getRoute("projectObject")
							.attachPatternMatched(this._onObjectMatched, this);

						// Store original busy indicator delay, so it can be restored later on
						iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
						this.setModel(oViewModel, "objectView");

						var oComponent = this.getOwnerComponent();
						oComponent
							.getModel("serviceModel")
							.metadataLoaded()
							.then(function () {
								// Restore original busy indicator delay for the object view
								oViewModel.setProperty("/delay", iOriginalBusyDelay);
								var sApplicationVersion = oComponent
									.getMetadata()
									.getManifestEntry("sap.app").applicationVersion.version;
								oViewModel.setProperty(
									"/sApplicationVersion",
									sApplicationVersion
								);
							});

						// define fields which has been changed for Project
						this.aChangeFields = [
							"PhaseOutDate",
							"PhaseOutManagerKey",
							"PhaseOutManagerDisplayName",
							"ProjectKey",
							"ProjectDescription"
						];

						//sap.ui.Device.resize.attachHandler(this.resetFclHeight(), this);
						$(window).resize(
							function () {
								this.resetFclHeight();
							}.bind(this)
						);
					} catch (oError) {
						jQuery.sap.log.error(oError);
					}
				},

				onExit: function () {
					this.bus.unsubscribe(
						"flexible",
						"setDetailPage",
						this.setDetailPage,
						this
					);
					this.bus.unsubscribe(
						"flexible",
						"closeDetailPage",
						this.closeDetailPage,
						this
					);
					this.bus.unsubscribe(
						"flexible",
						"setDetailDetailPage",
						this.setDetailDetailPage,
						this
					);
				},

				/* =========================================================== */
				/* event handlers                                              */
				/* =========================================================== */

				onAfterRendering: function (oEvent) {
					console.log("FCL - onAfterRendering");
				},

				resetFclHeight: function () {
					var hh = 0.85 * $(window).innerHeight();
					jQuery.sap.log.info("Current view height in px: " + hh);
					var sFclId = $("div[id$='fcl']")["0"].id;
					$("#" + sFclId).css("height", hh + "px");
					//$("#" + sFclId).css("border", "1px solid grey");
					$("#" + sFclId).css("box-shadow", "2px 3px 5px 1px rgba(0,0,0,0.25)");
					$("#" + sFclId).css(
						"-webkit-box-shadow",
						"2px 3px 5px 1px rgba(0,0,0,0.25)"
					);
					$("#" + sFclId).css(
						"-moz-box-shadow",
						"2px 3px 5px 1px rgba(0,0,0,0.25)"
					);
				},

				/**
				 * Event handler when the share in JAM button has been clicked
				 * @public
				 */
				onShareInJamPress: function () {
					var oViewModel = this.getModel("objectView"),
						oShareDialog = sap.ui.getCore().createComponent({
							name: "sap.collaboration.components.fiori.sharing.dialog",
							settings: {
								object: {
									id: location.href,
									share: oViewModel.getProperty("/shareOnJamTitle")
								}
							}
						});
					oShareDialog.open();
				},

				/* =========================================================== */
				/* internal methods                                            */
				/* =========================================================== */

				/**
				 * Binds the view to the object path.
				 * @function
				 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
				 * @private
				 */
				_onObjectMatched: function (oEvent) {
					var sObjectId = oEvent.getParameter("arguments").objectId;
					var oDataModel = this.getModel("serviceModel");
					oDataModel.metadataLoaded().then(
						function () {
							this.sObjectPath = oDataModel.createKey("ProjectEntitySet", {
								ProjectKey: sObjectId
							});
							this._bindView("/" + this.sObjectPath);
						}.bind(this)
					);
					this.getModel("UserSelectionModel").setProperty(
						"/CurrentProjectKey", sObjectId
					);
				},

				/**
				 * Binds the view to the object path.
				 * @function
				 * @param {string} sProjectPath path to the object to be bound
				 * @private
				 */
				_bindView: function (sProjectPath) {

					var oDataModel = this.getModel("serviceModel");
					var oViewModel = this.getModel("objectView");

					this.getView().bindElement({
						model: "serviceModel",
						path: sProjectPath,
						parameters: {
							expand: 'SelectedPlants,SelectedEcns'
						},
						events: {
							change: this._onBindingChange.bind(this),
							dataRequested: function () {
								oDataModel.metadataLoaded().then(function () {
									// Busy indicator on view should only be set if metadata is loaded,
									// otherwise there may be two busy indications next to each other on the
									// screen. This happens because route matched handler already calls '_bindView'
									// while metadata is loaded.
									oViewModel.setProperty("/busy", true);
								});
							},
							dataReceived: function (oEvent) {
								oViewModel.setProperty("/busy", false);
								// Whenever navigating into a project,
								// 1. show its list of SelectedEcns,
								// 2. show its list of SelectedPlants,
								// 3. for the selected Ecn (per default, the first Ecs should be selected!), show its list of PotentialItems,
								//    with $filter=PlantKey EQ '<AssignedPlantsKey1>' OR PlantKey EQ '<AssignedPlantsKey2>' OR ...
							}.bind(this)
						}
					});


				},

				_onBindingChange: function (oEvent) {
					var oView = this.getView(),
						oViewModel = this.getModel("objectView"),
						oElementBinding = oView.getElementBinding("serviceModel");

					// No data for the binding
					if (!oElementBinding.getBoundContext("serviceModel")) {
						this.getRouter()
							.getTargets()
							.display("objectNotFound");
						return;
					}

					var oResourceBundle = this.getResourceBundle(),
						oObject = oView.getBindingContext("serviceModel").getObject(),
						sObjectId = oObject.ProjectKey,
						sObjectName = oObject.ProjectKey;

					// Everything went fine.
					oViewModel.setProperty("/busy", false);
					oViewModel.setProperty(
						"/saveAsTileTitle",
						oResourceBundle.getText("saveAsTileTitle", [sObjectName])
					);
					oViewModel.setProperty("/shareOnJamTitle", sObjectName);
					oViewModel.setProperty(
						"/shareSendEmailSubject",
						oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId])
					);
					oViewModel.setProperty(
						"/shareSendEmailMessage",
						oResourceBundle.getText("shareSendEmailObjectMessage", [
							sObjectName,
							sObjectId,
							location.href
						])
					);

					//this.oFlexibleColumnLayout.setLayout(sap.f.LayoutType.OneColumn);
					this.oFlexibleColumnLayout.setLayout(
						sap.f.LayoutType.TwoColumnsMidExpanded
					);
					this.resetFclHeight();

				},

				// Lazy loader for the mid page - only on demand (when the user clicks an ECN in the Master View)
				setDetailPage: function (channel, event, data) {
					this.resetFclHeight();

					if (!this.detailView) {
						this.detailView = sap.ui.view({
							id: "midView",
							viewName: "com.vaillant.plm.fiori.projectcockpit.view.FCLdetail",
							type: "XML"
						});
					}

					//bind ECN element to the detail View
					this.detailView.bindElement({
						path: data.sEcnPath, // This ECN path comes from the ECN click event in the Master View
						model: "serviceModel"
					});

					this.oFlexibleColumnLayout.addMidColumnPage(this.detailView);
					//this.oFlexibleColumnLayout.setLayout(sap.f.LayoutType.TwoColumnsBeginExpanded);
					this.oFlexibleColumnLayout.setLayout(
						sap.f.LayoutType.TwoColumnsMidExpanded
					);

					// reset template variable for project data
					this.oTempContext = null;
				},

				closeDetailPage: function (channel, event, data) {
					this.oFlexibleColumnLayout.setLayout(sap.f.LayoutType.OneColumn);
				},

				// Lazy loader for the end page - only on demand (when the user clicks)
				setDetailDetailPage: function (channel, event, data) {
					if (!this.detailDetailView) {
						this.detailDetailView = sap.ui.view({
							id: "endView",
							viewName: "com.vaillant.plm.fiori.projectcockpit.view.FCLinfo",
							type: "XML"
						});
					}

					// generate path to the project-item
					var sProjectItemPath = this.oFlexibleColumnLayout
						.getModel("serviceModel")
						.createKey("MaterialPlantItemEntitySet", {
							MaterialKey: data.MaterialKey,
							PlantKey: data.PlantKey
						});
					this.detailDetailView.bindElement({
						path: "/" + sProjectItemPath,
						model: "serviceModel",
						parameters: {
							expand: "SameMaterials/AssignedProject,SameMaterials/AssignedEcn"
						},
						events: {
							dataRequested: function () {
								var i=0;
							},
							dataReceived: function (oEvent) {
								var i=0;
							}.bind(this)
						}
					});

					if ( sap.ui.getCore().getModel("settingsModel").getProperty("/TestMode") ) {
						// Ignore SameMaterials in Test Mode. There is a problem with mockdata.
						// Causes OPA5 timeout!
						this.oFlexibleColumnLayout.addEndColumnPage(this.detailDetailView);
						this.oFlexibleColumnLayout.setLayout(
							sap.f.LayoutType.ThreeColumnsMidExpanded
						);
					} else {
						//var oSection = sap.ui.getCore().byId("endView--idSameMaterialsSection");
						if (!this.oInternalFragment) {
							this.oInternalFragment = sap.ui.xmlfragment(
								this.getView().getId(),
								"com.vaillant.plm.fiori.projectcockpit.view.fragments.SameMaterials",
								this
							);

							if (this.oInternalFragment) {
								this.oInternalFragment.bindElement({
									path: "/" + sProjectItemPath,
									model: "serviceModel",
									parameters: {
										expand: "SameMaterials/AssignedProject,SameMaterials/AssignedEcn"
									} 
								});
							}

							this.detailDetailView
								.getContent()[0]
								.addSection(this.oInternalFragment);
						} else {
							if (this.oInternalFragment) {
								this.oInternalFragment.bindElement({
									path: "/" + sProjectItemPath,
									model: "serviceModel",
									parameters: {
										expand: "SameMaterials/AssignedProject,SameMaterials/AssignedEcn"
									} 
								});
							}
						}

						// Pro Zeile:
						// StockReportEntitySet
						/*

    				<Key>
    					<PropertyRef Name="MaterialKey"/>
    					<PropertyRef Name="PlantKey"/>
    					<PropertyRef Name="PlantTypeKey"/>
    				</Key>

    	    		/StockReportEntitySet?$filter=MaterialKey eq '0010011971' and PlantKey eq '0100'&$format=json"
                */

						//var sQueryStockReports = "/StockReportEntitySet?$filter=MaterialKey eq '" + data.MaterialKey + "' and PlantKey eq '" + data.MaterialKey + "'";
						//var sQueryStockReports = "/StockReportEntitySet";
						if (!this.oStockReportFragment) {
							this.oStockReportFragment = sap.ui.xmlfragment(
								this.getView().getId(),
								"com.vaillant.plm.fiori.projectcockpit.view.fragments.StockReport",
								this
							);
/*
							if (this.oStockReportFragment) {
								this.oStockReportFragment.bindElement({
									path: "/" + sProjectItemPath,
									model: "serviceModel"
								});
							}
*/

							this.detailDetailView
								.getContent()[0]
								.addSection(this.oStockReportFragment);
						} else {
							if (this.oStockReportFragment) {
								this.oStockReportFragment.bindElement({
									path: "/" + sProjectItemPath,
									model: "serviceModel"
								});
							}
						}

						this.oFlexibleColumnLayout.addEndColumnPage(this.detailDetailView);
						this.oFlexibleColumnLayout.setLayout(
							sap.f.LayoutType.ThreeColumnsMidExpanded
						);

						var sMaterialKey = data.MaterialKey;
						var sPlantKey = data.PlantKey;
						this.fnApplyStockPlantFilter(sMaterialKey, sPlantKey);

						/*
        		var sIdStockReportTable = $("div[id$='idStockReportTable']")["0"].id;
    			var oStockReportTable = this.byId(sIdStockReportTable);
    			//oStockReportTable.destroyRows();
    			var oFilter = new Filter([
    				new Filter("MaterialKey", FilterOperator.EQ, sMaterialKey),
    				new Filter("PlantKey", FilterOperator.EQ, sPlantKey)
    			], true);
    			oStockReportTable.getBinding("rows").filter(oFilter, "Application");
                //sap.ui.getCore().byId(sIdStockReportTable).getBinding("rows").filter(oFilter, "Control");

                */

						this.getModel("objectView").setProperty(
							"/bStockPlantFilter",
							true
						);
						this.getModel("objectView").setProperty(
							"/bStockPlantFilterIcon",
							"sap-icon://clear-filter"
						);
					}
				},

				fnApplyStockPlantFilter: function (sMaterialKey, sPlantKey) {
					var sIdStockReportTable = $("div[id$='idStockReportTable']")["0"].id;
					var oStockReportTable = this.byId(sIdStockReportTable);
					if (sPlantKey && sPlantKey !== "") {
						oStockReportTable.setVisibleRowCount(10);
						var oFilter = new Filter(
							[
								new Filter("MaterialKey", FilterOperator.EQ, sMaterialKey),
								new Filter("PlantKey", FilterOperator.EQ, sPlantKey)
							],
							true
						);
						oStockReportTable.getBinding("rows").filter(oFilter, "Application");
					} else {
						oStockReportTable.setVisibleRowCount(20);
						var oFilterMaterial = new Filter(
							"MaterialKey",
							FilterOperator.EQ,
							sMaterialKey
						);
						oStockReportTable.getBinding("rows").filter(oFilterMaterial, "Application");
					}
				},

				onPressStockPlantFilter: function (oEvent) {
					var sMaterialKey = oEvent
						.getSource()
						.getCustomData()[0]
						.getKey();
					var oObjectViewModel = this.getModel("objectView");
					if (oObjectViewModel.getProperty("/bStockPlantFilter")) {
						var sPlantKey = oEvent
							.getSource()
							.getCustomData()[1]
							.getKey();
						this.fnApplyStockPlantFilter(sMaterialKey, sPlantKey);
						oObjectViewModel.setProperty("/bStockPlantFilter", true);
						oObjectViewModel.setProperty(
							"/bStockPlantFilterIcon",
							"sap-icon://clear-filter"
						);
					} else {
						this.fnApplyStockPlantFilter(sMaterialKey);
						oObjectViewModel.setProperty("/bStockPlantFilter", false);
						oObjectViewModel.setProperty(
							"/bStockPlantFilterIcon",
							"sap-icon://filter"
						);
					}
				},

				/**************************************************************************************************************
				 *		visible / unvisible the toolbar for project object
				 * ***********************************************************************************************************/
				fnCheckShowSaveButton: function () {
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var oDataModel = this.getModel("serviceModel");
					var aDeletedEcnList = oUserSelectionModel.getProperty("/aDeletedEcnList");
					var aPhaseOutItemList = this.getModel("UserSelectionModel").getProperty("/aPhaseOutItemList") || [];
					var bProjectFooter = false;

					// Check if there are any ECNs to be deleted or any item marked to be added for or to be withdraown from phaseout
					if (aDeletedEcnList.length > 0 || aPhaseOutItemList.length > 0) {
						bProjectFooter = true;
					}

					// Check if there are any pending changes left
					jQuery.each(oDataModel.getPendingChanges(), function (entityPath) {
						if (entityPath.length > 0) {
							bProjectFooter = true;
							return false; // exit loop
						}
					});

					oUserSelectionModel.setProperty("/bProjectIsModified", bProjectFooter);
				},

				/**************************************************************************************************************
				 *		edit project header data dialog
				 * ***********************************************************************************************************/
				fnOpenProjectHeaderDataDialog: function () {
					// object for new values of dialog
					this.oNewInputValues = {};

					if (!this.pressDialog) {
						this.pressDialog = sap.ui.xmlfragment(
							"com.vaillant.plm.fiori.projectcockpit.view.fragments.DialogEditProjectHeaderData",
							this
						);
						//to get access to the global model
						this.getView().addDependent(this.pressDialog);

						//bind data model
						var oDataModel = this.getModel("serviceModel");
						this.pressDialog.setModel(oDataModel);
					}

					// disable apply buuton
					var oAppModel = this.getModel("appView");
					oAppModel.setProperty("/bNewProjectHeaderDataIsFilled", false);

					// get current data from project header data
					this.fnCreateCloneProjectData();

					// bind element
					if (this.oTempContext) {
						this.pressDialog.setBindingContext(
							this.oTempContext,
							"serviceModel"
						);
					}

					this.getView().getModel("UserSelectionModel").setProperty("/bEditSpareCheckbox", false);

					this.pressDialog.open();
				},

				fnCreateCloneProjectData: function () {
					var oDataModel = this.getModel("serviceModel");
					var oObject = oDataModel.getProperty("/" + this.sObjectPath);
					var properties = {};

					for (var i = 0; i < this.aChangeFields.length; i++) {
						var sField = this.aChangeFields[i];
						if (!properties[sField]) {
							properties[sField] = oObject[sField];
						}
					}
					
					// SPRINT-2: SPARE Flag Handling
					properties['ProjectCategory'] = oObject['ProjectCategory'];

					this.oTempContext = oDataModel.createEntry(
						"/ProjectEntitySet", {
							groupId: "ProjectEntityGroupId",
							properties: properties
						}
					);
				},

				fnApplyDataFromDialog: function (oEvent) {
					// update real project entity
					this.fnUpdateProjectHeaderData();

					//close dialog
					this.fnCloseDialog();
				},

				fnUpdateProjectHeaderData: function () {
					var oDataModel = this.getModel("serviceModel");
					var oObject = this.oTempContext.getObject();
					for (var i = 0; i < this.aChangeFields.length; i++) {
						var sField = this.aChangeFields[i];
						oDataModel.setProperty(
							"/" + this.sObjectPath + "/" + sField,
							oObject[sField]
						);
					}
				},

				fnCloseDialog: function () {
					//close dialog
					this.pressDialog.close();

					// unbind element
					this.pressDialog.unbindElement();

					//delete temp context
					var oDataModel = this.getModel("serviceModel");
					oDataModel.deleteCreatedEntry(this.oTempContext);
					this.oTempContext = null;

					// set visible footer bar with buttons
					this.fnCheckShowSaveButton();

					//set modified status for project header data
					this.fnUpdateProjectHeaderModifiedStatus();
				},

				/**********************************************************************************************************************************
				 * 		update modified status for project header
				 * ********************************************************************************************************************************/

				fnUpdateProjectHeaderModifiedStatus: function () {
					var oDataModel = this.getModel("serviceModel");
					var oObjectModel = this.getModel("objectView");
					var bModifiedStatusVisible = false;

					if (oDataModel.getPendingChanges()[this.sObjectPath]) {
						bModifiedStatusVisible = true;
					}
					oObjectModel.setProperty("/bProjectHeaderIsModified", bModifiedStatusVisible);
				},

				/**************************************************************************************************************************************
				 *  	save and cancel changes in the project
				 * ***********************************************************************************************************************************/
				// onPressCancel:
				fnOnCancelAllChangesInProject: function () {
					var oDataModel = this.getModel("serviceModel");
					var oObjectModelView = this.getModel("objectView");
					var oUserSelectionModel = this.getModel("UserSelectionModel");

					//reset changes in oDataModel

					var aPhaseOutItemList = this.getModel("UserSelectionModel").getProperty("/aPhaseOutItemList");
					aPhaseOutItemList.forEach(function (item) {

						var sItemPath = oDataModel.createKey("StatusManagementEntitySet", {
							"MaterialKey": item.MaterialKey,
							"PlantKey": item.PlantKey
						});
						var sOldStatus = oDataModel.getProperty("/" + sItemPath + "/MaterialPlantItemOldStatus");
						oDataModel.setProperty("/" + sItemPath + "/MaterialPlantItemStatus", sOldStatus);

					});

					oDataModel.resetChanges();

					oUserSelectionModel.setProperty("/aPhaseOutItemList", []);

					//refresh status in project header data
					oObjectModelView.setProperty("/bProjectHeaderIsModified", false);

					// refresh delete buttons for ecns and clear array with ecn's path for deleting
					jQuery.each($("button[data-deletedstatus='true']"), function (
						i,
						data
					) {
						var sBtnId = data.id;
						var oButton = sap.ui.getCore().byId(sBtnId);
						oButton.firePress();
					});

					//clear array with ecn's path for deleting
					oUserSelectionModel.setProperty("/aDeletedEcnList", []);

					// reset buttons for project items
					/*
					jQuery.each( $("button[data-CanUndo='true']"),
					  function(index, data){
					    sap.ui.getCore().byId(data.id).firePress();
					  }
					);
					*/

					//refresh status in ECNs
					this.fnRefreshEcnModifiedStatus();

					//refresh parameters
					this.fnSaveDeleteCommonAction();

					oDataModel.refresh();
				},

				/**
				 * onPressSave
				 */
				fnOnSaveChangesInProject: function () {
					var oDataModel = this.getModel("serviceModel");
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var oBundle = this.getResourceBundle();

					// 1. Submit Pending Changes
					// 2. Add Items for Phaseout
					// 3. Remove Items for Phaseout
					// 4. Remove ECNs  --> Undo 2 and 3 for the selected ecn to be deleted!

					// Collect information
					var aPhaseOutItemList = oUserSelectionModel.getProperty("/aPhaseOutItemList") || [];

					// The MaterialPlantItemStatus was abused for the UI to display the button state using a transient state.
					// First reset all the transient transfer states to the old state (from MaterialPlantItemOldStatus).
					// The transient states MUST not go into the pending changes!
					aPhaseOutItemList.forEach(function (item) {
						var sMaterialPlantItemEntity = oDataModel.createKey("MaterialPlantItemEntitySet", {
							MaterialKey: item.MaterialKey,
							PlantKey: item.PlantKey
						})
						var sOldStatus = oDataModel.getProperty("/" + sMaterialPlantItemEntity + "/StatusManagement/MaterialPlantItemOldStatus");
						oDataModel.setProperty("/" + sMaterialPlantItemEntity + "/StatusManagement/MaterialPlantItemStatus", sOldStatus);
					});

					var aListOfItemsToAddToPhaseOut = aPhaseOutItemList.filter(function (item) {
						return item.ShouldBePhasedOut === "true";
					});
					var aListOfItemsToWithdrawFromPhaseOut = aPhaseOutItemList.filter(function (item) {
						return item.ShouldBePhasedOut === "false";
					});

					var sCurrentProjectKey = oUserSelectionModel.getProperty("/CurrentProjectKey");
					var oTargetParameter = {
						"projectKey": sCurrentProjectKey
					};

					var d1 = new Date();

					// The strict order is important here: Process one group after the other.

					// 1.1 StatusManagement Changes
					this.pSubmitChanges(oDataModel, "StatusManagementEntity", "StatusManagementEntityGroupId")

					.then(function (oData) {
						// 1.2 Project Header Changes
						return this.pSubmitChanges(oDataModel, "ProjectEntity", "ProjectEntityGroupId");
					}.bind(this))

					.then(function (oData) {
						// 2. Add Items for Phaseout
						return this.pProcessEntitiesAssignment(oDataModel, "/selectForPhaseout", aListOfItemsToAddToPhaseOut, oTargetParameter);
					}.bind(this))

					.then(function (oData) {
						// 3. Remove Items from Phaseout
						return this.pProcessEntitiesAssignment(oDataModel, "/deselectForPhaseout", aListOfItemsToWithdrawFromPhaseOut);
					}.bind(this))

					.then(function (oData) {

						// 4. Remove ECNs from Project
						var aDeletedEcnList = oUserSelectionModel.getProperty("/aDeletedEcnList") || [];
						return this.pProcessEntitiesAssignment(oDataModel, "/deselectEcn", aDeletedEcnList, oTargetParameter);
					}.bind(this))

					.then(function (oData) {

						// 5. See if the list of SelectedPlants contains any plants that are in the list of PotentialPLants
						// If yes, remove those plants from the SelectedPlants list using the /deselectPlant function import!
        				return this.pGetPotentialPlants( sCurrentProjectKey );
					}.bind(this))
					
					.then(function (oData) {
					    // 6. Tricky:
					    // Remove all plants that are not needed anymore after removing some ECNs.
						var aAvailablePlants = oData.response.results || [];
						var aSelectedPlants = this.clone(oUserSelectionModel.getProperty("/aAssignedPlants"));

    					var aRemovedPlants = [];
    
    					for (var i = aSelectedPlants.length - 1; i > -1; i--) {
    					    aSelectedPlants[i].bSelected = true;
    					}
    					aAvailablePlants.forEach(function(oPotentialPlant){
    					    aSelectedPlants.forEach(function(oSeletedPlant){
    					        if(oPotentialPlant.PlantKey === oSeletedPlant.PlantKey){
    					            oSeletedPlant.bSelected = false;
    					        }
    					    });
    					});
    					for (var i = aSelectedPlants.length - 1; i > -1; i--) {
    						if (aSelectedPlants[i].bSelected) {
    							aRemovedPlants = aRemovedPlants.concat(aSelectedPlants.splice(i, 1));
    						}
    					}
						var sFunctionImportPath = "/deselectPlant";
						var oTargetParameter = {
							"projectKey": sCurrentProjectKey
						};
						return this.pProcessEntitiesAssignment(oDataModel, sFunctionImportPath, aRemovedPlants, oTargetParameter);
    					
					}.bind(this))

					.then(function (oData) {

						// Clear up processed lists
						oUserSelectionModel.setProperty("/aPhaseOutItemList", []);
						oUserSelectionModel.setProperty("/aDeletedEcnList", []);

						//set modified status for project header data
						this.fnUpdateProjectHeaderModifiedStatus();

						//refresh parameters
						this.fnRefreshEcnModifiedStatus();
						this.fnSaveDeleteCommonAction();

						//refresh data model
						oDataModel.refresh(true);

						var d2 = new Date();
						var d3 = d2 - d1;
						jQuery.sap.log.setLevel("4");
						jQuery.sap.log.info("Pressing Save so much time in ms: ", d3);

					}.bind(this))

					// The catch block should be listed at the very end. After a error occurs, you should not run into a then-block again.
					.catch(function (oError) {
						jQuery.sap.log.error(
							"Error onPressSave / fnOnSaveChangesInProject: "
						);

						sap.m.MessageBox.error(this.getResourceBundle().getText("errorText"), {
							details: oError.responseText,
							actions: [sap.m.MessageBox.Action.CLOSE],
							onClose: function () {
								// Fire press cancel!
								var oCancelButton = this.getView().byId("idFooterCancelButton");
								oCancelButton.firePress();
							}.bind(this)
						});

					}.bind(this));

				},

				fnRefreshEcnModifiedStatus: function () {
					//refresh status in ECNs
					jQuery.each(
						$(".sapMObjStatusText"),
						function (i, data) {
							var sText = data.textContent;
							if (sText.length > 0) {
								var oParentElement = data.parentElement;
								var bEcnAttribute = oParentElement.hasAttribute(
									"data-modifiedkey"
								);
								if (bEcnAttribute) {
									var sEcnKey = oParentElement.getAttribute("data-modifiedkey");
									new BaseController().fnSetModifiedStatusToEcn.apply(this, [
										sEcnKey,
										"false"
									]);
								}
							}
						}.bind(this)
					);
				},

				fnSaveDeleteCommonAction: function () {
					var oObjectModelView = this.getModel("objectView");
					//refresh footer state
					this.fnCheckShowSaveButton();

					//reset count deleted ecns
					oObjectModelView.setProperty( "/iCountEcnsMarkedAsDeleted", 0 );
				}

			}
		);
	}
);