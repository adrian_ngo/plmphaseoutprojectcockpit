sap.ui.define([
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function (BaseController, JSONModel) {
		"use strict";

		return BaseController.extend("com.vaillant.plm.fiori.projectcockpit.controller.App", {

			onInit : function () {
				var oViewModel,
					fnSetAppNotBusy,
					iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

				oViewModel = new JSONModel({
					busy : true,
					delay : 0,
					bEcnListIsFilled: false,
					bNewProjectHeaderDataIsFilled: false
				});
				this.setModel(oViewModel, "appView");


        this.initializeUserSelectionModel();


				fnSetAppNotBusy = function() {
					oViewModel.setProperty("/busy", false);
					oViewModel.setProperty("/delay", iOriginalBusyDelay);
				};

				this.getOwnerComponent().getModel("serviceModel").metadataLoaded()
				.then(fnSetAppNotBusy);

				// apply content density mode to root view
				this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
			},


			initializeUserSelectionModel: function() {

					/****
					 * The User Selection Model is a local JSON Model
					 * used to store user selections temporarily and
					 * also display settings like e.g. the visibility of buttons
					 * of numbers of items depending on user selections.
					 *
					 * Is is being used in both the "Project Overview" and "Project FCL View".
					 */

					var oUserSelectionModel = new JSONModel({
						"CurrentProjectKey": "",
						"aPhaseOutItemList": [],
						"aDeletedEcnList": [],
						"bProjectIsModified": false,
						"nEcnProjectItems": 0     // number of MaterialPlantItems that are relevant for this Ecn
					});

					this.setModel( oUserSelectionModel, "UserSelectionModel" );
			},




		});

	}
);