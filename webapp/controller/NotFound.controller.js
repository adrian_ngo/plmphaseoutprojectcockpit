sap.ui.define([
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("com.vaillant.plm.fiori.projectcockpit.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);