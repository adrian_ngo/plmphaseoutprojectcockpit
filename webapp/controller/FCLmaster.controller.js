/********************************************************************************************************************************************
 * Filename :  FCLmaster.controller.js:
 * Author   :  d475 / IBsolution GmbH
 *
 * Event Handlers:
 * ===============
 * onSelectECN: On clicking an ECN in the Master view
 *
 * ******************************************************************************************************************************************/
sap.ui.define(
	[
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/model/Sorter",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/ui/core/routing/History",
		"com/vaillant/plm/fiori/projectcockpit/model/formatter",
		"sap/m/MessageToast",
		"com/vaillant/plm/fiori/projectcockpit/common/js/Utils"
	],
	function (
		BaseController,
		JSONModel,
		Sorter,
		Filter,
		FilterOperator,
		History,
		formatter,
		MessageToast,
		Utils
	) {
		"use strict";

		return BaseController.extend(
			"com.vaillant.plm.fiori.projectcockpit.controller.FCLmaster", {
				formatter: formatter,
				sCountEcnsMarkedAsDeleted: "/iCountEcnsMarkedAsDeleted",

				aSelectedPlants: [],
				nPlantsToBeRemoved: 0,

				/* =========================================================== */
				/* lifecycle methods                                           */
				/* =========================================================== */

				/**
				 * Called when the worklist controller is instantiated.
				 * @public
				 */
				onInit: function () {
					this.bus = sap.ui.getCore().getEventBus();

					// keeps the search state
					this._aTableSearchState = [];

					// Model used to manipulate control states
					var oViewModel = new JSONModel({
						masterTableTitle: this.getResourceBundle().getText(
							"masterTableTitle"
						),
						iSelectedRowsTable1: 0,
						iSelectedRowsTable2: 0,
						selectedPlants: {},
						assignedPlantsTitle: this.getResourceBundle().getText("assignedPlantsTitle"),
						availablePlantsTitle: this.getResourceBundle().getText("availablePlantsTitle"),
						bEnabledSubmitButtonSeletedPlants: false,
						bEnableButtonAddPlants: false,
						bEnableButtonRemovePlants: false,
						sPromptAddPlants: this.getResourceBundle().getText("txtPromptAddPlants"),
						sPromptRemovePlants: this.getResourceBundle().getText("txtPromptRemovePlants")
					});
					this.setModel(oViewModel, "masterView");
				},

				displaySelectedEcnItem: function (aEcnTableItems, oSelectedItem, aAssignedPlants) {
					aEcnTableItems.forEach(function (item) {
						item.setHighlight(sap.ui.core.MessageType.None);
					});
					if (oSelectedItem) {
						oSelectedItem.setHighlight(sap.ui.core.IndicationColor.Indication05);

						var oContext = oSelectedItem.getBindingContext("serviceModel");
						var sEcnPath = oContext.getPath();
						var params = {
							sEcnPath: sEcnPath // This ECN Path is being passed to the Detail View
						};
						this.bus.publish("flexible", "setDetailPage", params);
						// Hint for the reader: Lookup the Event subscription for "setDetailPage" inside "FCLframe.controller.js"

    					this.fnApplyDefaultFilterToTableOfMaterialPlantItems(aAssignedPlants);

						//var oRowContext = oEvent.getSource().getBindingContext("serviceModel");
						//var sPath = oRowContext.getPath();
						var sEcnKey = oContext.getProperty(sEcnPath + "/EcnKey");
						this.getModel("UserSelectionModel").setProperty("/CurrentEcnKey", sEcnKey);

						var oObject = oContext.getObject();
						this.refreshEcnTitle(oObject);
						var sHint = this.getResourceBundle().getText("msgLoadingMidColumn", [
							oObject.EcnKey
						]);
						MessageToast.show(sHint);
					}
				},

				onUpdateFinishedTableOfAssignedEcns: function (oEvent) {
					// Update ECN Table Header showing the number of assigned ECNs
					var sTitle = this.getResourceBundle().getText("masterTableTitle");
					var iTotalItems = oEvent.getParameter("total");
					if (iTotalItems > 0) {
						sTitle = this.getResourceBundle().getText("masterTableCountTitle", [
							iTotalItems
						]);
					}
					this.getModel("masterView").setProperty(
						"/masterTableTitle",
						sTitle
					);

					var iFirst = 0;
					var oTable = oEvent.getSource();
					var aEcnTableItems = oTable.getItems();

					/* 
					    Loop over list of ECNs and their potentialItems and find out those items whose
					    MARC_MMSTA > 50. For those items, collect the plant keys.
					    All those plants must not be removable!
					*/

					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var aPotentialItemsPaths = [];
					aEcnTableItems.forEach(function (oEcnItem) {
						var oContext = oEcnItem.getBindingContext("serviceModel");
						var sEcnPath = oContext.getPath();
						aPotentialItemsPaths.push(sEcnPath + "/PotentialItems");
					});
					oUserSelectionModel.setProperty("/aPotentialItemsPaths", aPotentialItemsPaths);

					return;

				},

				onUpdateFinishedTableOfAssignedPlants: function (oEvent) {
					this.oTableOfAssignedPlants = oEvent.getSource();
					var nAssignedPlants = oEvent.getParameter("total");

				    // Read list of assigned plants from Project's expanded /SelectedPlants from "idTableOfAssignedPlants"
					var aPaths = this.oTableOfAssignedPlants.getBinding("items").aKeys;

					var oBundle = this.getResourceBundle();
					var oDataModel = this.getModel("serviceModel");
					var oUserSelectionModel = this.getModel("UserSelectionModel");

					var aAssignedPlants = [];

					var bNew = false;

					aPaths.map(
						function (sPath) {
							var sPlantKey = oDataModel.getProperty("/" + sPath).PlantKey;
							var sPlantDescription = oDataModel.getProperty("/" + sPath).PlantDescription;
							aAssignedPlants.push({
								"PlantKey": sPlantKey,
								"PlantDescription": sPlantDescription,
								"bCheckBoxEnabled": true,
								"bSelected": false,
								"statusText": oBundle.getText(
									bNew ? "selectedPlantNew" : "selectedPlantAssigned"
								),
								"statusState": bNew ? "Success" : "None"

							});
						}.bind(this)
					);

					oUserSelectionModel.setProperty("/aAssignedPlants", aAssignedPlants);
					
					
					// If there are no assigned ECNs or no assigned plants, it does not make sense to show the detail view.
					var oTable = this.getView().byId("masterTable");
					var aEcnTableItems = oTable.getItems();
					
					if(aEcnTableItems.length > 0 && aAssignedPlants.length > 0){
					    // click on the first ECN to show the detail page
    					var iFirst = 0;
						var oSelectedItem = aEcnTableItems[iFirst];
						this.displaySelectedEcnItem(aEcnTableItems, oSelectedItem, aAssignedPlants);
					} else {
						var sMessage = this.getResourceBundle().getText("txtProjectHasNoPlants");
					    if(aEcnTableItems.length < 1){
    						sMessage = this.getResourceBundle().getText("txtProjectHasNoEcns");
					    }
						sap.m.MessageToast.show(sMessage);
						this.fnOpenOnlyMasterPage();
					}
					
					
					/*
					    Tricky: 
					    This code is to find out if there are any ECNs or plants that cannot be removed because
					    items are already in the phaseout process.
					*/
					var aFilterArray = [];
					aAssignedPlants.forEach(function (oPlant) {
						aFilterArray.push(new Filter("PlantKey", "EQ", oPlant.PlantKey));
					});
					var oDefaultApplicationFilter = new Filter(aFilterArray, false);
					var aPotentialItemsPaths = oUserSelectionModel.getProperty("/aPotentialItemsPaths");
					var oUrlParameters = {
					    "$expand": "AssignedProject,MasterData,SameMaterials/AssignedProject,SameMaterials/AssignedEcn,SameMaterials/StatusManagement"
					};
					
					/* 
					    Loop over list of ECNs and their potentialItems and find out those items
					     - which are assigned to the current project AND
					     - whose MARC_MMSTA > 50.
					    For those items, the ECN or the plant MUST NOT be removable!
					*/
					var sCurrentProjectKey = oUserSelectionModel.getProperty("/CurrentProjectKey");
					var nBaseStatus = 50;
                    var aPromises = [];
					aPotentialItemsPaths.forEach(function(sPotentialItemsPath){
						aPromises.push(this.pReadPotentialItems(oDataModel, sPotentialItemsPath, [oDefaultApplicationFilter], oUrlParameters));
					}.bind(this));
					var aForbiddenPlantsList = [];
					Promise.all(aPromises).then(function(aData){
					    aData.forEach(function(oData){

		                	var bForbiddenToRemoveEcn = false;
					        oData.data.results.forEach(function(item){
					            if( item.AssignedProject && 
					            	item.AssignedProject.ProjectKey &&
					            	//item.AssignedProject.ProjectKey !== ""
					            	item.AssignedProject.ProjectKey === sCurrentProjectKey
					            	){
    				                if(parseInt(item.MasterData.MARC_MMSTA) > nBaseStatus){
    				                	bForbiddenToRemoveEcn = true;
    				                    aForbiddenPlantsList.push(item.PlantKey);
    				                }
					            }
					        });
					        
							var aEcnTableItems = oTable.getItems();
							aEcnTableItems.forEach(function(oEcnTableItem){
								var sEcnKey = oEcnTableItem.getCells()[0].getItems()[0].getItems()[1].getText();
								if(oData.sEcnPath.indexOf(sEcnKey) > -1 ){
							        if(bForbiddenToRemoveEcn){
							        	// Deactivate the trashcan button!
										oEcnTableItem.getCells()[0].getItems()[1].getItems()[1].setEnabled(false);
							        } else {
							        	// Activate the trashcan button!
										oEcnTableItem.getCells()[0].getItems()[1].getItems()[1].setEnabled(true);
							        }
								}
							});
					        
					    });
					    
                        function onlyUnique(value, index, self) { 
                            return self.indexOf(value) === index;
                        }
                        var aUniqueList = aForbiddenPlantsList.filter( onlyUnique );

    					aAssignedPlants.forEach(function (oPlant) {
    						if(aUniqueList.indexOf(oPlant.PlantKey) > -1){
    						    oPlant.bCheckBoxEnabled = false;
    						};
    					});
                        

					});
				},


				getTableOfMaterialItems: function () {
					var oDomElement = $("div[id$='TableBinding']")["0"];
					if (oDomElement) {
						var sTableId = oDomElement.id;
						var oTable = sap.ui.getCore().byId(sTableId);
						return oTable;
					} else {
						return null;
					}
				},

				fnApplyDefaultFilterToTableOfMaterialPlantItems: function (aAssignedPlants) {
					var oTable = this.getTableOfMaterialItems();
					if (oTable) {
						var oTableBinding = oTable.getBinding("rows");

						var aFilterArray = [];
						aAssignedPlants.forEach(function (oPlant) {
							aFilterArray.push(new Filter("PlantKey", "EQ", oPlant.PlantKey));
						});
						var oDefaultApplicationFilter = new Filter(aFilterArray, false);

						// VERY IMPORTANT: The filter must be cleaned up properly before using a new filter!
						oTableBinding.aFilters = null;
						
						oTableBinding.filter(oDefaultApplicationFilter, "Application"); // Apply filter here

						if (oTableBinding) {
							oTableBinding.attachDataRequested(
								function (oEvent) {
									oTable.setBusyIndicatorDelay(0);
									oTable.setBusy(true);
								}.bind(this)
							);
							oTableBinding.attachDataReceived(
								function (oEvent) {
									oTable.setBusy(false);
									var oData = oEvent.getParameters().data;
									if (oData) {
										var nEcnProjectItems = oData.results.length; // number of MeterialPlantItems that are relevant for this Ecn
										this.getModel("UserSelectionModel").setProperty(
											"/nEcnProjectItems",
											nEcnProjectItems
										);
									}
								}.bind(this)
							);
						}
					}
				},

				getTable: function () {
					return this.byId("masterTable");
				},

				getCells: function () {
					var aCells = [
						new sap.m.Input("idEcnDescription", {
							value: "{/EcnDescription}",
							required: true,
							liveChange: this.checkRequiredField
						}),
						new sap.m.Input("idEcnWBSelement", {
							value: "{/EcnWBSelement}",
							required: true,
							liveChange: this.checkRequiredField
						}),
						new sap.m.Input("idEcnWerk", {
							value: "{/EcnWerk}",
							required: true,
							liveChange: this.checkRequiredField
						})
					];

					return aCells;
				},

				getEcnHeaderContent: function () {
					var aCells = this.getCells(),
						aContent = [
							new sap.m.Label({
								text: "{i18n>ecnDescriptionTitle}",
								labelFor: aCells[0].getId()
							}),
							aCells[0],
							new sap.m.Label({
								text: "{i18n>ecnWBSTitle}",
								labelFor: aCells[1].getId()
							}),
							aCells[1],
							new sap.m.Label({
								text: "{i18n>ecnWerkTitle}",
								labelFor: aCells[2].getId()
							}),
							aCells[2]
						];

					return aContent;
				},

				/********************************************************************************************
				 *                Pop-up window for new ECN creation
				 * *****************************************************************************************/

				// check required field and chenge the state
				checkRequiredField: function (oEvent) {
					var oValue = oEvent.getParameter("value"),
						oInput = oEvent.getSource();
					if (!oValue) {
						oInput.setValueState("Error");
					} else {
						if (oValue.length > 0) {
							oInput.setValueState("None");
						} else {
							oInput.setValueState("Error");
						}
					}
				},

				getNewProjectObject: function (oData) {
					var projectId = "ID" + parseInt(Math.random() * 100000000, 10);

					// create default properties
					var oProperties = {
						ProjectKey: projectId,
						ProjectDescription: oData.ProjectDescription,
						//PhaseOutDate: oData.PhaseOutDate,
						PhaseOutManager: oData.PhaseOutManager
					};

					return oProperties;
				},

				/*
				 * Shows the selected item on the object page
				 * On phones a additional history entry is created
				 * @param {sap.m.ObjectListItem} oItem selected Item
				 * @private
				 */

				/* *******************************************************************************************************************************************
				 *		dialog for add new ECNs
				 * ******************************************************************************************************************************************/

				onAddEcn: function (oEvent) {
					this.oEcnListDialog = this.byId("idSelectECNsDialog");
					if (!this.oEcnListDialog) {
						this.oEcnListDialog = sap.ui.xmlfragment(
							this.getView().getId(),
							"com.vaillant.plm.fiori.projectcockpit.view.fragments.DialogSelectECNs",
							this
						);

						// connect dialog to the root view of this component (models, lifecycle)
						this.getView().addDependent(this.oEcnListDialog);
					}
					this.oEcnListDialog.open();
				},

				onSubmitEcnDialog: function (oEvent) {
					var d1 = new Date();
					var oButton = oEvent.getSource();
					var oBinding = oButton.getBindingContext("serviceModel");
					var sProjectKey = oBinding.getObject().ProjectKey;

					// NOTE: We must clone the model data here. Otherwise "EcnKey" might get lost during pProcessEntitiesAssignment when the dialog closes!
					var aSelectedEcns = this.clone( this.getModel("UserSelectionModel").getProperty("/aSelectedEcns") );
					// Remove empty or undefined ECNs before submitting:
					aSelectedEcns = aSelectedEcns.filter(function(item){
						return (item.EcnKey && item.EcnKey!=="" );
					});

					var oDataModel = this.getModel("serviceModel");
					if (!sProjectKey || sProjectKey === "") {
						sap.m.MessageToast.show("Project key is missing!");
					} else {

						var sFunctionImportPath = "/selectEcn";
						var oTargetParameter = {
							"projectKey": sProjectKey
						};
						this.pProcessEntitiesAssignment(oDataModel, sFunctionImportPath, aSelectedEcns, oTargetParameter)
						.then(function () {
							var d2 = new Date();
							var d3 = d2 - d1;
							jQuery.sap.log.info(
								"onSubmitEcnDialog: Time elapsed in ms = ",
								d3
							);
							this.oEcnListDialog.close();
						}.bind(this))
						.catch(function (oError) {
							jQuery.sap.log.error("onSubmitEcnDialog failed: ", oError);
							sap.m.MessageToast.show("onSubmitEcnDialog failed!");
						}.bind(this));

					}
					return;
				},

				fnClearEcnDialogData: function (oEvent) {

					// TODO for Datamodel V2:
					// Just remove the local JSON Model where all the Project -> SelectedEcns are stored and close the dialog. That's it!
					//return;

					var oResetBtn = this.byId(
						$("button[id$='idResetAddEcnsTable']")[0].id
					);
					var sAggrName = oEvent.getParameter("origin").sParentAggregationName;

					oResetBtn.firePress({
						sDeleteMode: sAggrName === "endButton" ? "noPopup" : "clearTableOnly"
					});
				},

				fnEcnDialogClose: function () {
					// close dialog
					this.oEcnListDialog.close();
				},

				onCancelEcnDialog: function (oEvent) {
					var sMsgText = this.getResourceBundle().getText("resetNewEcnList");

					this.confirmDiscardAction.apply(this, [
						sMsgText, [this.fnEcnDialogClose]
					]);
				},

				onEditProjectHeaderData: function (oEvent) {
					this.bus.publish("flexible", "editProjectHeaderData");
				},

				fnEnabledSubmitBtnSelectedPlants: function (bEnabled) {
					var oMasterView = this.getModel("masterView");
					oMasterView.setProperty(
						"/bEnabledSubmitButtonSeletedPlants",
						bEnabled
					);
				},

				enableButtonAddPlants: function (bEnabled) {
					var oMasterView = this.getModel("masterView");
					oMasterView.setProperty("/bEnableButtonAddPlants", bEnabled);
				},

				enableButtonRemovePlants: function (bEnabled) {
					var oMasterView = this.getModel("masterView");
					oMasterView.setProperty("/bEnableButtonRemovePlants", bEnabled);
				},


				/* *******************************************************************************************************************************************
				 *		title with count for availble plants  (dialog for add plants)
				 * ******************************************************************************************************************************************/
				fnGetTitleWithCountForAvailblePlantsTable: function () {
					//var oBinding = this.oTable1.getBinding("rows");

					var oBinding = this.oTable1.getBinding("items");
					oBinding.attachDataReceived(function (data) {
						var iTotal = data.getParameter("data").results.length;
						var oMasterView = this.getModel("masterView");
						var oBundle = this.getResourceBundle();
						var sTitleText;

						if (iTotal > 0) {
							sTitleText = oBundle.getText("availablePlantsTitleCount", [
								iTotal
							]);
						} else {
							sTitleText = oBundle.getText("availablePlantsTitle");
						}

						oMasterView.setProperty("/availablePlantsTitle", sTitleText);
					}, this);
				},

				/* *******************************************************************************************************************************************
				 *		title with count for assigned plants (dialog for add plants)
				 * ******************************************************************************************************************************************/

				fnGetTitleWithCountForAssignedPlantsTable: function () {
					var oMasterView = this.getModel("masterView");
					var plants = oMasterView.getProperty("/aAssignedPlants") || {};
					var oBundle = this.getResourceBundle();
					var iTotal = Object.keys(plants).length;
					var sTitleText;

					if (iTotal > 0) {
						sTitleText = oBundle.getText("assignedPlantsTitleCount", [iTotal]);
					} else {
						sTitleText = oBundle.getText("assignedPlantsTitle");
					}

					oMasterView.setProperty("/assignedPlantsTitle", sTitleText);
				},

				/* *******************************************************************************************************************************************
				 *		enabled move arrows (dialog for add plants)
				 * ******************************************************************************************************************************************/

				onSelectPlantToBeAdded: function (oEvent) {
					var oTable1 = oEvent.getSource();
					//this.setSelectedRowsParam(oTable1, "/iSelectedRowsTable1");
					var oMasterView = this.getModel("masterView");
					if (oTable1.getSelectedItems().length > 0) {
						oMasterView.setProperty("/bEnableButtonAddPlants", true);
					} else {
						oMasterView.setProperty("/bEnableButtonAddPlants", false);
					}
				},

				onSelectPlantToBeRemoved: function (oEvent) {
					if (oEvent.getParameter("selected")) {
						this.nPlantsToBeRemoved++;
					} else {
						this.nPlantsToBeRemoved--;
					}

					if (this.nPlantsToBeRemoved > 0) {
						this.getModel("masterView").setProperty(
							"/bEnableRemovingEcns",
							true
						);
					} else {
						this.getModel("masterView").setProperty(
							"/bEnableRemovingEcns",
							false
						);
					}
				},

				setSelectedRowsParam: function (oTable, sParameter) {
					var oMasterView = this.getModel("masterView");
					oMasterView.setProperty(sParameter, oTable.getSelectedItems().length);
				},

				/* *******************************************************************************************************************************************
				 * Plant Assignment Dialog
				 * ******************************************************************************************************************************************/

				onPressAssignPlants: function (oEvent) {
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var oContext = oEvent.getSource().getBindingContext("serviceModel");
					this.oAddPlantsDialog = this.byId("idSelectPlantsDialog");
					if (!this.oAddPlantsDialog) {
						// create dialog via fragment factory
						this.oAddPlantsDialog = sap.ui.xmlfragment(
							this.getView().getId(),
							"com.vaillant.plm.fiori.projectcockpit.view.fragments.DialogSelectPlants",
							this
						);
						// connect dialog to the root view of this component (models, lifecycle)
						this.getView().addDependent(this.oAddPlantsDialog);
					}
					// bind ecn to the dialog
					this.oAddPlantsDialog.setBindingContext(oContext, "serviceModel");
					this.oAddPlantsDialog.open();

					// binding tables
					this.oTable1 = this.byId("idAvailablePlantsTable");
					this.oTable2 = this.byId("idAssignedPlantsTable");

					this.oAddPlantsDialog.setBusyIndicatorDelay(0);
					this.oAddPlantsDialog.setBusy(true);
					var sCurrentProjectKey = oUserSelectionModel.getProperty("/CurrentProjectKey");

					// Fill left table:
					this.pGetPotentialPlants(sCurrentProjectKey)

					.then(function (oData) {
						var aAvailablePlants = oData.response.results;
						oUserSelectionModel.setProperty("/aAvailablePlants", aAvailablePlants);
						this.oAddPlantsDialog.setBusy(false);
					}.bind(this))

					.catch(function (oError) {
						console.log(oError);
					});

					// Fill right table:
					var aSelectedPlants = this.clone(oUserSelectionModel.getProperty("/aAssignedPlants"));
					oUserSelectionModel.setProperty("/aSelectedPlants", aSelectedPlants);

				},

				onPressAddPlants: function () {
					var oMasterView = this.getModel("masterView");
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var oTable1 = this.getView().byId("idAvailablePlantsTable");
					var aSelectedItems = oTable1.getSelectedItems();

					var aAssignedPlants = oUserSelectionModel.getProperty("/aAssignedPlants") || [];
					var aSelectedPlants = oUserSelectionModel.getProperty("/aSelectedPlants") || [];
					var deletedPlants = [];

					// check plant key if it assigned already to ecn
					var fnCheckStatus = function (sPlantKey) {
						return this.aSelectedPlants.indexOf(sPlantKey) > -1 ? false : true;
					}.bind(this);

					var fnPlantsContainsPlantKey = function (aSelectedPlants, sPlantKey) {
						var bReturn = false;
						if (aSelectedPlants.forEach) {
							aSelectedPlants.forEach(function (oPlant) {
								if (oPlant.PlantKey === sPlantKey) {
									bReturn = true;
									return false; // break loop
								}
							});
						}
						return bReturn;
					}.bind(this);

					for (var i = 0; i < aSelectedItems.length; i++) {
						var oItem = aSelectedItems[i];
						var oPlantEntity = oUserSelectionModel.getProperty(
							oItem.getBindingContextPath()
						);
						if (!fnPlantsContainsPlantKey(aSelectedPlants, oPlantEntity.PlantKey)) {
							var bNewStatus = true; //fnCheckStatus(oPlantEntity.PlantKey);
							if (fnPlantsContainsPlantKey(aAssignedPlants, oPlantEntity.PlantKey)) {
								bNewStatus = false;
							}
							aSelectedPlants.push(this.fnGetPlantObject(
								oPlantEntity,
								bNewStatus
							));
						}

					}

					oUserSelectionModel.setProperty("/aSelectedPlants", aSelectedPlants);

					// Get the number of newly selected plants:
					var nNewPlants = Object.keys(aSelectedPlants).filter(function (item) {
						return aSelectedPlants[item].statusText === "new";
					}).length;

					// set enabled submit button
					this.fnEnabledSubmitBtnSelectedPlants(nNewPlants > 0);

					// get title for assined plants
					this.fnGetTitleWithCountForAssignedPlantsTable();

					// Clear selection after adding
					oTable1.removeSelections();
				},

				onPressRemovePlants: function () {
					var oMasterView = this.getModel("masterView");
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var oTable2 = this.getView().byId("idAssignedPlantsTable");
					var aSelectedItems = oTable2.getItems();

					var aSelectedPlants = oUserSelectionModel.getProperty("/aSelectedPlants") || [];
					var aRemovedPlants = [];

					for (var i = aSelectedPlants.length - 1; i > -1; i--) {
						if (aSelectedPlants[i].bSelected) {
							aRemovedPlants = aRemovedPlants.concat(aSelectedPlants.splice(i, 1));
						}
					}


					oTable2.removeSelections();

					oUserSelectionModel.setProperty("/aSelectedPlants", aSelectedPlants);
					oUserSelectionModel.setProperty("/aRemovedPlants", aRemovedPlants);

					// set enabled submit button
					var bEnableSubmitButton = (aRemovedPlants.length > 0);
					this.fnEnabledSubmitBtnSelectedPlants(bEnableSubmitButton);

					// get title for assined plants
					this.fnGetTitleWithCountForAssignedPlantsTable();
				},

				fnGetPlantObject: function (oObject, bNew) {
					var oBundle = this.getResourceBundle();
					var properties = {
						PlantKey: oObject.PlantKey,
						PlantDescription: oObject.PlantDescription,
						statusText: oBundle.getText(
							bNew ? "selectedPlantNew" : "selectedPlantAssigned"
						),
						statusState: bNew ? "Success" : "None",
						bCheckBoxEnabled: true,
						bSelected: false,
						bNew: bNew
					};
					return properties;
				},

				onSubmitSelectedPlants: function (oEvent) {
					var d1 = new Date();
					var oButton = oEvent.getSource();
					var oBinding = oButton.getBindingContext("serviceModel");
					var sProjectKey = oBinding.getObject().ProjectKey;

					var oMasterView = this.getModel("masterView");
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var aAssignedPlants = oUserSelectionModel.getProperty("/aAssignedPlants") || [];
					var aSelectedPlants = oUserSelectionModel.getProperty("/aSelectedPlants") || [];
					var aSelectedPlantsDifference = aSelectedPlants.filter(function (oPlant) {
						return oPlant.bNew;
					}) || [];

					var oDataModel = this.getModel("serviceModel");
					if (!sProjectKey || sProjectKey === "") {
						sap.m.MessageToast.show("Project key is missing!");
					} else {
						var sFunctionImportPath = "/selectPlant";
						var oTargetParameter = {
							"projectKey": sProjectKey
						};
						this.pProcessEntitiesAssignment(oDataModel, sFunctionImportPath, aSelectedPlantsDifference, oTargetParameter)
							.then(function (oData) {
								sFunctionImportPath = "/deselectPlant";
								var aRemovedPlants = oUserSelectionModel.getProperty("/aRemovedPlants") || [];
								return this.pProcessEntitiesAssignment(oDataModel, sFunctionImportPath, aRemovedPlants, oTargetParameter);
							}.bind(this))
							.then(function (oData) {
								var d2 = new Date();
								var d3 = d2 - d1;
								jQuery.sap.log.info(
									"onSubmitSelectedPlants: Time elapsed in ms = ",
									d3
								);
								//close the dialog and unselect rows
								this.onCancelAssignPlants();
								oUserSelectionModel.setProperty("/aRemovedPlants", []);
							}.bind(this))
							.catch(function (oError) {
								jQuery.sap.log.error("onSubmitSelectedPlants failed: ", oError);
								sap.m.MessageToast.show("onSubmitSelectedPlants failed!");
								oUserSelectionModel.setProperty("/aRemovedPlants", []);
							}.bind(this));
					}
					return;

				},

				onCancelAssignPlants: function () {
					this.oTable1.removeSelections();
					this.oTable2.removeSelections();
					this.oAddPlantsDialog.close();
				},

				/********************************************************************************************************************************************
				 * Helper functions:
				 *******************************************************************************************************************************************/

				refreshEcnTitle: function (oObject) {
					// set header of Detail Page
					var sTitle = this.getResourceBundle().getText(
						"FCLdetailTitle", [oObject.EcnDescription, oObject.EcnKey]
					);
					this.getModel("objectView").setProperty(
						"/sDetailTitle",
						sTitle
					);
				},

				/********************************************************************************************************************************************
				 *
				 * Event Handlers:
				 *
				 *******************************************************************************************************************************************/

				onSelectECN: function (oEvent) {
					this.sSelectedItemId = oEvent.getParameter("id");
					var oTable = oEvent.getSource().getParent();
					var aEcnTableItems = oTable.getItems();
					var oSelectedItem = aEcnTableItems.filter(function (item) {
						return item.getId() === oEvent.getParameter("id");
					})[0];

                    var oUserSelectionModel = this.getModel("UserSelectionModel");
					var aAssignedPlants = oUserSelectionModel.getProperty("/aAssignedPlants") || [];
					if(aAssignedPlants.length > 0){
					    // Show the detail page for the clicked ECN only if there is at least one assigned plant.
    					this.displaySelectedEcnItem(aEcnTableItems, oSelectedItem, aAssignedPlants);
					} else {
						var sMessage = this.getResourceBundle().getText("txtProjectHasNoPlants");
						sap.m.MessageToast.show(sMessage);
						this.fnOpenOnlyMasterPage();
					}
				},

				// Show only first FCL column
				fnOpenOnlyMasterPage: function () {
					var sFlexLayoutId = $("div[id$='fcl']")["0"].id;
					var oFlexlayout = sap.ui.getCore().byId(sFlexLayoutId);

					var oMidColumnPages = oFlexlayout.getMidColumnPages();
					if (oMidColumnPages.length > 0) {
						// Note by Adrian. 2020-11-04: After GoLive of Sprint-1.
						// Take care: This unbindElement() provokes the OData Error "Resource for Segment PotentialItems not found". Therefore, I comment it out and keep watching...
						//oMidColumnPages[0].unbindElement("serviceModel");
					}

					oFlexlayout.setLayout("OneColumn");
				},

				/* *******************************************************************************************************************************************
				 *		set as removed the item in ECN list
				 * ******************************************************************************************************************************************/
				onMarkEcnForDeletion: function (oEvent) {
					var oButton = oEvent.getSource();
					var sEcnPath = oButton.getBindingContext("serviceModel").getPath();
					var oDataModel = this.getModel("serviceModel");
					var sEcnKey = oDataModel.getProperty(sEcnPath + "/EcnKey");

					var oItem = oButton
						.getParent()
						.getParent()
						.getParent();
					var aCustomData = oButton.getCustomData();
					var oUserSelectionModel = this.getModel("UserSelectionModel");
					var aDeletedEcnList = oUserSelectionModel.getProperty("/aDeletedEcnList") || [];
					var oDeletedStatus;
					var sDeletedStatusValue;
					var sDeletedIcon;
					var sResetIcon;
					var sDeletedEcnKey;
					var sItemType;

					//get parameter from CustomData
					jQuery.each(aCustomData, function (i, data) {
						var sDataKey = data.getProperty("key");
						switch (sDataKey) {
						case "deletedEcnKey":
							sDeletedEcnKey = data.getProperty("value");
							break;
						case "deletedStatus":
							oDeletedStatus = data;
							sDeletedStatusValue = data.getProperty("value");
							break;
						case "deleteIcon":
							sDeletedIcon = data.getProperty("value");
							break;
						case "resetIcon":
							sResetIcon = data.getProperty("value");
						}
					});

					//change value of button
					if (sDeletedStatusValue === "true") {
						sDeletedStatusValue = "false";
						sItemType = "Navigation";
					} else {
						sDeletedStatusValue = "true";
						sItemType = "Inactive";

						//close View with Project Item Table if for this ECN the Project Item View has opened
						if (this.sSelectedItemId === oItem.getId()) {
							this.fnOpenOnlyMasterPage();
						}
					}
					oDeletedStatus.setProperty("value", sDeletedStatusValue);

					//change icon of button
					if (sDeletedStatusValue === "true") {
						oButton.setIcon(sResetIcon);
					} else {
						oButton.setIcon(sDeletedIcon);
					}

					// enable / disable press function
					oItem.setType(sItemType);

					//add ecn's path to the array for deleting after save
					//var iIndex = aDeletedEcnList.indexOf(sEcnPath);
					var iIndex = aDeletedEcnList.findIndex(function (item) {
						return item.EcnKey === sEcnKey;
					});

					if (iIndex < 0) {
						aDeletedEcnList.push({
							"EcnKey": sEcnKey
						});
					} else {
						aDeletedEcnList.splice(iIndex, 1);
					}

					// define parameters for deleted ecn
					oUserSelectionModel.setProperty("/aDeletedEcnList", aDeletedEcnList);
					oUserSelectionModel.setProperty(
						this.sCountEcnsMarkedAsDeleted,
						aDeletedEcnList.length
					);

					//set status and footer
					new BaseController().fnSetModifiedStatusToEcn.apply(this, [
						sDeletedEcnKey,
						sDeletedStatusValue
					]);
				}
			}
		);
	}
);