sap.ui.define(
	[
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
		"com/vaillant/plm/fiori/projectcockpit/model/formatter",
		"sap/ui/model/Sorter",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/m/MessageToast",
		"com/vaillant/plm/fiori/projectcockpit/common/js/Utils"
	],
	function (
		BaseController,
		JSONModel,
		History,
		formatter,
		Sorter,
		Filter,
		FilterOperator,
		MessageToast,
		Utils
	) {
		"use strict";

		return BaseController.extend( "com.vaillant.plm.fiori.projectcockpit.controller.FCLdetail", {

				formatter: formatter,
				
				/* =========================================================== */
				/* lifecycle methods                                           */
				/* =========================================================== */

				/**
				 * Called when the worklist controller is instantiated.
				 * @public
				 */
				onInit: function () {
					this.bus = sap.ui.getCore().getEventBus();
					//var oBoundle = sap.ui.controller("com.vaillant.plm.fiori.projectcockpit.controller.BaseController").getResourceBundle();

					// Model used to manipulate control states
					var oViewModel = new JSONModel({
						bFlatTable: true,
						bExpandCollapseTable: true,
						bFlatTableEnabled: false,
						bCellFilterOn: false,
						bAvailabilityFilterOn: false,
						sGlobalFilter: "",
						bRootElement: false,
						sTableModeText: null,
						sTableModeState: null,
						sCurrentLocation: null,
						bEnabledFilterResetButton: false,
						sProjectItemsTitle: ""
					});
					this.setModel(oViewModel, "detailModel");

					//global variable for i18n texts
					this.oBundle = null;

					// array column filters
					this.aColumnFilters = null;


					//selected item
					this.sSelectedItemId = null;

				},

				onBeforeRendering: function () {
					// set tree mode for table as default
					if (!this.oBundle) {
						this.oBundle = this.getModel("i18n").getResourceBundle();
					}
				},

				onAfterRendering: function (oEvent) {
					console.log("FCLdetail - onAfterRendering");
				},

				/**
				 * Click handlers:
				 *
				 */

				onCloseDetailView: function (oEvent) {
					this.bus.publish("flexible", "closeDetailPage");
				},

				fnUpdateTitleWithCounter: function (nCounter) {
					var sTitle;
					if (nCounter && nCounter > 0) {
						sTitle = this.oBundle.getText("tableHeaderCountTitle", [nCounter]);
					} else {
						sTitle = this.oBundle.getText("tableHeaderTitle");
					}
					this.getModel("detailModel").setProperty(
						"/sProjectItemsTitle",
						sTitle
					);
				},


				/****************************************************************************************************************************/
				/***************               popover for display material master data partially                              ****************/
				/****************************************************************************************************************************/

				_PopoverMaterialData: null,

				onInvokePopover: function (oEvent, sFragmentName, sEntitySetName) {
					var oDataModel = this.getModel("serviceModel");
					var oController = oEvent.getSource();
					var oPopoverDialog;
					var sObjectPath;

					//get popover dialog
					oPopoverDialog = this.fnGetPopoverMatDataDialog(sFragmentName);

					// generate path to the material object
					/*
					 */
					if (true) {
						sObjectPath = oDataModel.createKey("/" + sEntitySetName, {
							"MaterialKey": oEvent
								.getSource()
								.getCustomData()[0]
								.getKey(),
							"PlantKey": oEvent
								.getSource()
								.getCustomData()[1]
								.getKey()
						});
					} else {
						sObjectPath = oDataModel.createKey("/" + sEntitySetName, {
							MARA_MATNR: oEvent
								.getSource()
								.getCustomData()[0]
								.getKey(),
							MARC_WERKS: oEvent
								.getSource()
								.getCustomData()[1]
								.getKey()
						});
					}

					// bind data of element
					oPopoverDialog.bindElement({
						path: sObjectPath,
						model: "serviceModel"
					});

					// Take care that the dialog is opened after its data is available
					oPopoverDialog.openBy(oController);
				},

				fnGetPopoverMatDataDialog: function (sFragmentName) {
					var oDialog;

					if (!this._PopoverMaterialData) {
						this._PopoverMaterialData = {};
					}

					if (!this._PopoverMaterialData[sFragmentName]) {
						oDialog = sap.ui.xmlfragment(
							"com.vaillant.plm.fiori.projectcockpit.view.fragments." +
							sFragmentName,
							this
						);

						this._PopoverMaterialData[sFragmentName] = oDialog;

						this.getView().addDependent(oDialog);
					} else {
						oDialog = this._PopoverMaterialData[sFragmentName];
					}
					return oDialog;
				},

				onClickMaterialKey: function (oEvent) {
					this.onInvokePopover(
						oEvent,
						"MaterialDetails",
						"MasterDataEntitySet"
					);
				},

				onClickPlantKey: function (oEvent) {
					this.onInvokePopover(
						oEvent,
						"PlantDetails",
						"MasterDataEntitySet"
					);
				},

				onClickShowDQMessage: function (oEvent) {
					this.onInvokePopover(
						oEvent,
						"DQMessageDetails",
						"StatusManagementEntitySet"
					);
				},

				onClickShowODataErrorMessage: function (oEvent) {
					this.onInvokePopover(
						oEvent,
						"InconsistentOData",
						"StatusManagementEntitySet"
					);
				},

				/****************************************************************************************************************************/
				/***************               press Material item / open Detail Details Layout                              ****************/
				/****************************************************************************************************************************/

				onSelectRow: function (oEvent) {

					// An empty row should not react on click.
					if (!oEvent.getParameters().rowBindingContext) {
						return;
					}

					// A cell with action switch should not open info view on click.
					var oCell = oEvent.getParameters().cellControl;
					if (oCell.getItems) {
						if (oCell.getItems()[0]) {
							if (oCell.getItems()[0].getVisible()) {
								return;
							}
						}
					}

					// Read Item Path:
					var oRowBindigContext = oEvent.getParameter("rowBindingContext");

					var sSelectedMaterialKey = oRowBindigContext.getProperty(oRowBindigContext.sPath + "/MaterialKey");
					var sSelectedPlantKey = oRowBindigContext.getProperty(oRowBindigContext.sPath + "/PlantKey");

					var params = {
						MaterialKey: sSelectedMaterialKey,
						PlantKey: sSelectedPlantKey
					};

					this.bus.publish("flexible", "setDetailDetailPage", params);

				},


				fnGetSmartTable: function () {
					return this.getView().byId("TableBinding");
				},


				onProjectItemTogglePhasedOut: function (oEvent) {
					// Marking an item for Phase-Out means to add it to the list of items to be phased out.
					var oButton = oEvent.getSource();

					var sRowPath = oButton.getBindingContext("serviceModel").getPath();
					var oDataModel = this.getModel("serviceModel");

					var sCurrentProjectKey = this.getModel("UserSelectionModel").getProperty("/CurrentProjectKey");
					var sCurrentEcnKey = this.getModel("UserSelectionModel").getProperty("/CurrentEcnKey");

					var sTargetState = "true";
					var bState = oDataModel.getProperty(sRowPath + "/AssignedProject/ProjectKey") === sCurrentProjectKey;
					if (bState) {
						sTargetState = "false";
					}

					var aPhaseOutItemList = this.getModel("UserSelectionModel").getProperty("/aPhaseOutItemList") || [];
					var oPhaseOutItem = {
						"MaterialKey": oDataModel.getProperty(sRowPath + "/MaterialKey"),
						"PlantKey": oDataModel.getProperty(sRowPath + "/PlantKey"),
						"EcnKey": sCurrentEcnKey,
						"ShouldBePhasedOut": sTargetState
					};

					var sDisplayStatus = oDataModel.getProperty(sRowPath + "/StatusManagement/MaterialPlantItemStatus");

					// show the correct icon with the correct coloring
					if (sDisplayStatus === "transfer_phaseout_true" ||  sDisplayStatus === "transfer_phaseout_false") {

						var sOldStatus = oDataModel.getProperty(sRowPath + "/StatusManagement/MaterialPlantItemOldStatus");
						oDataModel.setProperty(sRowPath + "/StatusManagement/MaterialPlantItemStatus", sOldStatus);
						// undo the change
						var iFound = -1;
						aPhaseOutItemList.forEach(function (item, index) {
							if (oPhaseOutItem.MaterialKey === item.MaterialKey && oPhaseOutItem.PlantKey === item.PlantKey) {
								iFound = index;
								return false; // break
							}
						});
						aPhaseOutItemList.splice(iFound, 1);

					} else {

						if (sTargetState === "false") {
							oDataModel.setProperty(sRowPath + "/StatusManagement/MaterialPlantItemStatus", "transfer_phaseout_false");
						} else {
							oDataModel.setProperty(sRowPath + "/StatusManagement/MaterialPlantItemStatus", "transfer_phaseout_true");
						}
						// Mark for change
						aPhaseOutItemList.push(oPhaseOutItem);

					}

					this.getModel("UserSelectionModel").setProperty("/aPhaseOutItemList", aPhaseOutItemList);

					// update project footer
					sap.ui.controller("com.vaillant.plm.fiori.projectcockpit.controller.FCLframe").fnCheckShowSaveButton.apply(this);

				},

				/***********************************************************************************************************************************
				 *		filter for tree table
				 * ********************************************************************************************************************************/

				fnColumnFilter: function (oEvent) {
					var oColumn = oEvent.getParameter("column");

					var sValue = oEvent.getParameter("value");
					if (sValue) {
						sValue = sValue.toUpperCase();
					}
					sValue = this.formatter.formatWildCards( sValue );

					var sFieldName = oColumn.getProperty("filterProperty");
					var sFilterOperator = oColumn.getProperty("filterOperator") || "Contains";
					var sFilterType = oColumn.getProperty("filterType").getName();
					var oFilter;
					var bAddNewFilter = true;

					//clear column multi filter
					this.multiColumnFilter = null;

					// copy filter value to all tables
					//this.fnSyncSetColumnFilterValue(sFieldName, sValue);

					if (sValue || this.aColumnFilters) {
						// convert filter value to the boolean
						if (sFilterType === "Boolean") {
							sValue = sValue.toLocaleLowerCase();
							if (sValue === "yes" || sValue === "true" || sValue === "ja") {
								sValue = true;
							} else {
								sValue = false;
							}
						}
						if (!this.aColumnFilters) {
							this.aColumnFilters = [];
						} else {
							for (var i = 0; i < this.aColumnFilters.length; i++) {
								var filter = this.aColumnFilters[i];
								if (filter.sPath === sFieldName) {
									if (sValue.length > 0 || typeof sValue === "boolean") {
										filter.oValue1 = sValue;
									} else {
										this.aColumnFilters.splice(i, 1);
									}
									bAddNewFilter = false;
								}
							}
						}

						if (bAddNewFilter) {
							oFilter = new Filter(sFieldName, sFilterOperator, sValue);
							this.aColumnFilters.push(oFilter);
						}

						var iLength = this.aColumnFilters.length;
						if (iLength > 1) {
							this.multiColumnFilter = new sap.ui.model.Filter(
								this.aColumnFilters,
								true
							);
						} else if (iLength === 1) {
							this.multiColumnFilter = this.aColumnFilters[0];
						} else {
							this.multiColumnFilter = null;
						}

					}
					// execute filter function
					this.fnFilterTable();
				},

				fnFilterTable: function () {
					var oFilter = null;
					var bEnabled = true;
					var oDetailModel = this.getModel("detailModel");

					if (this.multiColumnFilter) {
						oFilter = this.multiColumnFilter;
					} else {
						bEnabled = false;
					}

					// enabled/disabled reset filter button
					oDetailModel.setProperty("/bEnabledFilterResetButton", bEnabled);

                    var oUserSelectionModel = this.getModel("UserSelectionModel");
					var aAssignedPlants = oUserSelectionModel.getProperty("/aAssignedPlants") || [];
					var aFilterArray = [];
					aAssignedPlants.forEach(function (oPlant) {
						aFilterArray.push(new Filter("PlantKey", "EQ", oPlant.PlantKey));
					});
					var oDefaultApplicationFilter = new Filter(aFilterArray, false);

					// VERY IMPORTANT: The filter must be cleaned up properly before using a new filter!
					this.fnGetSmartTable().getBinding("rows").aFilters = null;

					if(oFilter){
						var oCombinedFilter = new Filter( [oDefaultApplicationFilter, oFilter], true );
						
						// If the multi column filter contains "PlantKey", do not use the default application filter!
						if(oFilter.sPath && oFilter.sPath === "PlantKey"){
							oCombinedFilter = oFilter;
						}
						if(oFilter.aFilters && oFilter.aFilters.length > 0){
							var aColumnFilterOnPlantKey = oFilter.aFilters.filter(function(item){
								return item.sPath === "PlantKey";
							});
							if(aColumnFilterOnPlantKey.length > 0){
								oCombinedFilter = oFilter;
							}
						}

						this.fnGetSmartTable().getBinding("rows").filter(oCombinedFilter, "Application"); // Must use FilterType = "Application" to combine non-control filters

					} else {

						this.fnGetSmartTable().getBinding("rows").filter(oDefaultApplicationFilter, "Application"); // Must use FilterType = "Application" on non-control filters

					}
				},

				fnClearAllFilters: function () {
					this.aColumnFilters = null; // column filter
					this.multiColumnFilter = null; // column filter

					var oDetailModel = this.getView().getModel("detailModel");
					oDetailModel.setProperty("/sGlobalFilter", "");
					oDetailModel.setProperty("/bAvailabilityFilterOn", false);

					//turn off RootElement mode
					oDetailModel.setProperty("/bRootElement", false);

					// clear column filters
					var oTable = this.fnGetSmartTable();
					var aColumns = oTable.getColumns();
					aColumns.forEach( function(column) {
						column.setFilterValue("");
						column.setFiltered(false);
					});

					this.fnFilterTable();
				},

				fnReload: function () {
					var oDataModel = this.getModel("serviceModel");
					oDataModel.refresh();
				},

				/* ******************************************************************************************************************
				 *		get property path for project item entity
				 * *****************************************************************************************************************/
				fnGetProjectItemPath: function (sMaterialKey, sPlantKey, property) {
					var oDataModel = this.getModel("serviceModel");
					var sObjectPath = oDataModel.createKey( "ProjectItemEntitySet", {
						MaterialKey: sMaterialKey,
						PlantKey: sPlantKey
					});

					if (property) {
						return "/" + sObjectPath + "/" + property;
					} else {
						return "/" + sObjectPath;
					}
				},

				/**********************************************************************************************************************
				 *		switch between sap.ui.table.TreeTable and sap.ui.table.Table
				 * *******************************************************************************************************************/

				_fragmentList: null,

				/* ********************************************************************************************************************
				 *		synchronize column filter between sap.ui.table.TreeTable and sap.ui.table.Table
				 * *******************************************************************************************************************/
				fnSyncSetColumnFilterValue: function (sFieldName, sValue) {
					jQuery.each(
						this._fragmentList,
						function (fragment, table) {
							if (fragment !== this.idTreeTable) {
								jQuery.each(table.getColumns(), function (i, column) {
									var sColumnFilterKey =
										column.getProperty("filterProperty") || null;
									if (sColumnFilterKey === sFieldName) {
										column.setFilterValue(sValue);
										column.setFiltered(sValue === "" ? false : true);
									}
								});
							}
						}.bind(this)
					);
				},

				fnSyncRemoveColumnFilter: function () {
					jQuery.each(
						this._fragmentList,
						function (fragment, table) {
							jQuery.each(table.getColumns(), function (i, column) {
								column.setFilterValue("");
								column.setFiltered(false);
							});
						}.bind(this)
					);
				}
			}
		);
	}
);