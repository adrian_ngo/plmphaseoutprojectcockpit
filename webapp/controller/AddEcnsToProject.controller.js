sap.ui.define(
	[
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/m/MessageToast",
		"sap/ui/core/Fragment",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"com/vaillant/plm/fiori/projectcockpit/common/js/Utils"
	],
	function (BaseController, JSONModel, MessageToast, Fragment, Filter, FilterOperator, Utils) {
		"use strict";
		return BaseController.extend("com.vaillant.plm.fiori.projectcockpit.controller.AddEcnsToProject", {

			currentEcnPath: "", // in UserSelectionModel

			onInit: function () {
				this.oSuggestionModel = new JSONModel({
					EcnValueHelpEntitySet: []
				});
				this.setModel(this.oSuggestionModel, "suggestionModel");

			},


			onShowEcnValueHelp1: function (oEvent) {
				var sInputValue = oEvent.getSource().getValue();

				this.currentEcnPath = oEvent.getSource().getBindingContext("UserSelectionModel").getPath();

				if (!this._oValueHelpDialog) {
					Fragment.load({
						name: "com.vaillant.plm.fiori.projectcockpit.view.fragments.EcnValueHelpDialog",
						controller: this
					}).then(function (oFragment) {
						this._oValueHelpDialog = oFragment;
						this.getView().addDependent(this._oValueHelpDialog);
	
						// Create a filter for the binding
						this._oValueHelpDialog.getBinding("items")
							.filter([new Filter("EcnKey", FilterOperator.Contains, sInputValue)]);
						// Open ValueHelpDialog filtered by the input's value
						this._oValueHelpDialog.open(sInputValue);
					}.bind(this));
				} else {
					// Create a filter for the binding
					this._oValueHelpDialog.getBinding("items")
						.filter([new Filter("EcnKey", FilterOperator.Contains, sInputValue)]);
					// Open ValueHelpDialog filtered by the input's value
					this._oValueHelpDialog.open(sInputValue);
				}
			},
			
				
			onShowEcnValueHelp: function (oEvent) {
				var oBundle = this.getModel("i18n").getResourceBundle();
				var sInputValue = oEvent.getSource().getValue();
				this.currentEcnPath = oEvent.getSource().getBindingContext("UserSelectionModel").getPath();
				if (!this.oEcnSearchDialog) {
					this.oEcnSearchDialog = new sap.m.Dialog({
						contentHeight: "80%",
						contentWidth: "auto",
						title: oBundle.getText("txtAvailableECNs"),
						content: new sap.m.List({
							headerToolbar: new sap.m.OverflowToolbar({
								content: [
									new sap.m.SearchField({
										width: "30rem",
										placeholder: oBundle.getText("txtSearchEcnKeyOrDescription"),
										search: function(oEvent){
											var sFilterQuery = oEvent.getSource().getValue();
											var oList = oEvent.getSource().getParent().getParent();
											var oFilter = new Filter ([
												new Filter("EcnKey", FilterOperator.Contains, sFilterQuery),
												new Filter("EcnDescription", FilterOperator.Contains, sFilterQuery)
											], false);
											oList.getBinding("items").filter(oFilter, "Application");
										}
									})
								]
							}),
							mode: sap.m.ListMode.SingleSelect,
							select: function(oEvent){
								var oSelectedItem = oEvent.getSource().getSelectedItems()[0];
								if(this.currentEcnPath !== ""){
									var sPath = this.currentEcnPath;
									this.getModel("UserSelectionModel").setProperty(sPath + "/EcnKey", oSelectedItem.getTitle());
									this.getModel("UserSelectionModel").setProperty(sPath + "/sEcnDescription", oSelectedItem.getDescription());
									this.getModel("UserSelectionModel").setProperty(sPath + "/bShowEcnDescription", true);
									this.getModel("UserSelectionModel").setProperty("/bEcnSelectionEnableSubmitButton", true);
								}
							}.bind(this),
							items: {
								path: 'serviceModel>/EcnValueHelpEntitySet',
								templateShareable: true,
								sorter: {
									path: 'EcnKey'
								},
								filters: [new Filter("EcnKey", FilterOperator.Contains, sInputValue)],
								template: new sap.m.StandardListItem({
									title: "{serviceModel>EcnKey}",
									description: "{serviceModel>EcnDescription}"
								})
							},
							growing: true,
							growingThreshold: 100,
							growingScrollToLoad: false
						}),
						beginButton: new sap.m.Button({
							type: sap.m.ButtonType.Emphasized,
							text: oBundle.getText("applyBtnTitle"),
							press: function () {
								this.oEcnSearchDialog.close();
							}.bind(this)
						}),
						endButton: new sap.m.Button({
							text: oBundle.getText("cancelBtnTitle"),
							press: function (oEvent) {
								this.oEcnSearchDialog.close();
								if(this.currentEcnPath !== ""){
									var sPath = this.currentEcnPath;
									this.getModel("UserSelectionModel").setProperty(sPath + "/EcnKey", "");
									this.getModel("UserSelectionModel").setProperty(sPath + "/sEcnDescription", "");
									this.getModel("UserSelectionModel").setProperty(sPath + "/bShowEcnDescription", false);
									this.getModel("UserSelectionModel").setProperty("/bEcnSelectionEnableSubmitButton", false);
								}
								
							}.bind(this)
						})
					});
	
					// to get access to the controller's model
					this.getView().addDependent(this.oEcnSearchDialog);
				}
	
				this.oEcnSearchDialog.open();
				// Reset ECN search
				var oList = this.oEcnSearchDialog.getContent()[0];
				if(oList){
					oList.removeSelections(); // Remove any previous selection
					var oSearchField = oList.getHeaderToolbar().getContent()[0];
					if(oSearchField){
						// Remove any previous search string and reload
						oSearchField.setValue("");
						oSearchField.fireSearch();
					}
				}
			},

			onValueHelpSearch: function (oEvent) {
				var sValue = oEvent.getParameter("value");
				var oFilter = new Filter(
					[ 
						new Filter("EcnKey", FilterOperator.Contains, sValue),
						new Filter("EcnDescription", FilterOperator.Contains, sValue)
					], 
					false // OR-filter
				);
				oEvent.getSource().getBinding("items").filter([oFilter]);
			},
	
			onValueHelpClose: function (oEvent) {
				var oSelectedItem = oEvent.getParameter("selectedItem");
				oEvent.getSource().getBinding("items").filter([]);
	
				if (!oSelectedItem) {
					return;
				}
	

				if(this.currentEcnPath !== ""){
					var sPath = this.currentEcnPath;
					this.getModel("UserSelectionModel").setProperty(sPath + "/EcnKey", oSelectedItem.getTitle());
					this.getModel("UserSelectionModel").setProperty(sPath + "/sEcnDescription", oSelectedItem.getDescription());
					this.getModel("UserSelectionModel").setProperty(sPath + "/bShowEcnDescription", true);
					this.getModel("UserSelectionModel").setProperty("/bEcnSelectionEnableSubmitButton", true);
				}

			},			


			onSuggestECNs: function (oEvent) {
				var s1 = oEvent.getParameters().suggestValue;
				var oInput = oEvent.getSource();
				//console.log( "s1=" + s1 );
				var oDataModel = oEvent
					.getSource()
					.getBindingContext("serviceModel")
					.getModel();
				oDataModel.read("/EcnValueHelpEntitySet", {
					urlParameters: {
						_: jQuery.now()
					},
					success: function (oData, response) {
						oInput.destroySuggestionItems();
						var iLength = oData.results.length;
						//console.log( "iLength=" + iLength );
						var aEcnList = oData.results.filter(function (item) {
							return (
								item.EcnKey.toLowerCase().indexOf(s1.toLowerCase()) > -1 ||
								item.EcnDescription.toLowerCase().indexOf(s1.toLowerCase()) >
								-1
							);
						});
						//console.log( "aEcnList.length = " + aEcnList.length );
						aEcnList.forEach(function (item) {
							//console.log(item.EcnKey + "   " + item.EcnDescription);
							oInput.addSuggestionItem(
								new sap.ui.core.Item({
									key: item.EcnKey,
									text: item.EcnKey + " - " + item.EcnDescription
								})
							);
						});

					},
					error: function (oError) {
						MessageToast.show("Faild to read data");
					}
				});
			},

			onAfterRendering: function () {
				//update title of ecn list
				this.onUpdateTitleCount();
				this.getModel("UserSelectionModel").setProperty("/bEcnSelectionEnableSubmitButton", false);
			},

			onUpdateTitleCount: function () {
				// update the worklist's object counter after the table update
				var sTitle;
				var oTableTitle = this.byId("idAddEcnListTitle");
				var oTable = this.getTable();
				var iTotalItems = oTable.getItems().length;
				var oBundle = this.getModel("i18n").getResourceBundle();
				// only update the counter if the length is final and
				// the table is not empty
				if (iTotalItems) {
					sTitle = oBundle.getText("addEcnListCountTitle", [iTotalItems]);
				} else {
					sTitle = oBundle.getText("addEcnListTitle");
				}
				//this.oViewController.getModel("worklistView").setProperty("/addEcnListTitle", sTitle);
				oTableTitle.setText(sTitle);
			},

			getTable: function () {
				return this.byId("idEcnList");
			},

			onPressAddECN: function () {

				var aSelectedEcns = this.getModel("UserSelectionModel").getProperty("/aSelectedEcns");
				if (aSelectedEcns) {
					// Adds another empty line to the dynamic table "idEcnList" of ECNs. See "AddEcnsToProject.view.xml".
					aSelectedEcns.push({
						"index": aSelectedEcns.length,
						"bShowEcnDescription": false
					});
				} else {
					// Adds first empty line to the dynamic table "idEcnList" of ECNs. See "AddEcnsToProject.view.xml".
					aSelectedEcns = [{
						"index": 0,
						"bShowEcnDescription": false
					}];
				}
				this.getModel("UserSelectionModel").setProperty("/aSelectedEcns", aSelectedEcns);

				return;
			},

			onPressResetECNList: function (oEvent) {
				var oUserSelectionModel = this.getModel("UserSelectionModel");
				oUserSelectionModel.setProperty("/aSelectedEcns", []);
				oUserSelectionModel.setProperty("/bEcnSelectionEnableSubmitButton", false);
				return;
			},



			// PLM2-1532: Select all plants for after "Add ECN"
			onSelectAllPLants: function (oEvent) {
				var oButton = oEvent.getSource();
				var oMultiComboBox = oButton.getParent().getCells()[1];
				var bSubmitEnabled = false;
				var sValueState = "Error";
				var aAllKeys = oMultiComboBox.getKeys();
				var aSelectedKeys = oMultiComboBox.getSelectedKeys();
				if (aAllKeys.length > aSelectedKeys.length) {
					// Select all Plants
					oMultiComboBox.setSelectedKeys(aAllKeys);
					bSubmitEnabled = true;
					sValueState = "None";
					oButton.setText("Deselect all");
				} else {
					// Deselect all Plants
					oMultiComboBox.setSelectedKeys([]);
					oButton.setText("Select all");
					sValueState = "Error";
				}
				// set field state
				oMultiComboBox.setValueState(sValueState);
				// enabled submit button
				this.fnEcnDataIsAvailable(bSubmitEnabled);
			},
			
			

			onPressRemoveECN: function (oEvent) {

				var oUserSelectionModel = this.getModel("UserSelectionModel");
				var aSelectedEcns = oUserSelectionModel.getProperty("/aSelectedEcns");
				var sPath = oEvent.getSource().getBindingContext("UserSelectionModel").sPath;
				var index = sPath.substr(sPath.lastIndexOf("/") + 1);
				aSelectedEcns.splice(index, 1);
				oUserSelectionModel.setProperty("/aSelectedEcns", aSelectedEcns);
				if (!aSelectedEcns || aSelectedEcns.length < 1) {
					oUserSelectionModel.setProperty("/bEcnSelectionEnableSubmitButton", false);
				}

				return;

			},





			fnOnSelectSuggestedECN: function (oEvent) {
				var oInputField = oEvent.getSource();
				var sValueState = "Error";
				var oVBox = oInputField.getParent();
				var oEcnDescription = oVBox.getItems()[1];
				var bEnabledSubmit = false;

				var oRow = oVBox.getParent();

				var oSelectedItem = oEvent.getParameter("selectedItem");
				var sNewEcnKey = oSelectedItem.getProperty("key");
				var sNewEcnDescription = oSelectedItem.getProperty("additionalText");

				var sPath = oEcnDescription.getBindingContext("UserSelectionModel").getPath();
				this.getModel("UserSelectionModel").setProperty(sPath + "/EcnKey", sNewEcnKey);
				this.getModel("UserSelectionModel").setProperty(sPath + "/sEcnDescription", sNewEcnDescription);
				this.getModel("UserSelectionModel").setProperty(sPath + "/bShowEcnDescription", true);

				this.getModel("UserSelectionModel").setProperty("/bEcnSelectionEnableSubmitButton", true);

				//oBinding.getModel().setProperty(sPath + "/sEcnDesription", sNewEcnDescription );
				//oBinding.getModel().setProperty(sPath + "/bShowEcnDesription", true );

				return;
			},

			/* *********************************************************************************************************************
			 *			display data in ECN's row
			 * *********************************************************************************************************************/


			fnTockenChange: function (oEvent) {
				var oToken = oEvent.getParameter("token");
				var sActionType = oEvent.getParameter("type");
				if (sActionType === "added") {
					oToken.setProperty("text", oToken.getProperty("key"));
				}
			},


			/************************************************************************************************************************
			 *  set enabled / disabled reset ecns table
			 * *********************************************************************************************************************/

			fnSetEnabledResetEcnTableBtn: function (bEnabled) {
				var oButton = this.byId("idResetAddEcnsTable");
				oButton.setEnabled(bEnabled);
			},

			/************************************************************************************************************************
			 *  check if row is filled out
			 * *********************************************************************************************************************/

			fnCheckEcnIsSelectedInRow: function () {
				var bCheck = true;
				jQuery.each($(".myRequired"), function (i, combobox) {
					var oInput = sap.ui.getCore().byId(combobox.id);
					var bEnabled = oInput.getEnabled();

					if (!oInput.getSelectedKey() && bEnabled) {
						oInput.setValueState("Error");
						bCheck = false;
						return false;
					}
				});

				return bCheck;
			},

			fnCheckPlantAreSelectedInRow: function () {
				var bCheck = true;
				jQuery.each($(".sapMMultiComboBox"), function (i, combobox) {
					var oInput = sap.ui.getCore().byId(combobox.id);
					var bEnabled = oInput.getEnabled();

					if (!bEnabled) {
						bCheck = false;
						return false;
					} else if (oInput.getSelectedKeys().length === 0 && bEnabled) {
						oInput.setValueState("Error");
						bCheck = false;
						return false;
					}
				});

				return bCheck;
			},
			/* ***********************************************************************************************************************
			 *  change selection in multicombobox for plants
			 * *********************************************************************************************************************/
			fnChangeSelectionPlantsList: function (oEvent) {
				var bSelected = oEvent.getParameter("selected");
				var oMultiBox = oEvent.getSource();
				var oButton = oEvent
					.getSource()
					.getParent()
					.getCells()[3];

				var aSelectedKeys = oMultiBox.getSelectedKeys();
				var aAllKeys = oMultiBox.getKeys();

				var bSubmitEnabled = true;
				var sValueState = "None";

				if (aSelectedKeys.length === 0) {
					bSubmitEnabled = false;
					sValueState = "Error";
					oButton.setText("Select All");
				} else if (aSelectedKeys.length === aAllKeys.length) {
					oButton.setText("Deselect all");
				} else {
					oButton.setText("Select All");
				}

				// set field state
				oMultiBox.setValueState(sValueState);

				if (bSelected && sValueState === "None") {
					bSubmitEnabled =
						this.fnCheckEcnIsSelectedInRow() &&
						this.fnCheckPlantAreSelectedInRow();
				}

				// enabled submit button
				this.fnEcnDataIsAvailable(bSubmitEnabled);
			},

			/************************************************************************************************************************
			 *  enabled / disabled submit button
			 * *********************************************************************************************************************/

			fnEcnDataIsAvailable: function (bEnabled) {
				var oAppModel = this.getModel("appView");
				oAppModel.setProperty("/bEcnListIsFilled", bEnabled);
			}
		});
	}
);