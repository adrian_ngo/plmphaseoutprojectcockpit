sap.ui.define(
	[
		"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
		"com/vaillant/plm/fiori/projectcockpit/controller/DemoPersoService",
		"sap/m/TablePersoController",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"com/vaillant/plm/fiori/projectcockpit/common/js/Utils"
	],
	function (
		BaseController,
		JSONModel,
		History,
		DemoPersoService,
		TablePersoController,
		Filter,
		FilterOperator,
		Utils
	) {
		"use strict";

		return BaseController.extend("com.vaillant.plm.fiori.projectcockpit.controller.ProjectWorklist", {

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */
			oUtils: null,

			getAllVariants: function () {
				var oPersonalizationVariantSet = {},
					aExistingVariants = [],
					aVariantKeysAndNames = [];
				//get the personalization service of shell
				this._oPersonalizationService = sap.ushell.Container.getService(
					"Personalization"
				);
				this._oPersonalizationContainer = this._oPersonalizationService.getPersonalizationContainer(
					"MyVariantContainer"
				);
				this._oPersonalizationContainer.fail(
					function () {
						// call back function in case of fail
						this.fbCallback(aExistingVariants);
					}.bind(this)
				);
				this._oPersonalizationContainer.done(
					function (oPersonalizationContainer) {
						// check if the current variant set exists, If not, add the new variant set to the container
						if (!oPersonalizationContainer.containsVariantSet(
								"MyApplicationVariants"
							)) {
							oPersonalizationContainer.addVariantSet(
								"MyApplicationVariants"
							);
						}
						// get the variant set
						oPersonalizationVariantSet = oPersonalizationContainer.getVariantSet(
							"MyApplicationVariants"
						);
						aVariantKeysAndNames = oPersonalizationVariantSet.getVariantNamesAndKeys();
						for (var key in aVariantKeysAndNames) {
							if (aVariantKeysAndNames.hasOwnProperty(key)) {
								var oVariantItemObject = {};
								oVariantItemObject.VariantKey = aVariantKeysAndNames[key];
								oVariantItemObject.VariantName = key;
								aExistingVariants.push(oVariantItemObject);
							}
						}
						this.fbCallback(aExistingVariants);
					}.bind(this)
				);
			},

			fbCallback: function () {
				return;
				var oVariantMgmtControl = this.getView().byId("variantManagement"),
					// create a new model
					oVariantModel = new JSONModel();
				//get Variants from personalization set and set the model of variants list to variant managment control
				this.getAllVariants(
					function (aVariants) {
						oVariantModel.oData.Variants = aVariants;
						oVariantMgmtControl.setModel(oVariantModel);
						//enable save button
						oVariantMgmtControl.oVariantSave.onAfterRendering = function () {
							this.setEnabled(true);
						};
					}.bind(this)
				);
			},

			initPerso: function () {
				// Peronalisation from ushell service to persist the settings
				if (
					sap.ushell &&
					sap.ushell.Container &&
					sap.ushell.Container.getService
				) {
					var oComponent = sap.ui.core.Component.getOwnerComponentFor(
						this.getView()
					);
					this.oPersonalizationService = sap.ushell.Container.getService(
						"Personalization"
					);
					var oPersId = {
						container: "TablePersonalisation", //any
						item: "idProjectTable" //any- I have used the table name
					};
					// define scope
					var oScope = {
						keyCategory: this.oPersonalizationService.constants.keyCategory
							.FIXED_KEY,
						writeFrequency: this.oPersonalizationService.constants
							.writeFrequency.LOW,
						clientStorageAllowed: true
					};

					// Get a Personalizer
					var oPersonalizer = this.oPersonalizationService.getPersonalizer(
						oPersId,
						oScope,
						oComponent
					);
					this.oPersonalizationService
						.getContainer("TablePersonalisation", oScope, oComponent)
						.fail(function () {})
						.done(
							function (oContainer) {
								this.oContainer = oContainer;
								this.oVariantSetAdapter = new sap.ushell.services.Personalization.VariantSetAdapter(
									this.oContainer
								);
								// get variant set which is stored in backend
								this.oVariantSet = this.oVariantSetAdapter.getVariantSet(
									"idProjectTableSet"
								);
								if (!this.oVariantSet) {
									//if not in backend, then create one
									this.oVariantSet = this.oVariantSetAdapter.addVariantSet(
										"idProjectTableSet"
									);
								}
								// array to store the existing variants
								var Variants = [];
								// now get the existing variants from the backend to show as list
								for (var key in this.oVariantSet.getVariantNamesAndKeys()) {
									if (
										this.oVariantSet
										.getVariantNamesAndKeys()
										.hasOwnProperty(key)
									) {
										var oVariantItemObject = {};
										oVariantItemObject.Key = this.oVariantSet.getVariantNamesAndKeys()[
											key
										];
										oVariantItemObject.Name = key;
										Variants.push(oVariantItemObject);
									}
								}
								// create JSON model and attach to the variant management UI control
								this.oVariantModel = new JSONModel();
								this.oVariantModel.oData.VariantList = Variants;

								var oVariantMgmtControl = this.getView().byId(
									"idVariantManagement"
								);

								oVariantMgmtControl.setModel(
									this.oVariantModel,
									"VariantsModel"
								);
								oVariantMgmtControl.setStandardItemText("Standard View");
								//enable save button
								// oVariantMgmtControl.oVariantSave.onAfterRendering = function(){this.setEnabled(true);};
								//oVariantMgmtControl.getModel("save_enablement").setProperty("/enabled", true);
							}.bind(this)
						);

					// create table persco controller
					if(!this.oTablepersoService){
						var oTablePersoController = new TablePersoController({
							table: this.getView().byId("idProjectTable"),
							persoService: oPersonalizer // DemoPersoService
						});
						this.oTablepersoService = oTablePersoController.activate();
					}
				}
			},
			
			onExit: function(){
				 if(this.oTablepersoService && this.oTablepersoService.getTablePersoDialog()){
				 	// This is to avoid the dupliate ID problem on re-entering the app!
				 	// https://launchpad.support.sap.com/#/notes/0002661470
				 	this.oTablepersoService.getTablePersoDialog().destroy();
				 };
			},

			onPersoButtonPressed: function (oEvent) {
				//this.oTablepersoService.openDialog();
				this.oTablepersoService.openDialog({
					ok: "this.onPerscoDonePressed.bind(this)"
				});
				this.oTablepersoService
					.getTablePersoDialog()
					.attachConfirm(this, this.onPerscoDonePressed.bind(this));
			},

			onPerscoDonePressed: function (oEvent) {
				//this.oTablepersoService.savePersonalizations();
			},

			onSaveAsVariant: function (oEvent) {
				// get variant parameters:
				var VariantParam = oEvent.getParameters();

				if (
					this.oVariantSet.getVariantKeyByName(VariantParam.name) !==
					undefined
				) {
					sap.m.MessageBox.warning("Please use another name!");
					return;
				}

				// get columns data:
				var aColumnsData = [];
				this.getView()
					.byId("idProjectTable")
					.getColumns()
					.filter(function (oColumn) {
						return oColumn.getVisible() === true;
					})
					.forEach(function (oColumn, index) {
						var aColumn = {};
						//aColumn.fieldName = oColumn.getProperty("name");
						aColumn.Id = oColumn.getId();
						aColumn.index = index;
						aColumn.Visible = oColumn.getVisible();
						aColumnsData.push(aColumn);
					});

				this.oVariant = this.oVariantSet.addVariant(VariantParam.name);
				if (this.oVariant) {
					this.oVariant.setItemValue(
						"ColumnsVal",
						JSON.stringify(aColumnsData)
					);
					var filterParametersValue = this.getFilterParametersValue();
					this.oVariant.setItemValue(
						"FiltersVal",
						JSON.stringify(filterParametersValue)
					);

					if (VariantParam.def === true) {
						this.oVariantSet.setCurrentVariantKey(
							this.oVariant.getVariantKey()
						);
					}
					this.oContainer.save().done(function () {
						// Tell the user that the personalization data was saved
					});
				}
			},

			onSelectVariant: function (oEvent) {
				var selectedKey = oEvent.getParameters().key;
				/*
			for (var i = 0; i < oEvent.getSource().getVariantItems().length; i++) {
				if (oEvent.getSource().getVariantItems()[i].getProperty("key") === selectedKey) {
					var selectedVariant = oEvent.getSource().getVariantItems()[i].getProperty("text");
					break;
				}
			}
			this._setSelectedVariantToTable(selectedVariant);
			*/
				var oSelectedVariant = oEvent.getSource()._getSelectedItem();
				if (oSelectedVariant) {
					this._setSelectedVariantToTable(oSelectedVariant.getText());
				}
			},

			_setSelectedVariantToTable: function (oSelectedVariant) {
				if (oSelectedVariant) {
					var sVariant = this.oVariantSet.getVariant(
						this.oVariantSet.getVariantKeyByName(oSelectedVariant)
					);
					var aColumns = JSON.parse(sVariant.getItemValue("ColumnsVal"));

					var filterParametersValue = JSON.parse(
						sVariant.getItemValue("FiltersVal")
					);
					if (filterParametersValue) {
						this.setFilterParametersValue(filterParametersValue);
					} else {
						this.resetFilterParametersValue();
					}

					// Hide all columns first
					this.getView()
						.byId("idProjectTable")
						.getColumns()
						.forEach(function (oColumn) {
							oColumn.setVisible(false);
						});
					// re-arrange columns according to the saved variant

					aColumns.forEach(
						function (aColumn) {
							/*
					var aTableColumn = $.grep(this.getView().byId("idProjectTable").getColumns(), function(el, id) {
						//return el.getProperty("name") === aColumn.fieldName;
						return el.getId() === aColumn.Id;
					});
					*/

							var aTableColumn = this.getView()
								.byId("idProjectTable")
								.getColumns()
								.filter(function (oTableColumn) {
									return oTableColumn.getId() === aColumn.Id;
								});

							if (aTableColumn.length > 0) {
								aTableColumn[0].setVisible(aColumn.Visible);
								//						this.getView().byId("idProjectTable").removeColumn(aTableColumn[0]);
								//						this.getView().byId("idProjectTable").insertColumn(aTableColumn[0], aColumn.index);
							}
						}.bind(this)
					);
				}
				// null means the standard variant is selected or the variant which is not available, then show all columns
				else {
					this.getView()
						.byId("idProjectTable")
						.getColumns()
						.forEach(function (oColumn) {
							oColumn.setVisible(true);
						});
				}
			},

			onManageVariant: function (oEvent) {
				var aParameters = oEvent.getParameters();
				// rename variants
				if (aParameters.renamed.length > 0) {
					aParameters.renamed.forEach(
						function (aRenamed) {
							var sVariant = this.oVariantSet.getVariant(aRenamed.key),
								sItemValue = sVariant.getItemValue("ColumnsVal");
							// delete the variant
							this.oVariantSet.delVariant(aRenamed.key);
							// after delete, add a new variant
							var oNewVariant = this.oVariantSet.addVariant(aRenamed.name);
							oNewVariant.setItemValue("ColumnsVal", sItemValue);
						}.bind(this)
					);
				}
				// default variant change
				if (aParameters.def !== "*standard*") {
					this.oVariantSet.setCurrentVariantKey(aParameters.def);
				} else {
					this.oVariantSet.setCurrentVariantKey(null);
				}
				// Delete variants
				if (aParameters.deleted.length > 0) {
					aParameters.deleted.forEach(
						function (aDelete) {
							this.oVariantSet.delVariant(aDelete);
						}.bind(this)
					);
				}
				//  Save the Variant Container
				this.oContainer.save().done(function () {
					// Tell the user that the personalization data was saved
				});
			},

			getFilterParametersValue: function () {
				var filterParametersValue = {
					filterProjectId: this.getView()
						.byId("idFilterProjectId")
						.getValue(),
					filterProjectDescription: this.getView()
						.byId("idFilterProjectDescription")
						.getValue(),
					filterPhaseOutManager: this.getView()
						.byId("idFilterPhaseOutManager")
						.getValue()
				};
				return filterParametersValue;
			},

			setFilterParametersValue: function (filterParametersValue) {
				this.getView()
					.byId("idFilterProjectId")
					.setValue(filterParametersValue.filterProjectId);
				this.getView()
					.byId("idFilterProjectDescription")
					.setValue(filterParametersValue.filterProjectDescription);
				this.getView()
					.byId("idFilterPhaseOutManager")
					.setValue(filterParametersValue.filterPhaseOutManager);
			},

			resetFilterParametersValue: function () {
				this.getView()
					.byId("idFilterProjectId")
					.setValue("");
				this.getView()
					.byId("idFilterProjectDescription")
					.setValue("");
				this.getView()
					.byId("idFilterPhaseOutManager")
					.setValue("");
			},

			/**
			 * Called when the worklist controller is instantiated.
			 * @public
			 */
			onInit: function () {
				try {

					this.oUtils = new Utils();

					//this.initPerso();

					//var sUserId = new sap.ushell.services.UserInfo().getId();

					var oViewModel;
					var iOriginalBusyDelay;
					var oTable = this.getTable();

					// Put down worklist table's original value for busy indicator delay,
					// so it can be restored later on. Busy handling on the table is
					// taken care of by the table itself.
					iOriginalBusyDelay = oTable.getBusyIndicatorDelay();
					// keeps the search state
					this._aTableSearchState = [];

					var aProjectStatusList = Object.keys(this.Constants.ProjectStates);
					var aProjectStatesFilterList = [{
						Key: "All",
						DisplayName: "projectStatusAll"
					}];
					aProjectStatusList.forEach(function (item) {
						if (item !== "TO_BE_PUBLISHED") {
							aProjectStatesFilterList.push(this.Constants.ProjectStates[item]);
						}
					}.bind(this));

					// Model used to manipulate control states
					oViewModel = new JSONModel({
						worklistTableTitle: this.getResourceBundle().getText(
							"worklistTableTitle"
						),
						saveAsTileTitle: this.getResourceBundle().getText(
							"saveAsTileTitle",
							this.getResourceBundle().getText("worklistViewTitle")
						),
						shareOnJamTitle: this.getResourceBundle().getText("worklistTitle"),
						shareSendEmailSubject: this.getResourceBundle().getText(
							"shareSendEmailWorklistSubject"
						),
						shareSendEmailMessage: this.getResourceBundle().getText(
							"shareSendEmailWorklistMessage", [location.href]
						),
						tableNoDataText: this.getResourceBundle().getText(
							"tableProjectNoDataText"
						),
						tableBusyDelay: 0,
						FilterText: "Filtered by None",
						aProjectStatesFilterList: aProjectStatesFilterList,
						bNewProjectDialogFull: false,
						sNewProjectTitle: this.getResourceBundle().getText(
							"newProjectTitle"
						),
						sNewProjectDialogWidth: "60%",
						sNewProjectDialogHeight: "30%"
					});
					this.setModel(oViewModel, "worklistView");

					var oComponent = this.getOwnerComponent();
					var sApplicationVersion = oComponent
						.getMetadata()
						.getManifestEntry("sap.app").applicationVersion.version;
					oViewModel.setProperty("/sApplicationVersion", sApplicationVersion);

					// Make sure, busy indication is showing immediately so there is no
					// break after the busy indication for loading the view's meta data is
					// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
					oTable.attachEventOnce("updateFinished", function () {
						// Restore original busy indicator delay for worklist's table
						oViewModel.setProperty("/tableBusyDelay", iOriginalBusyDelay);
					});

					// define filter

					this.aKeys = [
						"ProjectKey",
						"ProjectDescription",
						"PhaseOutManagerDisplayName",
						"PhaseOutDate",
						"ProjectStatus"
					];
					this.filterId = this.getView().byId("idFilterProjectId");
					this.filterDesript = this.getView().byId("idFilterProjectDescription");
					this.filterDate = this.getView().byId("idFilterPhaseOutDate");
					this.filterManager = this.getView().byId("idFilterPhaseOutManager");
					this.filterDocStatus = this.getView().byId("idFilterProjectStatus");

					// PLM2-1522:
					// ==========
					// Load the list of Projects into a local JSON model before the view shows up:
					// This enables filtering projects in the frontend code w.r.t. ECNs or Material Descriptions

					this.oProjectsModel = new JSONModel();
					this.getView().setModel(this.oProjectsModel, "ProjectsModel");

					// Add new events to handle data initialization or clean up
					// on entering or leaving this page
					/**
					 * Take care:
					 * ==========
					 * An expand like 'ProjectItems/MasterData' could be time-consuming!
					 * Don't use it here at the beginning if not really necessary!
					 *
					 */
					 
					this.getView().addEventDelegate({
							onBeforeShow: function (oEvent) {
							},
							onAfterShow: function (oEvent) {
								// Set the default Project Status filter to "Active"
								// Set the default PhaseOutDate filter to today
								this.onPressReset();

							}.bind(this),
							onBeforeHide: function (oEvent) {
								//debugger; // disappearance 1
							},
							onAfterHide: function (oEvent) {
								//debugger; // disappearance 2
							}
						},
						this
					);

				} catch (oError) {
					jQuery.sap.log.error(oError);
				}

			},

			getInitialFilterDate: function () {
				// By default set the initial filter for the phase out date to one year after today.
				// Reason: According to the backend developer, the backend requires this type to be set for the filter to work properly!
				var d1 = new Date();
				d1.setFullYear(d1.getFullYear() + 1);
				return d1;
			},

			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * Triggered by the table's 'updateFinished' event: after new table
			 * data is available, this handler method updates the table counter.
			 * This should only happen if the update was successful, which is
			 * why this handler is attached to 'updateFinished' and not to the
			 * table's list binding's 'dataReceived' method.
			 * @param {sap.ui.base.Event} oEvent the update finished event
			 * @public
			 */
			onUpdateFinished: function (oEvent) {
				
				//var nProjectsDisplayed = oEvent.getParameter("actual");
				var nProjectsInTotal = this.getTable().getBinding("items").getLength();
				
				//var sTitle = this.getResourceBundle().getText("worklistTableTitle");
				var sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [nProjectsInTotal]);
				this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);

				// update the worklist's object counter after the table update
				//var oTable = oEvent.getSource();
				//oTable.setBusy(true);
				//var sTitle = this.getResourceBundle().getText("worklistTableTitle");

				// PLM2-1522:
				// ==========
				/*
			var aProjects = oTable.getBinding("items").getModel().getProperty("/aProjects");
			if(aProjects && aProjects.length > 0){
    			// only update the counter if the length is final and
    			// the table is not empty
    			if (oTable.getBinding("items").isLengthFinal()) {
    			    var nActiveProjects = oTable.getItems().filter(function(item){
    			        return item.getProperty("visible") === true;
    			    }).length;
    				sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [nActiveProjects]);
    			}
    			//var aExpandedProjects = this.expandProjectsListToExcelTable(aProjects);
    			//this.oProjectsModel.setProperty("/aExpandedProjects",aExpandedProjects);
    			oTable.setBusy(false);
			}
			this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
            */

				/*
            // Code from before PLM2-1522:
            // ===========================
			var oTable = oEvent.getSource();
			var iTotalItems = oEvent.getParameter("total");
			// only update the counter if the length is final and the table is not empty
			if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
			} else {
				sTitle = this.getResourceBundle().getText("worklistTableTitle");
			}
			this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
			*/


				// Use jQuery to rename the "Go" button of the filter bar!
	
				// Search Button name
				var sGoButtonBdiName = this.getView().createId(
					"idFilterBarWorklist-btnGo-BDI-content"
				);
				$("bdi[id$='" + sGoButtonBdiName + "']").text(
					this.getResourceBundle().getText("lblButtonGo")
				);
	
				// Search Button tooltip
				var sSearchButton = this.getView().createId(
					"idFilterBarWorklist-btnGo"
				);
				$("button[id$='" + sSearchButton + "']").prop(
					"title",
					this.getResourceBundle().getText("lblButtonGo")
				);
	
				// Reset Button name
				var sResetButtonBdiName = this.getView().createId(
					"idFilterBarWorklist-btnClear-BDI-content"
				);
				$("bdi[id$='" + sResetButtonBdiName + "']").text(
					this.getResourceBundle().getText("lblButtonReset")
				);
	
				// Reset Button tooltip
				var sResetButton = this.getView().createId(
					"idFilterBarWorklist-btnClear"
				);
				$("button[id$='" + sResetButton + "']").prop(
					"title",
					this.getResourceBundle().getText("lblButtonReset")
				);
				
			},

			/**
			 * Event handler when a table item gets pressed
			 * @param {sap.ui.base.Event} oEvent the table selectionChange event
			 * @public
			 */
			onSelectProject: function (oEvent) {
				// The source is the list item that got pressed
				this._showProject(oEvent.getSource());
			},

			/**
			 * Event handler when the share in JAM button has been clicked
			 * @public
			 */
			onShareInJamPress: function () {
				var oViewModel = this.getModel("worklistView"),
					oShareDialog = sap.ui.getCore().createComponent({
						name: "sap.collaboration.components.fiori.sharing.dialog",
						settings: {
							object: {
								id: location.href,
								share: oViewModel.getProperty("/shareOnJamTitle")
							}
						}
					});
				oShareDialog.open();
			},

			/**
			 * Event handler for refresh event. Keeps filter, sort
			 * and group settings and refreshes the list binding.
			 * @public
			 */
			onRefresh: function () {
				var oTable = this.byId("table");
				oTable.getBinding("items").refresh();
			},

			/* =========================================================== */
			/* internal methods                                            */
			/* =========================================================== */

			/**
			 * Shows the selected item on the object page
			 * On phones a additional history entry is created
			 * @param {sap.m.ObjectListItem} oItem selected Item
			 * @private
			 */
			_showProject: function (oItem) {
				//var oRowBindingContext = oItem.getBindingContext("serviceModel");
				var oRowBindingContext = oItem.getBindingContext("serviceModel");
				if (oRowBindingContext.getProperty("ProjectStatus") !== "Deleted") {
					this.getRouter().navTo("projectObject", {
						objectId: oRowBindingContext.getProperty("ProjectKey")
					});
				}
			},

			/************************************************************************************
			 ************************************************************************************
			 *                      flexible search for project work list
			 * **********************************************************************************
			 ************************************************************************************/

			onPressSearch: function () {
				var aCurrentFilterValues = [];
				aCurrentFilterValues.push(this.getSelectedItemText(this.filterId));
				aCurrentFilterValues.push(
					this.getSelectedItemText(this.filterDesript)
				);
				aCurrentFilterValues.push(
					this.getSelectedItemText(this.filterManager)
				);
				aCurrentFilterValues.push(this.getSelectedItemText(this.filterDate));
				aCurrentFilterValues.push(
					this.getSelectedItemText(this.filterDocStatus)
				);
				this.filterTable(aCurrentFilterValues);
			},

			onPressReset: function () {
				this.resetSelectedItemText(this.filterId);
				this.resetSelectedItemText(this.filterDesript);
				this.resetSelectedItemText(this.filterManager);
				this.resetSelectedItemText(this.filterDate);
				this.resetSelectedItemText(this.filterDocStatus);

				var aCurrentFilterValues = []; // Take care: The ordering of elements in this array is very important!
				aCurrentFilterValues.push("");
				aCurrentFilterValues.push("");
				aCurrentFilterValues.push("");
				aCurrentFilterValues.push(this.getInitialFilterDate());
				aCurrentFilterValues.push("Active");
				
				// Apply the default Projects Filter
				this.filterTable(aCurrentFilterValues);
			},

			// PLM2-1522:
			// ==========
			// Quick Filter for the projects list

			_filter: function () {
				var oFilter = null;

				if (this._oGlobalFilter) {
					oFilter = this._oGlobalFilter;
				}

				var oTable = this.getView().byId("idProjectTable");
				oTable.getBinding("items").filter(oFilter, "Application");
			},

			filterGlobally: function (oEvent) {
				var sQuery = oEvent.getParameter("query");
				this._oGlobalFilter = null;

				if (sQuery) {
					this._oGlobalFilter = new Filter(
						[
							new Filter("ProjectKey", FilterOperator.Contains, sQuery),
							new Filter(
								"ProjectDescription",
								FilterOperator.Contains,
								sQuery
							),
							new Filter(
								"PhaseOutManagerDisplayName",
								FilterOperator.Contains,
								sQuery
							),
							new Filter("ProjectStatus", FilterOperator.Contains, sQuery),
							new Filter({
								path: "Ecns",
								test: function (oValue) {
									var bFound = false;
									oValue.results.some(function (item) {
										if (
											item.EcnKey.toUpperCase().indexOf(
												sQuery.toUpperCase()
											) > -1
										) {
											bFound = true;
											return true;
										}
										if (
											item.EcnDescription.toUpperCase().indexOf(
												sQuery.toUpperCase()
											) > -1
										) {
											bFound = true;
											return true;
										}
										return false;
									});
									return bFound;
								}
							}),
							new Filter({
								path: "ProjectItems",
								test: function (oValue) {
									var bFound = false;
									oValue.results.some(function (item) {
										if (
											item.MaterialKey.toUpperCase().indexOf(
												sQuery.toUpperCase()
											) > -1
										) {
											bFound = true;
											return true;
										}
										if (
											item.MaterialDescription.toUpperCase().indexOf(
												sQuery.toUpperCase()
											) > -1
										) {
											bFound = true;
											return true;
										}
										if (
											item.PlantKey.toUpperCase().indexOf(
												sQuery.toUpperCase()
											) > -1
										) {
											bFound = true;
											return true;
										}
										return false;
									});
									return bFound;
								}
							})
						],
						false
					);
				}

				this._filter();
			},

			getSelectedItemText: function (oSelect) {
				if (oSelect.hasStyleClass("myStringField")) {
					return oSelect.getSelectedItem() ? oSelect.getSelectedItem().getText() : oSelect.getValue();
				} else if (oSelect.hasStyleClass("mySelectField")) {
					return oSelect.getSelectedItem() ? oSelect.getSelectedItem().getKey() : "Active";
				} else {
					return oSelect.getDateValue() || "";
				}
			},
			filterTable: function (aCurrentFilterValues) {
				var aSearchValues = aCurrentFilterValues.filter(function (item) {
					return item && item !== "";
				});
				// Check Emptiness first
				if (aSearchValues.length > 0) {
					// Case 1:
					this.getTableItems().filter(this.getFilters(aCurrentFilterValues));
				} else {
					// Case 2:
					this.getTableItems().filter();
				}

				//this.updateFilterCriterias(this.getFilterCriteria(aCurrentFilterValues));
			},
			getTable: function () {
				return this.getView().byId("idProjectTable");
			},
			getTableItems: function () {
				return this.getTable().getBinding("items");
			},

			getFilters: function (aCurrentFilterValues) {

				if (!aCurrentFilterValues || aCurrentFilterValues.length < 1) {
					return [];
				}

				this.aFilters = [];

				this.aFilters = this.aKeys.map(
					function (sCriteria, i) {
						var oFilter = null;
						if ( sCriteria === "PhaseOutDate" && aCurrentFilterValues[i] !== "" ) {
							var oDate = aCurrentFilterValues[i];
							if(oDate){
								// set hours to the end of the day
								oDate.setHours(23);
								// set minutes to the end of the day
								oDate.setMinutes(59);
								// set minutes to the end of the day
								oDate.setSeconds(59);
								oFilter = new Filter(sCriteria, "LE", new Date(oDate));
							}
						} else if ( sCriteria === "ProjectStatus" && aCurrentFilterValues[i] !== "" ) {
							if (aCurrentFilterValues[i] !== "All") {
								if (aCurrentFilterValues[i] !== "Active") {
									oFilter = new Filter(
										sCriteria,
										FilterOperator.EQ,
										aCurrentFilterValues[i]
									);
								} else {
									// "Active"
									var aFilterArray = [];
									aFilterArray.push(new Filter("ProjectStatus", "EQ", this.Constants.ProjectStatus.DRAFT));
									aFilterArray.push(new Filter("ProjectStatus", "EQ", this.Constants.ProjectStatus.PUBLISHED));
									aFilterArray.push(new Filter("ProjectStatus", "EQ", this.Constants.ProjectStatus.IN_PROCESS));
									aFilterArray.push(new Filter("ProjectStatus", "EQ", this.Constants.ProjectStatus.STOCK_CLEANSING));
									oFilter = new Filter(aFilterArray, false);
								}
							}
						} else {
							oFilter = new Filter(
								sCriteria,
								FilterOperator.Contains,
								aCurrentFilterValues[i]
							);
						}
						return oFilter;
					
					}.bind(this)
				);

				return this.aFilters.filter(function (item) {
					return item !== null;
				});
			},

			getFilterCriteria: function (aCurrentFilterValues) {
				return this.aKeys.filter(function (el, i) {
					if (aCurrentFilterValues[i] !== "") {
						return el;
					}
				});
			},

			onReset: function () {
				this.resetSelectedItemText(this.filterId);
				this.resetSelectedItemText(this.filterDesript);
				this.resetSelectedItemText(this.filterManager);
				this.resetSelectedItemText(this.filterDate);
				this.resetSelectedItemText(this.filterDocStatus);
			},

			resetSelectedItemText: function (oSelect) {
				if (oSelect.hasStyleClass("myStringField")) {
					return oSelect.getSelectedItem() ? oSelect.setSelectedKey("Active") : oSelect.setValue(null);
				} else if (oSelect.hasStyleClass("mySelectField")) {
					return oSelect.setSelectedKey("Active");
				} else {
					return oSelect.setDateValue(this.getInitialFilterDate());
				}
			},

			/*************************************************************************************
			 * ***********************************************************************************
			 *         change project doc status
			 * ***********************************************************************************
			 * **********************************************************************************/

			onPressPublished: function (oEvent) {
				this._setProjectStatus(oEvent, this.Constants.ProjectStatus.PUBLISHED);
			},

			onPressToBePublished: function (oEvent) {
				this._setProjectStatus(
					oEvent,
					this.Constants.ProjectStatus.TO_BE_PUBLISHED
				);
			},

			onPressStop: function (oEvent) {
				this._setProjectStatus(oEvent, this.Constants.ProjectStatus.DRAFT);
			},

			onPressDelete: function (oEvent) {
				this._setProjectStatus(oEvent, this.Constants.ProjectStatus.DELETED);
			},

			onPressUndelete: function (oEvent) {
				this._setProjectStatus(oEvent, this.Constants.ProjectStatus.DRAFT);
			},

			_setProjectStatus: function (oEvent, sNewStatus) {
				var oDataModel = this.getModel("serviceModel");

				var oRowBindingContext = oEvent.getSource().getBindingContext("serviceModel");
				var sProjectKey = oRowBindingContext.getProperty("ProjectKey");
				var sPath = "/" + oDataModel.createKey("ProjectEntitySet", {
					ProjectKey: sProjectKey
				});

				oDataModel.setProperty(sPath + "/" + "ProjectStatus", sNewStatus);

				// show busy indicator
				var oTable = this.getView().byId("idProjectTable");
				oTable.setBusy(true);

				// This submitChanges() requires batch mode to work properly.
				oDataModel.setUseBatch(true);

				// set project status to "Closed"
				oDataModel.submitChanges({
					groupId: "ProjectEntityGroupId",
					success: function (data, response) {

						oDataModel.setUseBatch(sap.ui.getCore().getModel("settingsModel").getProperty("/BatchMode"));

						// busy indicator off
						oTable.setBusy(false);

						//output message taost
						var sProjectID = oDataModel.getProperty(sPath + "/" + "ProjectKey");
						var sProjectStatus = oDataModel.getProperty(sPath + "/" + "ProjectStatus");
						var sOutputText = this.getResourceBundle().getText(
							"projectStatusChangedTo", [sProjectID, sProjectStatus]
						);
						sap.m.MessageToast.show(sOutputText);

						// Show proper status icon after change is submitted.
						oDataModel.refresh();

					}.bind(this),
					error: function (data) {
						oTable.setBusy(false);
						sap.m.MessageToast.show("Failed to change project status");
					}
				});
			},

			/****************************************************************************************************
			 *
			 *         create new project object
			 *
			 * **************************************************************************************************/

			onCreateNewProject: function () {
				if (!this.oDialog) {
					this.oDialog = sap.ui.xmlfragment(
						this.getView().getId(),
						"com.vaillant.plm.fiori.projectcockpit.view.fragments.DialogNewProject",
						this
					);
					// connect dialog to the root view of this component (models, lifecycle)
					this.getView().addDependent(this.oDialog);
				}
				// initial parameters for dialog
				this.fnSetNewProjectParameters(false);
				
				// SPRINT-2: SPARE Flag Handling
				this.getView().getModel("UserSelectionModel").setProperty("/bEditSpareCheckbox", true);

				this.oDialog.open();
			},

			/**************************************************************************************************************************************
			 *			update parameters of new project dialog
			 * ***********************************************************************************************************************************/

			fnSetNewProjectParameters: function (bSavedProject) {
				var oWorklistModel = this.getModel("worklistView");
				var oAppModel = this.getModel("appView");
				var sDialogWidth;
				var sDialogHeight;
				var sTitle;

				if (bSavedProject) {
					var sProjectKey = this.oDialog
						.getBindingContext("serviceModel")
						.getObject().ProjectKey;
					sTitle = this.getResourceBundle().getText(
						"newProjectTitleWithNumber", [sProjectKey]
					);
					sDialogWidth = sDialogHeight = "60%";
				} else {
					sTitle = this.getResourceBundle().getText("newProjectTitle");
					sDialogWidth = "60%";
					sDialogHeight = "30%";
					var oDataModel = this.getModel("serviceModel");

					// clear value of selection field
					var oSelect = this.byId("idPhaseOutManagerDisplayName");
					if (oSelect) {
						oSelect.setValue("");
					}

					// clear ecn table
					//this.byId("idResetAddEcnsTable").firePress();

					// This is not a good solution because it would add the press event handler too oftern and it would get executed many times!
					// attach create project entity function to submit button
					// var oSubmitBtn = this.byId("idNewProjectSubmitBtn");
					// oSubmitBtn.attachEventOnce("press", null, this.fnPressAddNewEcnInNewProjectDialog, this);

					// create new entity
					this.oContext = oDataModel.createEntry("/ProjectEntitySet", {
						groupId: "ProjectEntityGroupId"
					});
					this.oDialog.setBindingContext(this.oContext, "serviceModel");
				}

				// update buttons visible for create project
				oAppModel.setProperty(
					"/bNewProjectHeaderDataIsFilled",
					bSavedProject
				);

				// update buttons visible and ECN area /bNewProjectDialogFull
				oWorklistModel.setProperty("/bNewProjectDialogFull", bSavedProject);

				// update title /sNewProjectTitle
				oWorklistModel.setProperty("/sNewProjectTitle", sTitle);

				// update dialog width and heihgt
				oWorklistModel.setProperty("/sNewProjectDialogWidth", sDialogWidth);
				oWorklistModel.setProperty("/sNewProjectDialogHeight", sDialogHeight);
			},

			/**************************************************************************************************************************************
			 *	press event-handler
			 * ***********************************************************************************************************************************/
			fnAddEcnOrSubmitNewProject: function (oEvent) {
				
				// SPRINT-2: Set initial Project Category to M if user does not choose "Spare"
				var oContext = oEvent.getSource().getBindingContext("serviceModel");
				var sProjectCategoryPath = oContext.sPath + "/ProjectCategory";
				var sProjectCategory = oContext.getProperty(sProjectCategoryPath);
				if(!sProjectCategory || sProjectCategory===""){
					var oDataModel = oContext.getModel();
					oDataModel.setProperty(sProjectCategoryPath, "M"); // Default is M
				}
				
				var oWorklistModel = this.getModel("worklistView");
				var bSavedProject = oWorklistModel.getProperty(
					"/bNewProjectDialogFull"
				);
				if (bSavedProject) {
					this.fnPressSubmitProjectData(oEvent);
				} else {
					this.fnPressAddNewEcnInNewProjectDialog(oEvent);
				}
			},

			/**************************************************************************************************************************************
			 *			update project data in dialog
			 * ***********************************************************************************************************************************/

			/**
			 * This event handler creates the new Project entity "on the fly".
			 * A new project key is created by the backend.
			 * And extends the New Project Dialog by the "Add Ecn" Section:
			 *
			 * */
			fnPressAddNewEcnInNewProjectDialog: function (oEvent) {
				var oDataModel = this.getModel("serviceModel");

				// busy indicator on dialog
				this.oDialog.setBusy(true);

				// This submitChanges() for the creation of a new project requires batch mode to work properly.
				oDataModel.setUseBatch(true);

				var sPendingChanges = oDataModel.getPendingChanges();
				jQuery.sap.log.info("sPendingChanges = ", sPendingChanges);

				// create new ProjectEntity
				oDataModel.submitChanges({

					groupId: "ProjectEntityGroupId",

					success: function (data, response) {
						// switch off batch mode for debugging
						oDataModel.setUseBatch(
							sap.ui
							.getCore()
							.getModel("settingsModel")
							.getProperty("/BatchMode")
						);

						// busy indicator off
						this.oDialog.setBusy(false);

						this.fnSetNewProjectParameters(true); // opens the lower part of the project creation dialog

						//output message
						var sProjectKey =
							data.__batchResponses["0"].__changeResponses["0"].data
							.ProjectKey;
						var sOutputText = this.getResourceBundle().getText(
							"projectIsCreated", [sProjectKey]
						);
						sap.m.MessageToast.show(sOutputText);
					}.bind(this),

					error: function (data) {
						sap.m.MessageToast.show("faild to create a new project");
					}
				});
			},

			/**
			 * This event handler adds the newly selected list of ECNs to the newly created project.
			 * It retrieves the correct project key from the button's binding context.
			 */

			fnPressSubmitProjectData: function (oEvent) {

				var d1 = new Date();
				var oButton = oEvent.getSource();
				var oBinding = oButton.getBindingContext("serviceModel");
				var sProjectKey = oBinding.getObject().ProjectKey;
				var sCurrentProjectPath = "/ProjectEntitySet('" + sProjectKey + "')";
				
				var aSelectedEcns = this.getModel("UserSelectionModel").getProperty("/aSelectedEcns");

				// Remove empty or undefined ECNs before submitting:
				aSelectedEcns = aSelectedEcns.filter(function(item){
					return (item.EcnKey && item.EcnKey!=="" );
				});
				
				var oDataModel = this.getModel("serviceModel");

				var sFunctionImportPath = "/selectEcn";
				var oTargetParameter = {
					"projectKey": sProjectKey
				};
				this.pProcessEntitiesAssignment(oDataModel, sFunctionImportPath, aSelectedEcns, oTargetParameter)
				.then(function () {
					var d2 = new Date();
					var d3 = d2 - d1;
					jQuery.sap.log.info(
						"onSubmitEcnDialog: Time elapsed in ms = ",
						d3
					);
					this.oDialog.close();
					//update data model
					oDataModel.refresh();
				}.bind(this))
				.catch(function (oError) {
					jQuery.sap.log.error("onSubmitEcnDialog failed: ", oError);
					sap.m.MessageToast.show("onSubmitEcnDialog failed!");
				}.bind(this));

				return;
			},

			fnClearDialogData: function (oEvent) {
				var oWorklistModel = this.getModel("worklistView");
				var bSavedProject = oWorklistModel.getProperty(
					"/bNewProjectDialogFull"
				);

				if (bSavedProject) {
					var oResetBtn = this.byId(
						$("button[id$='idResetAddEcnsTable']")[0].id
					);
					//var sAggrName = oEvent.getParameter("origin").sParentAggregationName;

					oResetBtn.firePress({
						sDeleteMode: "clearTableOnly" //sAggrName === "endButton" ? "noPopup" : "clearTableOnly"
					});
				}
			},

			/* *****************************************************************************************************************************
			 *		cancel and delete project actions
			 * ***************************************************************************************************************************/

			// TODO: Remove this!
			onCnacelCreateNewProject: function () {
				var oDataModel = this.getModel("serviceModel");
				oDataModel.deleteCreatedEntry(this.oContext);
				this.oDialog.close();
			},

			onDeleteNewProject: function (oEvent) {
				var oContext = oEvent.getSource().getBindingContext("serviceModel");
				var sProjectKey = oContext.getObject()["ProjectKey"];
				var sMsgText2 = this.getResourceBundle().getText(
					"projectCreationDiscardMsg", [sProjectKey]
				);

				this.oUtils.fnOneActionDiscard.apply(this, [
					sMsgText2,
					this.fnDeleteEntity,
					oContext
				]); //
			},

			fnDeleteEntity: function (oContext) {
				var oDataModel = this.getModel("serviceModel");
				var oBundle = this.getResourceBundle();
				var MsgText;
				oDataModel.remove(oContext.getPath(), {
					success: function () {
						MsgText = oBundle.getText("objectIsDeleted");
						sap.m.MessageToast.show(MsgText);
						//close dialog
						this.oDialog.close();
						this.oDialog.unbindElement();
					}.bind(this),
					error: function () {
						MsgText = oBundle.getText("objectIsNotDeleted");
						sap.m.MessageToast.show(MsgText);
					}
				});
			},

			onCancelNewProject: function (oEvent) {
				var oWorklistModel = this.getModel("worklistView");
				var bSavedProject = oWorklistModel.getProperty(
					"/bNewProjectDialogFull"
				);
				var oDataModel = this.getModel("serviceModel");

				if (bSavedProject) {
					var oContext = oEvent
						.getSource()
						.getBindingContext("serviceModel");
					var sProjectKey = oContext.getObject()["ProjectKey"];
					var sMsgText2 = this.getResourceBundle().getText(
						"projectCreationDiscardMsg", [sProjectKey]
					);

					this.oUtils.fnOneActionDiscard.apply(this, [
						sMsgText2,
						this.fnDeleteEntity,
						oContext
					]); //
				} else {
					oDataModel.deleteCreatedEntry(this.oContext);
					this.oDialog.close();
				}
			}
		});
	}
);