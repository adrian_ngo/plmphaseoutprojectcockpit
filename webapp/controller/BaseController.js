sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/vaillant/plm/fiori/projectcockpit/common/js/Constants",
	"com/vaillant/plm/fiori/projectcockpit/model/formatter"
], function(Controller,Constants,formatter) {
	"use strict";

	return Controller.extend("com.vaillant.plm.fiori.projectcockpit.controller.BaseController", {

		// Provide these useful Objects to all inherited controllers:
		Constants: Constants,
		formatter: formatter,

		oConfirmationDialog: 0,

		pSubmitChanges: function(oDataModel, sEntity, sGroupId){
			return new Promise( function(resolve,reject){
				var sPendingChanges = JSON.stringify( oDataModel.getPendingChanges() );
				if (sPendingChanges.indexOf(sEntity) > -1) {
					oDataModel.setUseBatch(true);
					oDataModel.submitChanges({
						groupId: sGroupId,
						success: function(oData) {
							oDataModel.setUseBatch( sap.ui.getCore().getModel("settingsModel").getProperty("BatchMode") );
							resolve(oData);
						}.bind(this),
						error: function(oError) {
							reject(oError);
							jQuery.sap.log.error("Error in pSubmitChanges: ", oError);
						}
					});
				} else {
					resolve({"Info": "Pending Changes does not contain " + sEntity});
				}
			});
		},

		pCreateLinkToNavigationTarget: function (oDataModel, sCurrentPath, sNavigationTarget, oPayload) {
			return new Promise(function (resolve, reject) {
				oDataModel.setUseBatch(true);
				oDataModel.create(sCurrentPath + "/$links/" + sNavigationTarget, oPayload, {
					success: function (oData) {
						console.log(oData);
						var batchMode = sap.ui.getCore().getModel("settingsModel").getProperty("/BatchMode");
						oDataModel.setUseBatch(batchMode);
						oDataModel.refresh();
						resolve(oData);
					}.bind(this),
					error: function (oError) {
						console.error(oError);
						reject(oError);
					}.bind(this),
					refreshAfterChange: true
				});
			}.bind(this));
		},


		pCallFunctionImport: function (oDataModel, sFunctionImportPath, oParameters) {
			return new Promise(function (resolve, reject) {
				oDataModel.setUseBatch(true);
				oDataModel.callFunction(sFunctionImportPath, {
					urlParameters: oParameters,
					success: function (oData) {
						oDataModel.setUseBatch(sap.ui.getCore().getModel("settingsModel").getProperty("/BatchMode"));
						//update data model
						oDataModel.refresh();
						resolve(oData);
					},
					error: function (oError) {
						oDataModel.setUseBatch(sap.ui.getCore().getModel("settingsModel").getProperty("/BatchMode"));
						reject(oError);
					}.bind(this)
				});
			}.bind(this));
		},


		pProcessAddingPlantsToProject: function (oDataModel, sProjectKey, aSelectedPlants) {
			return new Promise(function(resolver,rejecter){
				var nLength = aSelectedPlants.length;
				return aSelectedPlants.reduce(function (promise, oSelectedPlant, index) {
					return promise
						.then(function (oPreviousResult) {
							jQuery.sap.log.info("Add Plant ", oSelectedPlant.PlantKey);
							// assign selected Ecn to the current project
							var oParameters = {
								"projectKey": sProjectKey,
								"plantKey": oSelectedPlant.PlantKey
							};
							return this.pCallFunctionImport(oDataModel, "/selectPlant", oParameters)
								.then(function (oResult) {
									jQuery.sap.log.info("Added Plant ", oSelectedPlant.PlantKey);
									if (index === nLength - 1) {
										resolver(oResult); // successful exit
									}
								}.bind(this));
						}.bind(this))
						.catch(function (oError) {
							jQuery.sap.log.error(oError);
							rejecter(oError); // failed exit
						}.bind(this));
				}.bind(this), Promise.resolve({}));
			}.bind(this));
		},


		pProcessEntitiesAssignment: function (oDataModel, sFunctionImportPath, aSelectedEntities, oTargetParameter) {
			return new Promise(function(resolver,rejecter){
				var nLength = aSelectedEntities.length;
				if(nLength < 1){
					resolver();
				}
				return aSelectedEntities.reduce(function (promise, oSelectedEntity, index) {
					return promise
						.then(function (oPreviousResult) {
							// assign selected Item to current project or project + ecn
							var oParameters = {};
							if(sFunctionImportPath === "/selectPlant" || sFunctionImportPath === "/deselectPlant"){
								oParameters = {
									"projectKey": oTargetParameter.projectKey,
									"plantKey"  : oSelectedEntity.PlantKey
								};
							} else if (sFunctionImportPath === "/selectEcn" || sFunctionImportPath === "/deselectEcn"){
								oParameters = {
									"projectKey": oTargetParameter.projectKey,
									"ecnKey"    : oSelectedEntity.EcnKey
								};
							} else if (sFunctionImportPath === "/selectForPhaseout"){
								oParameters = {
									"materialKey": oSelectedEntity.MaterialKey,
									"plantKey"   : oSelectedEntity.PlantKey,
									"projectKey" : oTargetParameter.projectKey,
									"ecnKey"     : oSelectedEntity.EcnKey
								};
							} else if (sFunctionImportPath === "/deselectForPhaseout"){
								oParameters = {
									"materialKey": oSelectedEntity.MaterialKey,
									"plantKey"   : oSelectedEntity.PlantKey
								};
							}
							return this.pCallFunctionImport(oDataModel, sFunctionImportPath, oParameters)
								.then(function (oResult) {
									if (index === nLength - 1) {
										resolver(oResult); // successful exit after the last item is done
									}
								}.bind(this));
						}.bind(this))
						.catch(function (oError) {
							rejecter(oError); // failed exit
						}.bind(this));
				}.bind(this), Promise.resolve({}));
			}.bind(this));
		},


		pProcessAddingMaterialPlantItemForPhaseOut: function (oDataModel, sProjectKey, sEcnKey, aSelectedMaterialPlantItems) {
			return new Promise(function(resolver,rejecter){
				var nLength = aSelectedMaterialPlantItems.length;
				return aSelectedMaterialPlantItems.reduce(function (promise, oItem, index) {
					return promise
						.then(function (oPreviousResult) {
							jQuery.sap.log.info("Add item MaterialKey=" + oItem.MaterialKey + " PlantKey=" + oItem.PlantKey );
							// assign selected Ecn to the current project
							var oParameters = {
								"materialKey": oItem.MaterialKey,
								"plantKey": oItem.PlantKey,
								"projectKey": sProjectKey,
								"ecnKey": sEcnKey
							};
							return this.pCallFunctionImport(oDataModel, "/selectForPhaseout", oParameters)
								.then(function (oResult) {
									jQuery.sap.log.info("Added item MaterialKey=" + oItem.MaterialKey + " PlantKey=" + oItem.PlantKey );
									if (index === nLength - 1) {
										resolver(oResult); // successful exit after the last item is done
									}
								}.bind(this));
						}.bind(this))
						.catch(function (oError) {
							jQuery.sap.log.error(oError);
							rejecter(oError); // failed exit
						}.bind(this));
				}.bind(this), Promise.resolve({}));
			}.bind(this));
		},


		confirmDiscardAction: function(sMsgText, aActions, oParameters) {
			var that = this;
			if(!this.oConfirmationDialog || this.oConfirmationDialog.bIsDestroyed){
    			this.oConfirmationDialog = new sap.m.Dialog({
				    id: that.getView().createId("idConfirmationDialog"),
    				title: "Confirm",
    				type: "Message",
    				content: new sap.m.Text({
    					text: sMsgText
    				}),
    				beginButton: new sap.m.Button({
    				    id: that.getView().createId("idConfirmYes"),
    					text: "Yes",
    					press: function() {
    						for (var index in aActions) {
    							//aActions[index]();
    							aActions[index].apply(that, [oParameters]);

    						}
    						that.oConfirmationDialog.close();
    					}
    				}),
    				endButton: new sap.m.Button({
    				    id: that.getView().createId("idConfirmNo"),
    					text: "No",
    					press: function() {
    						that.oConfirmationDialog.close();
    					}
    				}),
    				afterClose: function() {
    					that.oConfirmationDialog.destroy();
    				}
    			});
			}
			this.oConfirmationDialog.open();
		},


		/**
		 * Convenience method for accessing the router.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},


		goBackToWorklist: function(){
            this.getRouter().navTo("projectWorklist", {});
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @public
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function(oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Getter for the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function() {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Event handler when the share by E-Mail button has been clicked
		 * @public
		 */
		onShareEmailPress: function() {
			var oViewModel = (this.getModel("objectView") || this.getModel("worklistView"));
			sap.m.URLHelper.triggerEmail(
				null,
				oViewModel.getProperty("/shareSendEmailSubject"),
				oViewModel.getProperty("/shareSendEmailMessage")
			);
		},

		/*******************************************************************************************************************/
		/***************                return array with paths elements from model                           **************/
		/*******************************************************************************************************************/

		onGetPathElements: function(that) {

			var pagecreateModel = that.getModel("pagecreateModel");

			var oProject = pagecreateModel.getProperty("/projectPath"),
				oEcn = pagecreateModel.getProperty("/ecnPath"),
				oMaterial = pagecreateModel.getProperty("/materialtPath"),
				pathArr = [];

			if (oProject) {
				pathArr.push(oProject);
			}
			if (oEcn) {
				pathArr.push(oEcn);
			}
			if (oMaterial) {
				pathArr.push(oMaterial);
			}

			return pathArr;
		},

		/*******************************************************************************************************************/
		/***************                                   update Breadcrumbs                                 **************/
		/*******************************************************************************************************************/

		onMakePath: function(that) {
			var globalPath = that.getModel("pagecreateModel").getProperty("/globalPath"),
				globalPathWindow = that.getModel("pagecreateModel").getProperty("/globalPathWindow");

			var oLink = new sap.m.Link({
					enabled: false
				}),
				textArr = [],
				sLink;

			// delete all links from path
			globalPath.removeAllLinks();

			//get array with text of elements
			textArr = this.onGetPathElements(that);

			if (textArr.length > 1) {
				for (var i = 1; i < textArr.length; i++) {
					sLink = oLink.clone();
					sLink.setText(textArr[i - 1]);
					globalPath.addLink(sLink);

					globalPath.setCurrentLocationText(textArr[i]);
				}

			}

			if (globalPath.getLinks().length > 0) {
				globalPathWindow.setVisible(true);
			} else {
				globalPathWindow.setVisible(false);
			}
		},

		/****************************************************************************************************************************
		 *		set "modified" status to the relevant ECN and project
		 * **************************************************************************************************************************/

		fnSetModifiedStatusToEcn: function(ecnKey, sDelete) {
			var sEcnModifiedStatusId = $("div[data-modifiedkey='" + ecnKey + "']")["0"].id;
			var sEcnDeleteBtnId = $("button[data-deletedecnkey='" + ecnKey + "']")["0"].id;
			var oModifiedStatus = sap.ui.getCore().byId(sEcnModifiedStatusId);
			var oEcnDeleteBtn = sap.ui.getCore().byId(sEcnDeleteBtnId);
			var oBundle = this.getModel("i18n").getResourceBundle();
			var sModifiedStatusText = oBundle.getText("objectModifiedStatus");
			var sRemovedStatusText = oBundle.getText("objectRemovedStatus");
			var oDataModel = this.getModel("serviceModel");
			var bEcnModified = false;

			//check it, there is any change entity
			jQuery.each(oDataModel.getPendingChanges(), function(sPath) {
				var sItemEcnKey = oDataModel.getProperty("/" + sPath + "/ParentEcnKey");
				if (sItemEcnKey === ecnKey) {
					bEcnModified = true;
					return false;
				}
			});

			if (sDelete === "true") { // change stutus text and state when ecn was deleted
				// set status text
				oModifiedStatus.setText(sRemovedStatusText);
				//set status state
				oModifiedStatus.setState("Error");
			} else if (bEcnModified) {
				// set status text
				oModifiedStatus.setText(sModifiedStatusText);
				//set status state
				oModifiedStatus.setState("Warning");
			} else {
				//remove status
				oModifiedStatus.setText("");
			}

			// enable/disable "delete button"
			if (sDelete === "false") {
				oEcnDeleteBtn.setEnabled(!bEcnModified);
			}

			// update project footer
			sap.ui.controller("com.vaillant.plm.fiori.projectcockpit.controller.FCLframe").fnCheckShowSaveButton.apply(this);

		},

		/* ***************************************************************************************************************************
		 *		create project items
		 * **************************************************************************************************************************/


		// TODO: Remove this obsolete code from V1.1:
		fnCreateProjectItemsFronEcnList: function(){
			var sTableId = $("div[id$='idEcnList']")["0"].id;
			var oDataModel = this.getModel(this.sDataModel);
			var oProjectsModel = this.oProjectsModel;
			var oBundle = this.getResourceBundle();
			var aItem = this.byId(sTableId).getItems();

			var fnPromiseFuncImportCngSelctPlants = function(oEcnItem){

				return new Promise(
					function(resolve,reject){

						oDataModel.callFunction("/changeSelectedPlants", {
							urlParameters: {
								ProjectKey: oEcnItem.ProjectKey,
								EcnKey: oEcnItem.EcnKey,
								PlantKeyList: oEcnItem.SelectedPlants
							},
							success: function(data) {
								var oResult = {
									"response": data,
									"request": oEcnItem
								};
								resolve(oResult);
							},
							error: function() {
								var sMsgText = oBundle.getText("faildCreateProjectItem", [oEcnItem.ProjectKey, oEcnItem.EcnKey]);
								sap.m.MessageToast.show(sMsgText);
							}
						});
					}

				)
				.catch(function(oError){

					jQuery.sap.log.error("Error in fnPromiseFuncImportCngSelctPlants: ", oError);

				});
			};

			var aEcnsList = [];
			for(var i = 0; i < aItem.length; i++){
				var oCell = aItem[i].getCells()[1];
				var oObject = oCell.getBindingContext(this.sDataModel).getObject();
				var sCurrProjectKey = oObject[this.sParentProjectKey];
				var sCurrEcnKey = oObject[this.sFieldEcnKey];
				var sCurrPlants = oCell.getSelectedKeys().join("|");

				var oEcnItem = {
					"ProjectKey": sCurrProjectKey,
					"EcnKey": sCurrEcnKey,
					"SelectedPlants": sCurrPlants
				};
				aEcnsList.push(oEcnItem);
			}

			function processAdditionOfEcns(arr) {
				return arr.reduce(function(promise, item, index){
					return promise
						.then(function(oPreviousResult){
							return fnPromiseFuncImportCngSelctPlants(item)
								.then(
									function(oResult){
										var sProjectKey = oResult.request.ProjectKey;
										var sEcnKey = oResult.request.EcnKey;
										var sMsgText = oBundle.getText("createProjectItem", [sProjectKey, sEcnKey]);
										sap.m.MessageToast.show(sMsgText);
									}
								);
						})
						.catch(function(oError){
							jQuery.sap.log.error(oError);
						});
				}, Promise.resolve({}));
			}

			var d1 = new Date();
			processAdditionOfEcns(aEcnsList)
			.then(
				function(){
					//update data model
					oDataModel.refresh();

        		    // PLM2-1522:
					// Reload local data for table after updating server model
                    oDataModel.read("/ProjectEntitySet", {
                        urlParameters : {
                            "$expand": "Ecns,ProjectItems"
                        },
                        success: function(oData, oResponse){
                            // Update local JSON Model for the projects list after filling new project with ECNs.
                            if(oProjectsModel){
                                oProjectsModel.setProperty("/aProjects",oData.results);
                            }
                        }.bind(this),
                        error:  function(oData, oResponse){
                            //var i=0;
                        }
                    });

					var d2 = new Date();
					var d3 = d2 - d1;
					jQuery.sap.log.setLevel(jQuery.sap.log.Level.INFO);
					jQuery.sap.log.info("processAdditionOfEcns: Time elapsed in ms = ", d3);
					jQuery.sap.log.setLevel(jQuery.sap.log.Level.ERROR);
				}
			);


		},


		// TODO: Remove this obsolete code from V1:
		fnCreateProjectItemsFronEcnList_OLD: function(){
			var sTableId = $("div[id$='idEcnList']")["0"].id;
			var oDataModel = this.getModel(this.sDataModel);
			var oBundle = this.getResourceBundle();
			var aItem = this.byId(sTableId).getItems();

			for(var i = 0; i < aItem.length; i++){
				var oCell = aItem[i].getCells()[1];
				var oObject = oCell.getBindingContext(this.sDataModel).getObject();
				var sCurrProjectKey = oObject[this.sParentProjectKey];
				var sCurrEcnKey = oObject[this.sFieldEcnKey];
				var sCurrPlants = oCell.getSelectedKeys().join("|");

				//call func import for change selected plants
				this.fnFuncImportCngSelctPlants(oDataModel, oBundle, sCurrProjectKey, sCurrEcnKey, sCurrPlants);
			}
		},


		// TODO: Remove this obsolete code from V1:
		fnFuncImportCngSelctPlants: function(oDataModel, oBundle, sProjectKey, sEcnKey, sSelectedPlants){
			oDataModel.setUseBatch(true);
			oDataModel.callFunction("/changeSelectedPlants", {
				urlParameters: {
					ProjectKey: sProjectKey,
					EcnKey: sEcnKey,
					PlantKeyList: sSelectedPlants
				},
				success: function(data) {

					// message toast display
					var sMsgText = oBundle.getText("createProjectItem", [sProjectKey, sEcnKey]);
					sap.m.MessageToast.show(sMsgText);

					oDataModel.setUseBatch( sap.ui.getCore().getModel("settingsModel").getProperty("/BatchMode") );
					//update data model
					oDataModel.refresh();

				},
				error: function(oError) {
					oDataModel.setUseBatch( sap.ui.getCore().getModel("settingsModel").getProperty("/BatchMode") );
				}.bind(this)
			});

		},


		pGetPotentialPlants: function(sProjectKey){

			var oDataModel = this.getModel("serviceModel");
			var oBundle = this.getResourceBundle();

			return new Promise(
				function(resolve,reject){

					oDataModel.callFunction("/getPotentialPlants", {
						urlParameters: {
							projectKey: sProjectKey,
						},
						success: function(data) {
							var oResult = {
								"response": data,
								"request": sProjectKey
							};
							resolve(oResult);
						},
						error: function(oError) {
							var sMsgText = oBundle.getText("txtFailedGetPotentialPlants", [sProjectKey]);
							sap.m.MessageToast.show(sMsgText);
							reject(oError);
						}
					});
				}.bind(this)

			)
			.catch(function(oError){

				jQuery.sap.log.error("Error in pGetPotentialPlants: ", oError);
				reject(oError);

			}.bind(this));
		},


		pReadPotentialItems: function(oDataModel,sPotentialItemsPath,aFilters,oUrlParameters){

			return new Promise(
				function(resolve,reject){
					oDataModel.read( sPotentialItemsPath, {

                        filters: aFilters,
                        urlParameters: oUrlParameters,
						success: function(oData) {
							resolve({
								"data": oData,
								"sEcnPath": sPotentialItemsPath.substring(0,sPotentialItemsPath.lastIndexOf("/"))
							});
						},
						error: function(oError) {
						    var sMsgText = "Failed to read " + sPotentialItemsPath;
							sap.m.MessageToast.show(sMsgText);
							reject(oError);
						}
					});
				}.bind(this)
			)
		},


		clone: function( object ){
		    return JSON.parse( JSON.stringify( object ) );
		}

	});
});