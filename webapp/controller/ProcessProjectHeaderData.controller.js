sap.ui.define([
	"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"com/vaillant/plm/fiori/projectcockpit/common/js/Utils"
], function(BaseController, JSONModel, MessageToast, Utils) {
	"use strict";
	return BaseController.extend("com.vaillant.plm.fiori.projectcockpit.controller.ProcessProjectHeaderData", {


		onInit: function() {
		},
		
		fnChangeProjectHeaderField: function(oEvent) {
			this.oInputValues = this.fnCheckFieldAndUpdate(oEvent, this.oInputValues);

			// enabled / disabled submit or add ECNs buttons
			this.fnEnabledSubmitButton();
		},
		
		// SPRINT-2: SPARE Flag Handling
		onSelectProjectCategory: function(oEvent){
			var oBindingContext = oEvent.getSource().getBindingContext("serviceModel");
			var sPath = oBindingContext.sPath;
			if( this.getView().byId("idProjectCategory").getSelected() ){
				// switch forth to S
				oBindingContext.getModel().setProperty(sPath + "/ProjectCategory", "S")
			} else {
				// switch back to M
				oBindingContext.getModel().setProperty(sPath + "/ProjectCategory", "M")
			}
		},
		
		onAfterRendering: function(oEvent) {

			//changeLive function for datePicker
			$("input[id$='idPhaseOutDate']").keyup(function(evt) {
				var oDatePicker = sap.ui.getCore().byId(evt.currentTarget.id);
				if (oDatePicker._getInputValue() === "") {
					oDatePicker.fireChange();
				}

			});

			// prepare array with required fields this.oNewInputValues = {};
			var aInput = {};
			// Adrian: What is this good for?
			jQuery.each($("input[aria-required='true']"), function(i, data) {
				aInput[data.name] = {
					id: data.parentNode.parentNode.id,
					value: data.value,
					required: true
				};
			});

			if(aInput["ProjectDescription"]){
    			aInput["ProjectDescription"].value  = this.getView().byId("idProjectDescription").getValue();
			}
			if(aInput["PhaseOutManagerKey"]){
    			aInput["PhaseOutManagerKey"].value  = this.getView().byId("idPhaseOutManagerDisplayName").getSelectedKey();
			}
			if(aInput["PhaseOutDate"]){
    			aInput["PhaseOutDate"].value  = this.getView().byId("idPhaseOutDate").getDateValue();
			}

			this.oInputValues = aInput;
			
			// enabled apply button 
			//this.fnProjectHeaderDataIsFilled(false);
			
			var oAppModel = this.getModel("appView");
			var oToday = new Date();
			if(oAppModel){
				oAppModel.setProperty("/minPhaseOutDate", oToday);
			} else {
				this.setModel(new JSONModel({
					"minPhaseOutDate": oToday
				}),"appView");
			}
			
		},

		fnEnabledSubmitButton: function() {
			var bEnabled = this.fnSetEnabledButton();
			this.fnProjectHeaderDataIsFilled(bEnabled);
		},

		/* ****************************************************************************************************************
		 *  check field and update value
		 * ***************************************************************************************************************/
		fnCheckFieldAndUpdate: function(oEvent, oInputValues) {
			var oInputField = oEvent.getSource();
			var oInputType = oInputField.getMetadata().getName();
			var sNewValue;
			var sInputName = oInputField.getProperty("name");
			var bRequired = oInputField.getProperty("required");
			var oDataModel = this.getModel("serviceModel");
			var sPath = oInputField.getBindingContext("serviceModel").getPath();
			
			switch(oInputType){
				case "sap.m.Input":
					sNewValue = oEvent.getParameter("value");
					break;
				case "sap.m.ComboBox":
					sNewValue = oInputField.getSelectedKey();
					// change manger name
					oDataModel.setProperty(sPath + "/PhaseOutManagerDisplayName", oInputField.getValue());
					break;
				case "sap.m.DatePicker":
					sNewValue = oEvent.getParameter("value").replace(".", "").length > 5 ? oInputField.getProperty("dateValue") : "";
			}

			// add value to the object
			if (!this.oInputValues[sInputName]) {
				this.oInputValues[sInputName] = {
					id: oEvent.getParameter("id"),
					value: sNewValue,
					required: bRequired
				};
			} else {
				this.oInputValues[sInputName].value = sNewValue;
			}

			//change state of input field
			if (sNewValue.length === 0 && bRequired === true) {
				oInputField.setValueState("Error");
			} else {
				oInputField.setValueState("None");
			}
			
			// update entry in model
			oDataModel.setProperty(sPath + "/" + sInputName, sNewValue);
			
			return oInputValues;
		},
		
		/* *********************************************************************************************************************************
		 * 		control enable/disable button depends on input
		 * ********************************************************************************************************************************/
		fnSetEnabledButton: function() {
			var bEnabled = true;

			jQuery.each(this.oInputValues, function(name, data){
			    if(name && name !== ""){
    				if (( !data.value || data.value === "" ) && data.required === true) {
    					bEnabled = false;
    				}
			    }
			});

			return bEnabled;
		},
		
		/************************************************************************************************************************
		 *  enabled / disabled submit button
		 * *********************************************************************************************************************/

		fnProjectHeaderDataIsFilled: function(bEnabled) {
			var oAppModel = this.getModel("appView");
			oAppModel.setProperty("/bNewProjectHeaderDataIsFilled", bEnabled);
		}

	});
});