sap.ui.define([
	"com/vaillant/plm/fiori/projectcockpit/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"com/vaillant/plm/fiori/projectcockpit/model/formatter",
	"sap/m/MessageToast"
], function (BaseController, JSONModel, History, formatter, MessageToast) {
	"use strict";

	return BaseController.extend("com.vaillant.plm.fiori.projectcockpit.controller.FCLinfo", {

		formatter: formatter,
		sObjectModelView: "objectView",

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit: function () {

			this.bus = sap.ui.getCore().getEventBus();

		},

		onExit: function () {
			this.bus.unsubscribe("flexible", "setDetailPage", this.setDetailPage, this);
			this.bus.unsubscribe("flexible", "setDetailDetailPage", this.setDetailDetailPage, this);
		},

		handlePress: function () {
			MessageToast.show("Loading end column...");
		},

		fnCloseMasterDetails: function () {

			var oFlexColumnLayout = this.getModel(this.sObjectModelView).getProperty("/oFlexibleColumnLayout");
			oFlexColumnLayout.setLayout("TwoColumnsMidExpanded");
		}

	});
});